//
//  AlertController.swift
//  LaundryApp
//
//  Created by ZoZy on 9/1/15.
//  Copyright (c) 2015 ZoZy. All rights reserved.
//

import Foundation

class AlertController{
    var uiView: UIView!
    var container: UIView!
    init(){
        uiView = UIApplication.currentViewController()?.view
        container = UIView()
        container.frame = uiView.frame
        container.center = uiView.center
        container.backgroundColor = UIColor(netHex: 0x000000)
        container.alpha = 0.5
        let loadingView: UIView = UIView()
        loadingView.frame = CGRectMake(0, 0, 80, 80)
        loadingView.center = uiView.center
        loadingView.backgroundColor = UIColor(netHex: 0x000000)
                loadingView.alpha = 0.7
        loadingView.clipsToBounds = true
        loadingView.layer.cornerRadius = 10
        let actInd: UIActivityIndicatorView = UIActivityIndicatorView()
        actInd.frame = CGRectMake(0.0, 0.0, 40.0, 40.0);
        actInd.activityIndicatorViewStyle =
            UIActivityIndicatorViewStyle.WhiteLarge
        actInd.center = CGPointMake(loadingView.frame.size.width / 2,
            loadingView.frame.size.height / 2);
        loadingView.addSubview(actInd)
        container.addSubview(loadingView)
        actInd.startAnimating()
    }
    init(view: UIView){
        uiView = view
        container = UIView()
        container.frame = uiView.frame
        container.center = uiView.center
        container.backgroundColor = UIColor(netHex: 0x000000)
        container.alpha = 0.5
        let loadingView: UIView = UIView()
        loadingView.frame = CGRectMake(0, 0, 80, 80)
        loadingView.center = uiView.center
        loadingView.backgroundColor = UIColor(netHex: 0x000000)
        loadingView.alpha = 0.7
        loadingView.clipsToBounds = true
        loadingView.layer.cornerRadius = 10
        let actInd: UIActivityIndicatorView = UIActivityIndicatorView()
        actInd.frame = CGRectMake(0.0, 0.0, 40.0, 40.0);
        actInd.activityIndicatorViewStyle =
            UIActivityIndicatorViewStyle.WhiteLarge
        actInd.center = CGPointMake(loadingView.frame.size.width / 2,
                                    loadingView.frame.size.height / 2);
        loadingView.addSubview(actInd)
        container.addSubview(loadingView)
        actInd.startAnimating()
    }
    func getView()-> UIView{
        return container
    }
      func ShowIndicator() {
        if(container.isDescendantOfView(uiView)) {
//            print("not add")
        } else {
//            print("added")
            uiView.addSubview(container)
        }
    }
    func DismissIndicator(){
        if(container.isDescendantOfView(uiView)) {
//            print("remove")

            container.removeFromSuperview()
        } else {
//            print("not remove")
        }
    }
    func showAlert(title: String, mess: String){
        let alert = UIAlertController(title: title, message: mess, preferredStyle: UIAlertControllerStyle.Alert)
        alert.addAction(UIAlertAction(title: "OK", style: .Destructive, handler: nil))
        UIApplication.currentViewController()?.presentViewController(alert, animated: true, completion: nil)
    }
//    func showNotification(mess: String){
//        uiView = UIApplication.currentViewController()?.view
//        let popupNotification = BadgeNotification(nibName: "BadgeNotification", bundle: nil)
//        popupNotification.message = mess
//        popupNotification.showInView(uiView, animated: true)
//    }
}