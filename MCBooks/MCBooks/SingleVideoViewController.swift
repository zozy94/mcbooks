//
//  SingleMediaViewController.swift
//  MCBooks
//
//  Created by Quang Anh on 6/20/16.
//  Copyright © 2016 Quang Anh. All rights reserved.
//

import UIKit
import AVFoundation

class SingleVideoViewController: UIViewController {
    @IBOutlet weak var topView: UIView!
    
    @IBOutlet weak var bookInfoView: UIView!
    @IBOutlet weak var bookImage: UIImageView!
    @IBOutlet weak var bookName: UILabel!
    @IBOutlet weak var publisher: UILabel!
    @IBOutlet weak var author: UILabel!
    @IBOutlet weak var rating: UILabel!
    @IBOutlet weak var price: UILabel!
    @IBOutlet weak var webView: UIWebView!
    
    @IBOutlet weak var priceView: UIView!
    
    var audioURL = ""
    var playerItem: AVPlayerItem!
    var player: AVPlayer!
    var isPlaying = false
    var Timer: NSTimer!
    var cellHeight:CGFloat = 0
    var playingID = ""
    var mediaData = Media()
    var bookData = Book()
    override func viewDidLoad() {
        self.navigationController!.interactivePopGestureRecognizer!.enabled = false
        bookInfoView.hidden = true
        priceView.layer.cornerRadius = 3
        priceView.layer.borderColor = Color.theme.CGColor
        priceView.layer.borderWidth = 0.6

        topView.backgroundColor = Color.theme
        mediaData = AppFunc().appDelegate.SelectedMedia
        webView.allowsInlineMediaPlayback = true
        webView.sizeThatFits(webView.bounds.size)
        webView.scalesPageToFit = true
        webView.backgroundColor = UIColor.whiteColor()
        webView.scrollView.scrollEnabled = false
        webView.contentMode = UIViewContentMode.ScaleAspectFit
        loadVideo(mediaData.url)

        
        getBookInfo()
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(DetailViewController.ShowBook), name: "ShowBook", object: nil)
    }
    
    func getBookInfo(){
        let CN = ConnectServer()
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)) {
            var status = 0
            if let json: AnyObject = CN.GETPageBook("book/" + self.mediaData.bookID){
                status =  json.valueForKey("code") as! Int
                if (status == 1){
                    let result = json.valueForKey("result")! as AnyObject
                    self.bookData = AppFunc().fetchBookData(result)
                    if let thumb = AppFunc().getImage(URL.Thumb + self.bookData.image){
                        self.bookData.thumb = thumb
                    }
                    dispatch_async(dispatch_get_main_queue()) {
                        if self.bookData.thumb != nil {
                            self.bookImage.image = self.bookData.thumb
                        }
                        self.bookName.text = self.bookData.name
                        self.publisher.text = self.bookData.publisher
                        self.author.text = self.bookData.author
                        self.rating.text = AppFunc().ratingLabel(self.bookData.avg_star)
                        self.price.text = AppFunc().decimalNum(self.bookData.price) + " vnđ"
                        self.bookInfoView.hidden = false
                    }
                }
            }
        }
    }
    @IBAction func viewBookButtonTapped(sender: AnyObject) {
        AppFunc().appDelegate.SelectedBook = self.bookData
        performSegueWithIdentifier("bookDetail", sender: self)
        
    }
    
    func loadVideo(url: String){
        let id = getID(url)
        let embededHTML = "<html><style> #map_container {position: relative;width: 100%;padding-bottom: 60%;} #map { position:absolute;width:100%; height:100%;}</style> <body style=\"background: #0173bc;\"><div id=\"map_container\"><iframe  id=\"map\" src=\"https://www.youtube.com/embed/\(id)?&playsinline=1&rel=0&autoplay=1\" width=\"100%%\" height=\"100%%\" frameborder=\"0\" allowfullscreen></iframe></div></body></html>"
        print(embededHTML)
        webView.loadHTMLString(embededHTML, baseURL: nil)
    }
    func getID(url: String) -> String{
        print(url)
        var id = ""
        let stop: Character = "="
        for index in url.characters.indices {
            let char = url[index]
            if char == stop {
                id = url.substringFromIndex(index.advancedBy(1))
                break
            }
        }
        return id
    }
    
    override func viewWillDisappear(animated: Bool) {
        self.navigationController!.interactivePopGestureRecognizer!.enabled = true
        NSNotificationCenter.defaultCenter().removeObserver(self, name: "ShowBook", object: nil)
        
    }
}
