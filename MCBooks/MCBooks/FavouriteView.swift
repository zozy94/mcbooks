//
//  FavouriteViewController.swift
//  MCBooks
//
//  Created by Quang Anh on 6/20/16.
//  Copyright © 2016 Quang Anh. All rights reserved.
//

import UIKit
@IBDesignable
class FavouriteView: UIView {
    var view: UIView!

    @IBOutlet weak var viewContainter: UIView!
    @IBOutlet weak var favBookBtn: UIButton!
    @IBOutlet weak var favAudioBtn: UIButton!
    @IBOutlet weak var favVideoBtn: UIButton!
    @IBOutlet weak var sliderView: UIView!
    @IBOutlet weak var sliderLeadContraint: NSLayoutConstraint!
    var bookView:ShowAllView!
    var audioView:ShowAllView!
    var videoView:ShowAllView!
    var sliderFr:CGRect!
    var showTab = Tab.BOOK
    override init(frame: CGRect) {
        super.init(frame: frame)
        xibSetup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        xibSetup()
    }
    func refresh(){
        removeSubView()
        print("refresh favourite")
        showTab = AppFunc().appDelegate.showTab
        switch(showTab){
        case Tab.BOOK:
            sliderTo(0)
            showBook()
            break
        case Tab.AUDIO:
            sliderTo(1)
            showAudio()
            break
        case Tab.VIDEO:
            sliderTo(2)
            showVideo()
            break
        default:
            break
        }
    }
    func xibSetup() {
        view = loadViewFromNib()
        view.frame = bounds
        sliderFr = sliderView.frame
        favBookBtn.backgroundColor = Color.theme
        favAudioBtn.backgroundColor = Color.theme
        favVideoBtn.backgroundColor = Color.theme
        print(AppFunc().appDelegate.favouriteBook)
        addSubview(view)
    }

    func showBook(){
        self.favBookBtn.enabled = false
        self.favAudioBtn.enabled = false
        self.favVideoBtn.enabled = false
        removeSubView()
        AppFunc().appDelegate.showTab = Tab.BOOK
        if(bookView == nil){
            let frame = CGRectMake(0, 0, self.view.frame.width+8, self.view.frame.height-100)
            self.bookView = ShowAllView(frame: frame)
            self.bookView.setURL(showType.FAV)
            self.viewContainter.addSubview(self.bookView)
        } else {
            self.viewContainter.addSubview(self.bookView)
            bookView.slideInFromLeft(0.25, completionDelegate: self)
            bookView.setURL(showType.FAV)
        }
        showTab = Tab.BOOK
        self.favBookBtn.enabled = true
        self.favAudioBtn.enabled = true
        self.favVideoBtn.enabled = true

    }
    func showAudio(){
        removeSubView()
        AppFunc().appDelegate.showTab = Tab.AUDIO
        self.favBookBtn.enabled = false
        self.favAudioBtn.enabled = false
        self.favVideoBtn.enabled = false
        if(audioView == nil){
            let frame = CGRectMake(0, 0, self.view.frame.width+8, self.view.frame.height-100)
            self.audioView = ShowAllView(frame: frame)
            self.audioView.setURL(showType.MEDIA)
            self.viewContainter.addSubview(self.audioView)
                if showTab == Tab.BOOK {
                    self.audioView.slideInFromRight(0.25, completionDelegate: self)
                } else {
                    self.audioView.slideInFromLeft(0.25, completionDelegate: self)
                }
        } else {
            self.viewContainter.addSubview(self.audioView)
            if showTab == Tab.BOOK {
                self.audioView.slideInFromRight(0.25, completionDelegate: self)
            } else {
                self.audioView.slideInFromLeft(0.25, completionDelegate: self)
            }
            self.audioView.setURL(showType.MEDIA)
        }
        showTab = Tab.AUDIO
        self.favBookBtn.enabled = true
        self.favAudioBtn.enabled = true
        self.favVideoBtn.enabled = true

    }
    func showVideo(){
        removeSubView()
        AppFunc().appDelegate.showTab = Tab.VIDEO
        self.favBookBtn.enabled = false
        self.favAudioBtn.enabled = false
        self.favVideoBtn.enabled = false
        if(videoView == nil){
            let frame = CGRectMake(0, 0, self.view.frame.width+8, self.view.frame.height-100)
            self.videoView = ShowAllView(frame: frame)
            self.videoView.setURL(showType.VIDEO)
            self.viewContainter.addSubview(self.videoView)
            self.videoView.slideInFromRight(0.25, completionDelegate: self)
        } else {
            self.viewContainter.addSubview(self.videoView)
            self.videoView.slideInFromRight(0.25, completionDelegate: self)
            self.videoView.setURL(showType.VIDEO)
        }
        showTab = Tab.VIDEO
        self.favBookBtn.enabled = true
        self.favAudioBtn.enabled = true
        self.favVideoBtn.enabled = true
        print("Video \(videoView.mediaData.count)")
    }
    func removeSubView(){
        for view in viewContainter.subviews {
            view.removeFromSuperview()
        }
    }
    
    @IBAction func bookClick(sender: AnyObject) {
        if showTab != Tab.BOOK {
        sliderTo(0)
        showBook()
            NSNotificationCenter.defaultCenter().postNotificationName("ShowSwitchView", object: nil, userInfo: nil)
        }
    }
    
    @IBAction func audioClick(sender: AnyObject) {
        if showTab != Tab.AUDIO {
        sliderTo(1)
        showAudio()
            NSNotificationCenter.defaultCenter().postNotificationName("HideSwitchView", object: nil, userInfo: nil)

        }
    }
    
    @IBAction func videoClick(sender: AnyObject) {
        if showTab != Tab.VIDEO {
            showVideo()
            sliderTo(2)
            NSNotificationCenter.defaultCenter().postNotificationName("HideSwitchView", object: nil, userInfo: nil)
        }
    }
    
    func sliderTo(pos: Int){
        var x:CGFloat = 0
        if pos == 1 {
            x = favAudioBtn.frame.minX
        } else if pos == 2 {
            x = favVideoBtn.frame.minX
        }
        let fr = CGRectMake(x, sliderFr.minY, favBookBtn.frame.width, sliderFr.height)
        UIView.animateWithDuration(0.25, animations: {
            self.sliderView.frame = fr
            }, completion:{(finished : Bool)  in
                if (finished)
                {
                    
                }
        });
    }
    func slideRightTransitionFromView(view: UIView, toView: UIView, duration: NSTimeInterval, completion: ((Bool) -> Void)?) {
        if let container = view.superview {
            // Final position of outgoing view
            let outgoingViewEndX = view.frame.origin.x - view.frame.size.width
            
            // Final position of incoming view (existing frame)
            let incomingViewEndX = view.frame.origin.x
            
            // Start point of incoming view -- at the right edge of the outgoing view
            let incomingViewStartX = view.frame.origin.x + view.frame.size.width
            
            // Distance traveled by outgoing view
            let outgoingDisplacement = view.frame.origin.x - outgoingViewEndX
            
            toView.frame.origin = CGPointMake(incomingViewStartX, toView.frame.origin.y)
            container.addSubview(toView)
            
            dispatch_async(dispatch_get_main_queue(), { ()
                UIView.animateWithDuration(duration, delay: 0, options: .CurveLinear, animations: {
                    view.frame.origin.x = outgoingViewEndX
                    toView.frame.origin.x = incomingViewEndX
                    }, completion: {(complete: Bool) in
                        // If the outgoing view is still in onscreen, keep sliding until it's offscreen
                        if outgoingViewEndX + view.frame.size.width > 0 {
                            
                            // Adjust the duration for the final animation based on the distance it has to travel in order to keep the same velocity.
                            let finalDuration = duration * Double((outgoingViewEndX + view.frame.size.width)/outgoingDisplacement)
                            
                            UIView.animateWithDuration(finalDuration, delay: 0, options: .CurveLinear, animations: {
                                view.frame.origin.x = -view.frame.size.width
                                }, completion: { (complete: Bool) in
                                    completion?(complete)
                                    view.removeFromSuperview()
                            })
                        }
                            
                        else {
                            completion?(complete)
                            view.removeFromSuperview()
                        }
                })
            })
        }
    }

    func slideLeftTransitionFromView(view: UIView, toView: UIView, duration: NSTimeInterval, completion: ((Bool) -> Void)?) {
        if let container = view.superview {
            // Final position of outgoing view
            let outgoingViewEndX = -view.frame.origin.x + view.frame.size.width
            
            // Final position of incoming view (existing frame)
            let incomingViewEndX = view.frame.origin.x
            
            // Start point of incoming view -- at the right edge of the outgoing view
            let incomingViewStartX = view.frame.origin.x - view.frame.size.width
            
            // Distance traveled by outgoing view
            let outgoingDisplacement = view.frame.origin.x - outgoingViewEndX
            
            toView.frame.origin = CGPointMake(incomingViewStartX, toView.frame.origin.y)
            container.addSubview(toView)
            
            dispatch_async(dispatch_get_main_queue(), { ()
                UIView.animateWithDuration(duration, delay: 0, options: .CurveLinear, animations: {
                    view.frame.origin.x = outgoingViewEndX
                    toView.frame.origin.x = incomingViewEndX
                    }, completion: {(complete: Bool) in
                        // If the outgoing view is still in onscreen, keep sliding until it's offscreen
                        if outgoingViewEndX + view.frame.size.width > 0 {
                            
                            // Adjust the duration for the final animation based on the distance it has to travel in order to keep the same velocity.
                            let finalDuration = duration * Double((outgoingViewEndX + view.frame.size.width)/outgoingDisplacement)
                            
                            UIView.animateWithDuration(finalDuration, delay: 0, options: .CurveLinear, animations: {
                                view.frame.origin.x = -view.frame.size.width
                                }, completion: { (complete: Bool) in
                                    completion?(complete)
                                    view.removeFromSuperview()
                            })
                        }
                            
                        else {
                            completion?(complete)
                            view.removeFromSuperview()
                        }
                })
            })
        }
    }

    func loadViewFromNib() -> UIView {
        let bundle = NSBundle(forClass: self.dynamicType)
        let nib = UINib(nibName: "FavouriteView", bundle: bundle)
        let view = nib.instantiateWithOwner(self, options: nil)[0] as! UIView
        return view
    }

}
