//
//  AddressInputView.swift
//  JupViec
//
//  Created by Quang Anh on 5/4/16.
//  Copyright © 2016 Quang Anh. All rights reserved.
//

import UIKit
@IBDesignable
class PopupSearch: UIViewController, UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate {
//    var view: UIView!

    
    @IBOutlet weak var topView: UIView!
    @IBOutlet var popupView: UIView!
    @IBOutlet weak var doneButton: UIButton!
    var searchResult = Array<Book>()
    @IBOutlet weak var textField: UITextField!
    var page = 1
    @IBOutlet weak var tableView: UITableView!
    var textSearch = ""
    var isAdd = false
    var isBottom = false

    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
    }
    var connection: NSURLConnection!
    override init(nibName nibNameOrNil: String!, bundle nibBundleOrNil: NSBundle!) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    }
    var currentTask: NSURLSessionTask?
    override func viewDidLoad() {
        super.viewDidLoad()
//        self.view.backgroundColor = UIColor.blackColor().colorWithAlphaComponent(0.4)
        doneButton.backgroundColor = Color.theme
        topView.backgroundColor = Color.theme
        textField.delegate = self
        textField.addTarget(self, action: #selector(PopupSearch.textFieldDidChange(_:)), forControlEvents: UIControlEvents.EditingChanged)
        AppFunc().appDelegate.isShowSearch = true
    }
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 80
    }
    func textFieldDidChange(textField: UITextField) {
         search(textField.text!)
    }

    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return searchResult.count
    }
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let bundle = NSBundle(forClass: self.dynamicType)
        let nib = UINib(nibName: "SearchItemCell", bundle: bundle)
        let cell = nib.instantiateWithOwner(self, options: nil)[0] as! SearchItemCell
        cell.setup(searchResult[indexPath.row])
        return cell
    }
    func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
        if (indexPath.row + 1) == searchResult.count {
            isBottom = true
        } else {
            isBottom = false
        }
    }
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let cell = tableView.cellForRowAtIndexPath(indexPath) as! SearchItemCell
        AppFunc().appDelegate.SelectedBook = cell.book
        NSNotificationCenter.defaultCenter().postNotificationName("ShowBookSearch", object: nil, userInfo: nil)
        self.close()
    }
    func scrollViewDidEndDecelerating(scrollView: UIScrollView) {
        if isBottom {
            isAdd = true
            search(textSearch)
        }
    }
    func showInView(aView: UIView!, animated: Bool)
    {
        self.view.frame = AppFunc().appDelegate.viewFrame
        aView.addSubview(self.view)
        if animated
        {
            self.showAnimate()
        }
    }
    func showAnimate()
    {
        self.view.transform = CGAffineTransformMakeScale(1.3, 1.3)
        self.view.alpha = 0.0;
        UIView.animateWithDuration(0.25, animations: {
            self.view.alpha = 1.0
            self.view.transform = CGAffineTransformMakeScale(1.0, 1.0)
        });
    }
    
    @IBAction func closePopup(sender: AnyObject) {
        close()
    }
    func close(){
        UIView.animateWithDuration(0.25, animations: {
            self.view.transform = CGAffineTransformMakeScale(1.3, 1.3)
            self.view.alpha = 0.0;
            }, completion:{(finished : Bool)  in
                if (finished)
                {
                    self.view.removeFromSuperview()
                    AppFunc().appDelegate.isShowSearch = false
                }
        });
    }
    enum JSONError: String, ErrorType {
        case NoData = "ERROR: no data"
        case ConversionFailed = "ERROR: conversion from JSON failed"
    }

    func search(text: String){
        var check = false
        if text != textSearch {
            textSearch = text
            check = true
            self.page = 1
        }
        if text != "" {
            if currentTask != nil {
                currentTask?.cancel()
            }
            var newdata = Array<Book>()
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)) {
                let token = AppFunc().appDelegate.userData.token
                var url = URL.Server + URL.search+"?page=\(self.page)&keyword=\(self.textSearch)"
                print(url)
                url = url.stringByAddingPercentEncodingWithAllowedCharacters(NSCharacterSet.URLQueryAllowedCharacterSet())!
                // abc
                let request = NSMutableURLRequest(URL: NSURL(string: url)!);
                request.HTTPMethod = "GET";
                request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
                request.setValue("application/json", forHTTPHeaderField: "Accept")
                request.setValue("Access_token " + token, forHTTPHeaderField: "Authorization")
                print("Token = \(token)")
                var response: NSURLResponse?
                do {
                    let urlData = try NSURLConnection.sendSynchronousRequest(request, returningResponse: &response)
                    if let jsonResult: AnyObject? = try? NSJSONSerialization.JSONObjectWithData(urlData, options: NSJSONReadingOptions(rawValue: 0)) {
                        let json = jsonResult!.valueForKey("result")! as AnyObject
                        if let result = json.valueForKey("books") as? Array<AnyObject> {
                            if result.count > 0 {
                                self.page += 1
                                for item in result {
                                    let book = AppFunc().fetchBookData(item)
                                    newdata.append(book)
                                }
                            }
                        }
                    }
                    dispatch_async(dispatch_get_main_queue()) {
                        self.addItem(newdata,isNew: check)
                    }
                } catch let error1 as NSError {
                    print(error1)
                }
            }
        } else {
            self.searchResult.removeAll()
            tableView.reloadData()
        }
    }
    func addItem(book: Array<Book>, isNew: Bool){
        self.tableView.beginUpdates()
        if isNew {
            searchResult.removeAll()
        }
        for item in book {
            self.searchResult.append(item)
        }
        tableView.reloadSections(NSIndexSet.init(index: 0), withRowAnimation: .Fade)
        self.tableView.endUpdates()
    }
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}
