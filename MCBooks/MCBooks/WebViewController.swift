//
//  SecondViewController.swift
//  MCBooks
//
//  Created by Quang Anh on 6/17/16.
//  Copyright © 2016 Quang Anh. All rights reserved.
//

import UIKit

class WebViewController: UIViewController{
    var URL = ""
    var PDFView: WebView!
    override func viewWillAppear(animated: Bool) {
        self.view.frame = AppFunc().appDelegate.viewFrame
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        ShowPDF()
    }
    override func viewWillDisappear(animated: Bool) {
        if PDFView != nil {
            PDFView.clean()
        }
        self.view = nil
    }
    func ShowPDF(){
        PDFView = WebView(frame: self.view.frame)
        self.view.addSubview(PDFView)
        var url: NSURL!
        if URL == "" {
            PDFView.webView.scalesPageToFit = false
            url = NSBundle.mainBundle().URLForResource("a", withExtension:"html")
        } else {
            PDFView.webView.scalesPageToFit = true
            url = NSURL(string: URL)
        }
        PDFView.setPDF(url)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
