//
//  ModelCollection.swift
//  MCBooks
//
//  Created by Quang Anh on 6/4/16.
//  Copyright © 2016 Quang Anh. All rights reserved.
//

import Foundation

class User{
    var name = ""
    var phone = ""
    var email = ""
    var facebookLink = ""
    var facebookId = ""
    var googleID = ""
    var token = ""
    var avatarURL = ""
    var avatar : UIImage?
}

class Book {
    var id = ""
    var image = ""
    var name = ""
    var price = 0
    var publisher = ""
    var description = ""
    var author = ""
    var preview = ""
    var categories = Array<Category>()
    var medias = Array<Media>()
    var sale = Array<String>()
    var isHot = false
    var isNew = false
    var isComing = false
    var isFavourite = false
    var myrate = Rate()
    var avg_star = 0.0
    var thumb: UIImage?
    var buyURL = ""
    func audio() -> Array<Media>{
        var audio = Array<Media>()
        for item in medias {
            if item.type == 0 {
                audio.append(item)
            }
        }
        return audio
    }
    func video() -> Array<Media>{
        var video = Array<Media>()
        for item in medias {
            if item.type == 1 {
                video.append(item)
            }
        }
        return video
    }

}
class Category {
    var id = ""
    var name = ""
    var description = ""
}
class Media {
    var type = 0
    var id = ""
    var name = ""
    var url = ""
    var offlineURL = ""
    var isDownload = false
    var isFav = false
    var bookName = ""
    var bookID = ""
    func getOfflineURL(){
        let path = NSSearchPathForDirectoriesInDomains(NSSearchPathDirectory.DocumentDirectory, NSSearchPathDomainMask.UserDomainMask, true)
        let documentDirectoryPath:String = path[0]
        let fileManager = NSFileManager()
        let destinationURLForFile = NSURL(fileURLWithPath: documentDirectoryPath.stringByAppendingString("/\(id).mp3"))
        if fileManager.fileExistsAtPath(destinationURLForFile.path!){
            print("checked \(id)")
            offlineURL = destinationURLForFile.path!
            isDownload = true
        } else {
            print("not found \(id)")
        }
    }
}
class Rate {
    var name = ""
    var stars = 0.0
    var comment = ""
    var date = ""
    var image = ""
    var avatar:UIImage?
}
class Gift {
    var id = ""
    var name = ""
    var type = 0
    var image = ""
    var description = ""
    var file = ""
    var thumb: UIImage?
}
class googleData: NSObject {
    var name = ""
    var phone = ""
    var email = ""
    var facebookLink = ""
    var facebookId = ""
    var googleID = ""
    var token = ""
    var avatarURL = ""
    override init(){
        
    }
    func encodeWithCoder(aCoder: NSCoder!){
        aCoder.encodeObject(name, forKey: "name")
        aCoder.encodeObject(phone, forKey: "phone")
        aCoder.encodeObject(email, forKey: "email")
        aCoder.encodeObject(facebookLink, forKey: "facebookLink")
        aCoder.encodeObject(facebookId, forKey: "facebookId")
        aCoder.encodeObject(googleID, forKey: "googleID")
        aCoder.encodeObject(token, forKey: "token")
        aCoder.encodeObject(avatarURL, forKey: "avatarURL")
    }
    required init(coder aDecoder: NSCoder!){
        self.name = aDecoder.decodeObjectForKey("name") as! String
        self.phone = aDecoder.decodeObjectForKey("phone") as! String
        self.email = aDecoder.decodeObjectForKey("email") as! String
        self.facebookLink = aDecoder.decodeObjectForKey("facebookLink") as! String
        self.facebookId = aDecoder.decodeObjectForKey("facebookId") as! String
        self.googleID = aDecoder.decodeObjectForKey("googleID") as! String
        self.token = aDecoder.decodeObjectForKey("token") as! String
        self.avatarURL = aDecoder.decodeObjectForKey("avatarURL") as! String
        super.init()
    }

}