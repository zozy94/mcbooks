//
//  CommentView.swift
//  MCBooks
//
//  Created by Quang Anh on 6/25/16.
//  Copyright © 2016 Quang Anh. All rights reserved.
//

import UIKit
@IBDesignable
class CommentView: UIView {
    var view: UIView!
    @IBOutlet weak var avatar: UIImageView!
    @IBOutlet weak var dateLbl: UILabel!
    @IBOutlet weak var comment: UILabel!
    @IBOutlet weak var rateLbl: UILabel!
    @IBOutlet weak var nameLbl: UILabel!
    override init(frame: CGRect) {
        super.init(frame: frame)
        xibSetup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        xibSetup()
    }
    func xibSetup() {
        view = loadViewFromNib()
        view.frame = bounds
        avatar.clipsToBounds = true
        addSubview(view)
    }
    func setup(data: Rate){
        if data.avatar != nil {
            self.avatar.image = data.avatar
        }
        avatar.layer.cornerRadius = avatar.bounds.width/2
        print(avatar.layer.cornerRadius)
        self.dateLbl.text = data.date
        self.comment.text = data.comment
        self.rateLbl.text = AppFunc().ratingLabel(data.stars)
        self.nameLbl.text = data.name
    }
    func loadViewFromNib() -> UIView {
        let bundle = NSBundle(forClass: self.dynamicType)
        let nib = UINib(nibName: "CommentView", bundle: bundle)
        let view = nib.instantiateWithOwner(self, options: nil)[0] as! UIView
        return view
    }
    
}
