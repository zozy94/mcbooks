//
//  Horizontal.swift
//  JupViec
//
//  Created by Quang Anh on 4/10/16.
//  Copyright © 2016 Quang Anh. All rights reserved.
//


import UIKit
@IBDesignable
class Horizontal: UIView{
    var view: UIView!
    @IBOutlet var UIV: UIView!
    override init(frame: CGRect) {
        super.init(frame: frame)
        xibSetup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        xibSetup()
    }
    @IBInspectable var isWhite: Bool = Bool(){
        didSet {
            UIV.removeFromSuperview()
            let frheight = UIV.frame.size.height
            let frwid = UIV.frame.size.width
            let border = CALayer()
            let width = CGFloat(0.8)
            border.borderColor = UIColor.whiteColor().colorWithAlphaComponent(0.5).CGColor
            border.frame = CGRect(x: -2, y: frheight/2, width: frwid*2, height: frheight)
            border.borderWidth = width
            UIV.layer.addSublayer(border)
            UIV.layer.masksToBounds = true
            addSubview(view)
        }
    }
    func xibSetup() {
        view = loadViewFromNib()
        view.frame = bounds
        let frheight = UIV.frame.size.height
        let frwid = UIV.frame.size.width
        let border = CALayer()
        let width = CGFloat(0.8)
        border.borderColor = Color.lightgray.colorWithAlphaComponent(0.8).CGColor
        border.frame = CGRect(x: -2, y: frheight/2, width: frwid*2, height: frheight)
        border.borderWidth = width
        UIV.layer.addSublayer(border)
        UIV.layer.masksToBounds = true
        addSubview(view)
        

    }
    func loadViewFromNib() -> UIView {
        let bundle = NSBundle(forClass: self.dynamicType)
        let nib = UINib(nibName: "Horizontal", bundle: bundle)
        let view = nib.instantiateWithOwner(self, options: nil)[0] as! UIView
        return view
    }
}

