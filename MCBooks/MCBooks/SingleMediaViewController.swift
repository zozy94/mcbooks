//
//  SingleMediaViewController.swift
//  MCBooks
//
//  Created by Quang Anh on 6/20/16.
//  Copyright © 2016 Quang Anh. All rights reserved.
//

import UIKit
import AVFoundation

class SingleMediaViewController: UIViewController, NSURLSessionDownloadDelegate {
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var progressBar: UISlider!
    @IBOutlet weak var playTime: UILabel!
    @IBOutlet weak var remainTime: UILabel!
    @IBOutlet weak var centerIcon: UILabel!
    @IBOutlet weak var playButton: UIButton!
    @IBOutlet weak var indicator: UIActivityIndicatorView!
    
    @IBOutlet weak var bookInfoView: UIView!
    @IBOutlet weak var bookImage: UIImageView!
    @IBOutlet weak var bookName: UILabel!
    @IBOutlet weak var publisher: UILabel!
    @IBOutlet weak var author: UILabel!
    @IBOutlet weak var rating: UILabel!
    @IBOutlet weak var price: UILabel!
    @IBOutlet weak var priceView: UIView!
    
    @IBOutlet weak var progress: UIProgressView!
    @IBOutlet weak var downloadButton: UIButton!
    @IBOutlet weak var favButton: UIButton!
    @IBOutlet weak var repeatBtn: UIButton!
    
    var isRepeat = false
    var audioURL = ""
    var playerItem: AVPlayerItem!
    var player: AVPlayer!
    var isPlaying = false
    var Timer: NSTimer!
    var cellHeight:CGFloat = 0
    var playingID = ""
    var mediaData = Media()
    var bookData = Book()
    override func viewDidLoad() {
        self.navigationController!.interactivePopGestureRecognizer!.enabled = false
        bookInfoView.hidden = true
        topView.backgroundColor = Color.theme
        indicator.hidden = true
        mediaData = AppFunc().appDelegate.SelectedMedia
        mediaData.getOfflineURL()
        if(mediaData.isDownload){
            self.downloadButton.setTitle("", forState: .Normal)
        }
        if mediaData.isFav {
            favButton.setTitleColor(Color.red, forState: .Normal)
        } else {
            favButton.setTitleColor(UIColor.whiteColor(), forState: .Normal)
        }
        progress.setProgress(0.0, animated: true)  //set progressBar to 0 at start
        progress.hidden = true

        repeatBtn.setTitle("", forState: .Normal)

        getBookInfo()
        listen()
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(DetailViewController.ShowBook), name: "ShowBook", object: nil)
    }
    func listen(){
        if playingID != mediaData.id {
            playingID = mediaData.id
            if mediaData.isDownload {
                audioURL = "file://" + mediaData.offlineURL
            } else {
                audioURL = mediaData.url
            }
            loadMedia(audioURL)
        }
    }

    func updateSlider() {
        if player.currentItem?.status == AVPlayerItemStatus.ReadyToPlay {
        let time = Float(CMTimeGetSeconds(player.currentTime()))
        progressBar.value = time
        self.playTime.text = stringFromTimeInterval(NSTimeInterval(time))
        }
    }
    @IBAction func downloadClick(sender: AnyObject) {
        if mediaData.isDownload {
            let alert = UIAlertController(title: "Bạn có chắc chắn muốn xóa không", message: "", preferredStyle: .ActionSheet)
            alert.addAction(UIAlertAction(title: "Có", style: .Default, handler: {(action: UIAlertAction) in
                self.delete()
            }))
            alert.addAction(UIAlertAction(title: "Không", style: .Cancel, handler: nil))
            UIApplication.currentViewController()?.presentViewController(alert, animated: true, completion: nil)
        } else {
            down()
        }
    }
    @IBAction func favClick(sender: AnyObject) {
        if(!mediaData.isFav){
            mediaData.isFav = true
            favButton.setTitleColor(Color.red, forState: .Normal)
            AppFunc().appDelegate.updateFavouriteMedia(mediaData)
            let CN = ConnectServer()
            let params : [String: AnyObject] = [:]
            let alert = AlertController()
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)) {
                var status = 0
                var mess = ""
                if let json: AnyObject = CN.POSTwAuth(params,url: "favorite/medias/" + self.mediaData.id){
                    print(json)
                    status =  json.valueForKey("code") as! Int
                } else {
                    mess = "Không thể kết nối tới máy chủ"
                }
                dispatch_async(dispatch_get_main_queue()) {
                    if (status == 0){
                        alert.showAlert("Có lỗi xảy ra",mess: mess)
                    }
                }
            }
        } else {
            mediaData.isFav = false
            favButton.setTitleColor(UIColor.whiteColor(), forState: .Normal)
            let CN = ConnectServer()
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)) {
                var index = 0
                for item in AppFunc().appDelegate.FavouriteMedias {
                    if(item.id == self.mediaData.id){
                        AppFunc().appDelegate.FavouriteMedias.removeAtIndex(index)
                        break
                    }
                    index += 1
                }
                if let json: AnyObject = CN.DELETE("favorite/medias/" + self.mediaData.id){
                    print(json)
                } else {
                }
                dispatch_async(dispatch_get_main_queue()) {
                    
                }
            }
        }
    }
    
    @IBAction func playClick(sender: AnyObject) {
        if(!isPlaying){
            if(player == nil){
                loadMedia(audioURL)
            } else {
                isPlaying = true
                player.play()
                self.playButton.setTitle("", forState: .Normal)
            }
        } else {
            if player != nil {
                isPlaying = false
                player.pause()
                self.playButton.setTitle("", forState: .Normal)
            } else {
                loadMedia(audioURL)
            }
        }
    }
    @IBAction func repeatClick(sender: AnyObject) {
        if isRepeat {
            repeatBtn.setTitle("", forState: .Normal)
        } else {
            repeatBtn.setTitle("", forState: .Normal)
        }
        isRepeat = !isRepeat
    }

    func loadMedia(url: String){
        print(url)
        indicator.hidden = false
        centerIcon.hidden = true
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)) {
            if let sound = NSURL(string: url){
                let asset = AVURLAsset(URL: sound, options: nil)
                let audioDuration = asset.duration
                let audioDurationSeconds = CMTimeGetSeconds(audioDuration)
                self.progressBar.maximumValue = Float(audioDurationSeconds)
                asset.loadValuesAsynchronouslyForKeys(["tracks"], completionHandler: {
                    self.playerItem = AVPlayerItem(asset: asset)
                    self.player = AVPlayer(playerItem: self.playerItem)
                    self.player.play()
                    self.player.addObserver(self, forKeyPath: "status", options: NSKeyValueObservingOptions.Old, context: nil)
                    self.isPlaying = true
                    dispatch_async(dispatch_get_main_queue()) {
                        self.playButton.setTitle("", forState: .Normal)
                        self.remainTime.text = self.stringFromTimeInterval(audioDurationSeconds)
                        self.indicator.hidden = true
                        self.centerIcon.hidden = false
                        self.Timer = NSTimer.scheduledTimerWithTimeInterval(0.1, target: self, selector: #selector(SingleMediaViewController.updateSlider), userInfo: nil, repeats: true)
                        NSNotificationCenter.defaultCenter().addObserver(self,
                            selector: #selector(SingleMediaViewController.playerItemDidReachEnd(_:)),
                            name: AVPlayerItemDidPlayToEndTimeNotification,
                            object: self.player.currentItem)
                    }
                })
            }
        }
    }
    // catch changes to status
    override func observeValueForKeyPath(keyPath: String?, ofObject object: AnyObject?, change: [String : AnyObject]?, context: UnsafeMutablePointer<Void>) {
        if (keyPath == "status") {
            print(player.status)
        }
    }

    func stringFromTimeInterval(interval: NSTimeInterval) -> String {
        let interval = Int(interval)
        let seconds = interval % 60
        let minutes = (interval / 60) % 60
        return String(format: "%02d:%02d", minutes, seconds)
    }
    @IBAction func forwardClick(sender: AnyObject) {
        if player != nil {
            player.seekToTime(CMTimeMakeWithSeconds(CMTimeGetSeconds(player.currentTime()) + 5, player.currentTime().timescale))
            self.playTime.text = stringFromTimeInterval(NSTimeInterval(progressBar.value))
        }
    }
    @IBAction func backwardClick(sender: AnyObject) {
        if player != nil {
            player.seekToTime(CMTimeMakeWithSeconds(CMTimeGetSeconds(player.currentTime()) - 5, player.currentTime().timescale))
            self.playTime.text = stringFromTimeInterval(NSTimeInterval(progressBar.value))
        }
    }
    
    @IBAction func beginChangeTime(sender: AnyObject) {
        if player != nil {
            Timer.invalidate()
        }
    }
    @IBAction func endChangeTime(sender: AnyObject) {
    }
    @IBAction func touchUpEditg(sender: AnyObject) {
        if player != nil {
            indicator.hidden = false
            centerIcon.hidden = true
            player.pause()
            player.seekToTime(CMTimeMake(Int64(progressBar.value), 1))
            self.playTime.text = stringFromTimeInterval(NSTimeInterval(progressBar.value))
            player.play()
            self.Timer = NSTimer.scheduledTimerWithTimeInterval(0.1, target: self, selector: #selector(SingleMediaViewController.updateSlider), userInfo: nil, repeats: true)
            indicator.hidden = true
            centerIcon.hidden = false
        } else {
            progressBar.value = 0
        }

    }
    
    @IBAction func ChangeAudioTime(sender: AnyObject) {
        if player != nil {
            Timer.invalidate()
        }
    }
    func playerItemDidReachEnd(notification: NSNotification) {
        if isRepeat {
            player.pause()
            player.seekToTime(CMTimeMakeWithSeconds(CMTimeGetSeconds(player.currentTime()-player.currentTime()), player.currentTime().timescale))
            self.playTime.text = stringFromTimeInterval(NSTimeInterval(progressBar.value))
            player.play()
        }
    }

    func getBookInfo(){
        priceView.layer.cornerRadius = 3
        priceView.layer.borderColor = Color.theme.CGColor
        priceView.layer.borderWidth = 0.6

        let CN = ConnectServer()
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)) {
            var status = 0
            if let json: AnyObject = CN.GETPageBook("book/" + self.mediaData.bookID){
                status =  json.valueForKey("code") as! Int
                if (status == 1){
                    let result = json.valueForKey("result")! as AnyObject
                    self.bookData = AppFunc().fetchBookData(result)
                    if let thumb = AppFunc().getImage(URL.Thumb + self.bookData.image + "&width=250"){
                        self.bookData.thumb = thumb
                    }
                    dispatch_async(dispatch_get_main_queue()) {
                        if self.bookData.thumb != nil {
                            self.bookImage.image = self.bookData.thumb
                        }
                        self.bookName.text = self.bookData.name
                        self.publisher.text = self.bookData.publisher
                        self.author.text = self.bookData.author
                        self.rating.text = AppFunc().ratingLabel(self.bookData.avg_star)
                        self.price.text = AppFunc().decimalNum(self.bookData.price) + " vnđ"
                        self.bookInfoView.hidden = false
                    }
                }
            }
        }
    }
    @IBAction func viewBookButtonTapped(sender: AnyObject) {
        AppFunc().appDelegate.updateBook(self.bookData)
        AppFunc().appDelegate.SelectedBook = self.bookData
        performSegueWithIdentifier("bookDetail", sender: self)
    }
    
    
    var task : NSURLSessionTask!
    var percentageWritten:Float = 0.0
    var taskTotalBytesWritten = 0
    var taskTotalBytesExpectedToWrite = 0
    
    lazy var session : NSURLSession = {
        let config = NSURLSessionConfiguration.ephemeralSessionConfiguration()
        config.allowsCellularAccess = false
        let session = NSURLSession(configuration: config, delegate: self, delegateQueue: NSOperationQueue.mainQueue())
        return session
    }()
    func delete(){
        let path = NSSearchPathForDirectoriesInDomains(NSSearchPathDirectory.DocumentDirectory, NSSearchPathDomainMask.UserDomainMask, true)
        let documentDirectoryPath:String = path[0]
        let fileManager = NSFileManager()
        let destinationURLForFile = NSURL(fileURLWithPath: documentDirectoryPath.stringByAppendingString("/\(mediaData.id).mp3"))
        do {
            try fileManager.removeItemAtURL(destinationURLForFile)
            print("Deleted \(destinationURLForFile)")
        } catch{
            print("An error occurred while moving file to destination url")
        }
        mediaData.isDownload = false
        self.downloadButton.setTitle("", forState: .Normal)
    }
    func down() {
        let s = self.mediaData.url
        print("download \(s)")

        if let url = NSURL(string:s) {
            progress.hidden = false
            if self.task != nil {
                self.task = nil
            }

            let req = NSMutableURLRequest(URL:url)
            let task = self.session.downloadTaskWithRequest(req)
            self.task = task
            task.resume()
        }
    }
    
    func URLSession(session: NSURLSession, downloadTask: NSURLSessionDownloadTask, didWriteData bytesWritten: Int64, totalBytesWritten writ: Int64, totalBytesExpectedToWrite exp: Int64) {
        taskTotalBytesWritten = Int(writ)
        taskTotalBytesExpectedToWrite = Int(exp)
        percentageWritten = Float(taskTotalBytesWritten) / Float(taskTotalBytesExpectedToWrite)
        progress.progress = percentageWritten
    }
    
    func URLSession(session: NSURLSession, downloadTask: NSURLSessionDownloadTask, didResumeAtOffset fileOffset: Int64, expectedTotalBytes: Int64) {
        // unused in this example
    }
    
    func URLSession(session: NSURLSession, task: NSURLSessionTask, didCompleteWithError error: NSError?) {
        print("completed: error: \(error)")
    }
    
    // this is the only required NSURLSessionDownloadDelegate method
    
    func URLSession(session: NSURLSession, downloadTask: NSURLSessionDownloadTask, didFinishDownloadingToURL location: NSURL) {
        let path = NSSearchPathForDirectoriesInDomains(NSSearchPathDirectory.DocumentDirectory, NSSearchPathDomainMask.UserDomainMask, true)
        let documentDirectoryPath:String = path[0]
        let fileManager = NSFileManager()
        let destinationURLForFile = NSURL(fileURLWithPath: documentDirectoryPath.stringByAppendingString("/\(self.mediaData.id).mp3"))
        print(destinationURLForFile.path)
        if fileManager.fileExistsAtPath(destinationURLForFile.path!){
            print(destinationURLForFile.path)
        }
        else{
            do {
                try fileManager.moveItemAtURL(location, toURL: destinationURLForFile)
                print(destinationURLForFile.path)
                self.downloadButton.setTitle("", forState: .Normal)
                progress.hidden = true
                mediaData.isDownload = true
            }catch{
                print("An error occurred while moving file to destination url")
            }
        }
        
    }
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if(segue.identifier == "bookDetail"){
            let VC = segue.destinationViewController as! BookDetailViewController
            VC.bookDetail = AppFunc().appDelegate.SelectedBook
        }
    }
    
    override func viewWillDisappear(animated: Bool) {
        stop()
        self.navigationController!.interactivePopGestureRecognizer!.enabled = true
        NSNotificationCenter.defaultCenter().removeObserver(self, name: "ShowBook", object: nil)

    }
    func stop(){
        if player != nil {
            isPlaying = false
            self.playButton.setTitle("", forState: .Normal)
            player.pause()
        }
    }
}
