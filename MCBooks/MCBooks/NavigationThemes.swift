//
//  NavigationThemes.swift
//  LaundryApp
//
//  Created by ZoZy on 7/17/15.
//  Copyright (c) 2015 ZoZy. All rights reserved.
//

import UIKit

class NavigationThemes: UINavigationController, UIViewControllerTransitioningDelegate {
    
    var imageView: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Status bar white font
        self.navigationBar.tintColor = UIColor.whiteColor()
                let logo = UIImage(named: "logo_small.png")
                imageView = UIImageView(image:logo)
                imageView.frame = CGRectMake(AppFunc().appDelegate.screenWidth/2 - 60,-10,120,60)
                imageView.contentMode = .ScaleAspectFit
                self.navigationBar.addSubview(imageView)

    }
    func removeImage(){
        imageView.removeFromSuperview()
    }
}