//
//  LoginViewController.swift
//  MCBooks
//
//  Created by Quang Anh on 6/3/16.
//  Copyright © 2016 Quang Anh. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController, FBSDKLoginButtonDelegate, GIDSignInUIDelegate,GIDSignInDelegate {
    
    let loginView : FBSDKLoginButton = FBSDKLoginButton()
    var alert: AlertController!
    
    @IBOutlet weak var loginGGButton: UIButton!
    @IBOutlet weak var loginFBButton: UIButton!

    override func viewDidAppear(animated: Bool) {
        alert = AlertController()
        removeFbData()

    }
    override func viewDidLoad() {
        loginView.readPermissions = ["public_profile", "email"]
        loginView.delegate = self
        GIDSignIn.sharedInstance().disconnect()
        GIDSignIn.sharedInstance().uiDelegate = self
                GIDSignIn.sharedInstance().delegate = self

    }
    
    @IBAction func loginFB(sender: AnyObject) {
        alert.ShowIndicator()
        loginView.sendActionsForControlEvents(.TouchUpInside)
        
    }
    
    @IBAction func loginGG(sender: AnyObject) {
        alert.ShowIndicator()
        GIDSignIn.sharedInstance().signIn()

    }
    func loginButton(loginButton: FBSDKLoginButton!, didCompleteWithResult result: FBSDKLoginManagerLoginResult!, error: NSError!) {
        if ((error) != nil)
        {
            // Process error
            alert.DismissIndicator()
        }
        else if result.isCancelled {
            // Handle cancellations
            alert.DismissIndicator()
        }
        else {
            // If you ask for multiple permissions at once, you
            // should check if specific permissions missing
            if result.grantedPermissions.contains("email") && result.grantedPermissions.contains("public_profile")
            {
                // Do work.
                self.fetchFacebookProfile()
            }
        }
    }
    
    func loginButtonDidLogOut(loginButton: FBSDKLoginButton!) {
    }
    func removeFbData() {
        //Remove FB Data
        alert.DismissIndicator()
        let fbManager = FBSDKLoginManager()
        fbManager.logOut()
        FBSDKAccessToken.setCurrentAccessToken(nil)
    }
    
    func fetchFacebookProfile()
    {
        if FBSDKAccessToken.currentAccessToken() != nil {
            let req = FBSDKGraphRequest(graphPath: "me", parameters: ["fields":"email,name"], tokenString: FBSDKAccessToken.currentAccessToken().tokenString, version: nil, HTTPMethod: "GET")
            req.startWithCompletionHandler({ (connection, result, error : NSError!) -> Void in
                if(error == nil)
                {
                    print("result \(result)")
                    self.loginFBToServer(result)
                }
                else
                {
                    print("error \(error)")
                }
            })
        }
    }
    func loginFBToServer(result: AnyObject){
        let CN = ConnectServer()
        let FBid = result.valueForKey("id") as! String
        var params : [String: AnyObject] = [:]
        params["facebook_id"] = FBid
        params["name"] = result.valueForKey("name") as! String
        params["email"] = result.valueForKey("email") as! String
        params["avatar"] = "https://graph.facebook.com/\(FBid)/picture?width=150&height=150"
        let alert = AlertController()
        alert.ShowIndicator()
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)) {
            var status = 0
            var mess = ""
            print("Sending")
            if let json: AnyObject = CN.POST(params,url: URL.login){
                print(json)
                status =  json.valueForKey("code") as! Int
                print(status)
                if (status == 0){
//                    mess = json.valueForKey("message") as! String
                } else {
                    AppFunc().appDelegate.InitData(result,json: json)
                }
            } else {
                mess = "Không thể kết nối tới máy chủ"
            }
            dispatch_async(dispatch_get_main_queue()) {
                alert.DismissIndicator()
                if (status == 0){
                    alert.showAlert("Có lỗi xảy ra",mess: mess)
                    //                    self.passTF.setWarningOn("Sai mật khẩu hoặc tài khoản")
                } else {
                    self.performSegueWithIdentifier("loggedIn", sender: self)
                }
                self.alert.DismissIndicator()
            }
        }
        
    }
    func signInWillDispatch(signIn: GIDSignIn!, error: NSError!) {
        alert.DismissIndicator()
    }
    
    // Present a view that prompts the user to sign in with Google
    func signIn(signIn: GIDSignIn!,
                presentViewController viewController: UIViewController!) {
        self.presentViewController(viewController, animated: true, completion: nil)
    }
    
    // Dismiss the "Sign in with Google" view
    func signIn(signIn: GIDSignIn!,
                dismissViewController viewController: UIViewController!) {
        self.dismissViewControllerAnimated(true, completion: nil)
        alert.DismissIndicator()
        
    }
    func signIn(signIn: GIDSignIn!, didSignInForUser user: GIDGoogleUser!, withError error: NSError!) {
        print("didSignIn")
        if(GIDSignIn.sharedInstance().clientID != nil ){
            
            AppFunc().appDelegate.userData.googleID = user.userID
            AppFunc().appDelegate.userData.name = user.profile.name
            AppFunc().appDelegate.userData.email = user.profile.email
            if user.profile.hasImage{
                AppFunc().appDelegate.userData.avatarURL = user.profile.imageURLWithDimension(200).absoluteString
            }

            let CN = ConnectServer()
            let appDelegate = AppFunc().appDelegate
            var params : [String: AnyObject] = [:]
            params["google_id"] = appDelegate.userData.googleID
            params["name"] = appDelegate.userData.name
            params["email"] = appDelegate.userData.email
            params["avatar"] = appDelegate.userData.avatarURL
            let alert = AlertController()
            alert.ShowIndicator()
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)) {
                var status = 0
                var mess = ""
                print("Sending")
                if let json: AnyObject = CN.POST(params,url: URL.login){
                    print(json)
                    status =  json.valueForKey("code") as! Int
                    print(status)
                    if (status == 0){
                        //                    mess = json.valueForKey("message") as! String
                    } else {
                        
                        appDelegate.InitData(json)
                    }
                } else {
                    mess = "Không thể kết nối tới máy chủ"
                }
                dispatch_async(dispatch_get_main_queue()) {
                    alert.DismissIndicator()
                    if (status == 0){
                        alert.showAlert("Có lỗi xảy ra",mess: mess)
                    } else {
                        self.performSegueWithIdentifier("loggedIn", sender: self)
                    }
                    self.alert.DismissIndicator()
                }
            }
        }
        

    }

}
