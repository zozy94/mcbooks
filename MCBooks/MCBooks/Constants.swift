//
//  Constants.swift
//  JupViec
//
//  Created by Quang Anh on 4/9/16.
//  Copyright © 2016 Quang Anh. All rights reserved.
//

import UIKit

struct URL{
    static let Image = "http://quatang.mcbooks.vn/";
    static let Thumb = "http://quatang.mcbooks.vn/api/image/resize?path=";
    static let Server = "http://quatang.mcbooks.vn/api/";
    static let Gift = "http://quatang.mcbooks.vn/api/giftcode";
    static let FavMedia = "favorite/medias";
    static let favBook = "favorite/books";
    static let category = "categories";
    static let hotBook = "books/hot";
    static let newBook = "books/new";
    static let comingBook = "books/coming";
    static let login = "login";
    static let signup = "signup2";
    static let search = "search";
    static let checkAC = "checkAC";
    static let updatePhone = "updatePhone";
    static let uploadAva = "uploadAvatar";
    static let updateProfile = "me/about";
    static let getCredit = "getCredit";
    static let sendPromocode = "sendPromocode";
    static let findStaff = "suitStaffNew";
    static let regService = "regService";
    static let updateService = "updateService";
    static let getHistory = "getAllService";
    static let getPrice = "GetPrice";
    static let getNotification = "GetNotification";
    static let MarkNotification = "MarkNotification";
}
struct showType {
    static let HOT = 0;
    static let NEW = 1;
    static let COMING = 2;
    static let FAV = 3;
    static let CAT = 4;
    static let MEDIA = 5;
    static let VIDEO = 6;
    static let GIFT = 7;
    
}
struct Tab {
    static let BOOK = 0;
    static let AUDIO = 1;
    static let VIDEO = 2;
}
struct Constants {
    static let defaultAvatar: UIImage = UIImage(named:"user.jpg")!;
}

struct Icon{
    static let value: [String: Int] = [
        "user" :0xf007,
        "x" : 0xf00d,
        "v" : 0xf00c,
        "mail" : 0xf0e0,
        "email" : 0xf0e0,
        "phone" : 0xf095,
        "calendar" : 0xf073,
        "lock": 0xf023,
        "heart": 0xf004,
        "flag": 0xf024,
        "bell": 0xf0f3,
        "mapmark": 0xf041,
        "home": 0xf015,
        "edit": 0xf040,
        "back": 0xf053,
        "card": 0xf09d,
        "share": 0xf1e0,
        "promo": 0xf06b,
        "faq": 0xf059,
        "info": 0xf05a,
        "history": 0xf1da,
        "caretdown": 0xf0d7,
        "warning": 0xf071,
        "book": 0xf02d,
        "news": 0xf1ea
    ]
}

struct Color{
    static let theme = UIColor(netHex: 0x0173bc)
    static let themedark = UIColor(netHex: 0x004d7e)
    static let themelight = UIColor(netHex: 0x108edf)
    static let homebackground = UIColor(netHex: 0x0173bc)
    static let gray = UIColor(netHex: 0xb7b7b7)
    static let green = UIColor(netHex: 0x31b96e)
    static let red = UIColor(netHex: 0xb93131)
    static let blue = UIColor(netHex: 0x4388c9)
    static let darkgray = UIColor(netHex: 0x888888)
    static let lightgray = UIColor(netHex: 0xb7b7b7)
    static let menuColor = UIColor(netHex: 0xf3f3f3)
    static let yellow = UIColor(netHex: 0xbdb491)
    static let lightred = UIColor(netHex: 0xfdc6c6)
    static let orange = UIColor(netHex: 0xffa200)
}