//
//  HomeView.swift
//  MCBooks
//
//  Created by Quang Anh on 6/10/16.
//  Copyright © 2016 Quang Anh. All rights reserved.
//


import UIKit
@IBDesignable
class HomeView: UIView {
    var view: UIView!
    @IBOutlet weak var scrollView: UIScrollView!
    
    @IBOutlet weak var bannerImage: UIImageView!
    
    var nextYPosition:CGFloat = 0
    override init(frame: CGRect) {
        super.init(frame: frame)
        xibSetup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        xibSetup()
    }

    func xibSetup() {
        view = loadViewFromNib()
        view.frame = bounds
        scrollView.frame = AppFunc().appDelegate.viewFrame
        addSubview(view)
    }
    func reloadItem(){
        if scrollView != nil && bannerImage != nil {
        for subview in scrollView.subviews {
            subview.removeFromSuperview()
        }
        loadItem()
        }
        
    }
    func loadItem(){
        print("Load Home")
        let delegate = AppFunc().appDelegate
        nextYPosition = bannerImage.bounds.height
        let sliderView = SliderView(frame: CGRectMake(0,0,view.frame.width,nextYPosition))
        scrollView.addSubview(sliderView)
        
        let horizontalViewHeight = AppFunc().appDelegate.itemHeight + CGFloat(37)
        let new = delegate.newBooks()
        print("new \(new.count)")
        if(new.count > 0){
            let bookView = BookHorizontalView(frame: CGRectMake(0, nextYPosition, bounds.width, horizontalViewHeight))
            bookView.setupTitle("Sách Mới")
            scrollView.addSubview(bookView)
            for item in new {
                bookView.addBook(item)
            }
            bookView.type = showType.NEW
            nextYPosition += horizontalViewHeight
        }
        let hot = delegate.hotBooks()
        print("hot \(hot.count)")

        if(hot.count > 0){
            let bookView = BookHorizontalView(frame: CGRectMake(0, nextYPosition, bounds.width, horizontalViewHeight))
            bookView.setupTitle("Sách Hot")
            scrollView.addSubview(bookView)
            for item in hot {
                bookView.addBook(item)
            }
            bookView.type = showType.HOT
            nextYPosition += horizontalViewHeight
        }
        let coming = delegate.comingBooks()
        print("coming \(coming.count)")

        if(coming.count > 0){
            let bookView = BookHorizontalView(frame: CGRectMake(0, nextYPosition, bounds.width, horizontalViewHeight))
            bookView.setupTitle("Sách sắp phát hành")
            scrollView.addSubview(bookView)
            for item in coming {
                bookView.addBook(item)
            }
            bookView.type = showType.COMING
            nextYPosition += horizontalViewHeight
        }
        scrollView.contentSize = CGSizeMake(bounds.width, nextYPosition)
        scrollView.setNeedsDisplay()
        if let offset = AppFunc().appDelegate.lastScrollOffset {
            scrollView.contentOffset = offset
        }
    }
    func loadViewFromNib() -> UIView {
        let bundle = NSBundle(forClass: self.dynamicType)
        let nib = UINib(nibName: "HomeView", bundle: bundle)
        let view = nib.instantiateWithOwner(self, options: nil)[0] as! UIView
        return view
    }
    
}
