//
//  MediaItemCell.swift
//  MCBooks
//
//  Created by Quang Anh on 6/11/16.
//  Copyright © 2016 Quang Anh. All rights reserved.
//

import UIKit
import AVFoundation

class MediaItemCell: UITableViewCell, NSURLSessionDownloadDelegate {
    
    var id = ""
    var url = ""
    var type = 0
    var media: Media!
    @IBOutlet weak var iconLabel: UILabel!
    @IBOutlet weak var favButton: UIButton!
    @IBOutlet weak var downloadButton: UIButton!
    @IBOutlet weak var durationLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    
    func setup(media: Media){
        self.media = media
        if media.type == 0 {
//            let path = NSSearchPathForDirectoriesInDomains(NSSearchPathDirectory.DocumentDirectory, NSSearchPathDomainMask.UserDomainMask, true)
//            let documentDirectoryPath:String = path[0]
//            let fileManager = NSFileManager()
//            let destinationURLForFile = NSURL(fileURLWithPath: documentDirectoryPath.stringByAppendingString("/\(id).mp3"))
//            if fileManager.fileExistsAtPath(destinationURLForFile.path!){
//                self.downloadButton.setTitle("", forState: .Normal)
//                media.isDownload = true
//            } else {
//                media.isDownload = false
//            }

        if(media.isDownload){
            self.downloadButton.setTitle("", forState: .Normal)
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)) {
                if media.offlineURL != "" && NSURL(string: "file://" + media.offlineURL) != nil {
                    let sound = NSURL(string: "file://" + media.offlineURL)
                    let asset = AVURLAsset(URL: sound!, options: nil)
                    let audioDuration = asset.duration
                    let audioDurationSeconds = CMTimeGetSeconds(audioDuration)
                    dispatch_async(dispatch_get_main_queue()) {
                        self.durationLabel.text = AppFunc().stringFromTimeInterval(audioDurationSeconds)
                    }
                } else {
                    self.downloadButton.setTitle("", forState: .Normal)
                    media.isDownload = false
                    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)) {
                        if let sound = NSURL(string: media.url) {
                            let asset = AVURLAsset(URL: sound, options: nil)
                            let audioDuration = asset.duration
                            let audioDurationSeconds = CMTimeGetSeconds(audioDuration)
                            dispatch_async(dispatch_get_main_queue()) {
                                self.durationLabel.text = AppFunc().stringFromTimeInterval(audioDurationSeconds)
                            }
                        }
                    }
                }
            }
        } else {
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)) {
                if let sound = NSURL(string: media.url) {
                    let asset = AVURLAsset(URL: sound, options: nil)
                    let audioDuration = asset.duration
                    let audioDurationSeconds = CMTimeGetSeconds(audioDuration)
                    dispatch_async(dispatch_get_main_queue()) {
                            self.durationLabel.text = AppFunc().stringFromTimeInterval(audioDurationSeconds)
                    }
                }
            }
        }
        } else {
            self.durationLabel.text = ""
        }
        if media.isFav {
            favButton.setTitle("", forState: .Normal)

            favButton.setTitleColor(Color.red, forState: .Normal)
        } else {
            favButton.setTitle("", forState: .Normal)
            favButton.setTitleColor(Color.theme, forState: .Normal)
        }
        progressBar.setProgress(0.0, animated: true)  //set progressBar to 0 at start
        progressBar.hidden = true
        id = media.id
        type = media.type
        if(media.type == 0){
            iconLabel.text = ""
        } else {
            iconLabel.text = ""
        }
        self.nameLabel.text = media.name
    }
    func setPlaying(isPlaying:Bool){
        if(isPlaying){
            iconLabel.text = ""
            self.backgroundColor = Color.themelight.colorWithAlphaComponent(0.5)
        } else {
            self.backgroundColor = UIColor.whiteColor()
            if(type == 0){
                iconLabel.text = ""
            } else {
                iconLabel.text = ""
            }
        }
    }
    @IBAction func downloadAudio(sender: AnyObject) {
        if media.isDownload {
            let alert = UIAlertController(title: "Bạn có chắc chắn muốn xóa không", message: "", preferredStyle: .ActionSheet)
            alert.addAction(UIAlertAction(title: "Có", style: .Default, handler: {(action: UIAlertAction) in
                self.delete()
            }))
            alert.addAction(UIAlertAction(title: "Không", style: .Cancel, handler: nil))
            UIApplication.currentViewController()?.presentViewController(alert, animated: true, completion: nil)
        } else {
            down()
        }
    }
    @IBAction func favouriteAudio(sender: AnyObject) {
        if(!media.isFav){
            favButton.setTitleColor(Color.red, forState: .Normal)
            favButton.setTitle("", forState: .Normal)
            media.bookID = AppFunc().appDelegate.SelectedBook.id
            media.bookName = AppFunc().appDelegate.SelectedBook.name
            AppFunc().appDelegate.updateFavouriteMedia(media)
            let CN = ConnectServer()
            let params : [String: AnyObject] = [:]
            let alert = AlertController()
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)) {
                var status = 0
                var mess = ""
                if let json: AnyObject = CN.POSTwAuth(params,url: "favorite/medias/" + self.media.id){
                    print(json)
                    status =  json.valueForKey("code") as! Int
                } else {
                    mess = "Không thể kết nối tới máy chủ"
                }
                dispatch_async(dispatch_get_main_queue()) {
                    if (status == 0){
                        alert.showAlert("Có lỗi xảy ra",mess: mess)
                    }
                }
            }
        } else {
            favButton.setTitleColor(Color.theme, forState: .Normal)
            favButton.setTitle("", forState: .Normal)
            let CN = ConnectServer()
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)) {
                var index = 0
                for item in AppFunc().appDelegate.FavouriteMedias {
                    if(item.id == self.media.id){
                        AppFunc().appDelegate.FavouriteMedias.removeAtIndex(index)
                        break
                    }
                    index += 1
                }
                if let json: AnyObject = CN.DELETE("favorite/medias/" + self.media.id){
                    print(json)
                } else {
                }
                dispatch_async(dispatch_get_main_queue()) {
                    
                }
            }
        }
        media.isFav = !media.isFav
    }
    
    @IBOutlet weak var progressBar: UIProgressView!
//    @IBOutlet weak var progressCount: UILabel!
    var task : NSURLSessionTask!
    
    var percentageWritten:Float = 0.0
    var taskTotalBytesWritten = 0
    var taskTotalBytesExpectedToWrite = 0
    
    lazy var session : NSURLSession = {
        let config = NSURLSessionConfiguration.ephemeralSessionConfiguration()
        config.allowsCellularAccess = false
        let session = NSURLSession(configuration: config, delegate: self, delegateQueue: NSOperationQueue.mainQueue())
        return session
    }()
    
  
    func delete(){
        let path = NSSearchPathForDirectoriesInDomains(NSSearchPathDirectory.DocumentDirectory, NSSearchPathDomainMask.UserDomainMask, true)
        let documentDirectoryPath:String = path[0]
        let fileManager = NSFileManager()
        let destinationURLForFile = NSURL(fileURLWithPath: documentDirectoryPath.stringByAppendingString("/\(id).mp3"))
        do {
            try fileManager.removeItemAtURL(destinationURLForFile)
            print("Deleted \(destinationURLForFile)")
            media.isDownload = false
            self.downloadButton.setTitle("", forState: .Normal)
        } catch{
            print("An error occurred")
        }
    }
    func down() {
        if let url = NSURL(string:self.url) {
            progressBar.hidden = false
            if self.task != nil {
                self.task = nil
            }
            let req = NSMutableURLRequest(URL:url)
            let task = self.session.downloadTaskWithRequest(req)
            self.task = task
            task.resume()
        }
    }
    
    func URLSession(session: NSURLSession, downloadTask: NSURLSessionDownloadTask, didWriteData bytesWritten: Int64, totalBytesWritten writ: Int64, totalBytesExpectedToWrite exp: Int64) {
        taskTotalBytesWritten = Int(writ)
        taskTotalBytesExpectedToWrite = Int(exp)
        percentageWritten = Float(taskTotalBytesWritten) / Float(taskTotalBytesExpectedToWrite)
        progressBar.progress = percentageWritten
    }
    
    func URLSession(session: NSURLSession, downloadTask: NSURLSessionDownloadTask, didResumeAtOffset fileOffset: Int64, expectedTotalBytes: Int64) {
        // unused in this example
    }
    
    func URLSession(session: NSURLSession, task: NSURLSessionTask, didCompleteWithError error: NSError?) {
        print("completed: error: \(error)")
    }
    
    // this is the only required NSURLSessionDownloadDelegate method
    
    func URLSession(session: NSURLSession, downloadTask: NSURLSessionDownloadTask, didFinishDownloadingToURL location: NSURL) {
        let path = NSSearchPathForDirectoriesInDomains(NSSearchPathDirectory.DocumentDirectory, NSSearchPathDomainMask.UserDomainMask, true)
        let documentDirectoryPath:String = path[0]
        let fileManager = NSFileManager()
        let destinationURLForFile = NSURL(fileURLWithPath: documentDirectoryPath.stringByAppendingString("/\(id).mp3"))
        print(destinationURLForFile.path)
        if fileManager.fileExistsAtPath(destinationURLForFile.path!){
            print(destinationURLForFile.path)
        }
        else{
            do {
                try fileManager.moveItemAtURL(location, toURL: destinationURLForFile)
                print(destinationURLForFile.path)
                self.downloadButton.setTitle("", forState: .Normal)
                progressBar.hidden = true
                media.isDownload = true
                media.getOfflineURL()
            }catch{
                print("An error occurred while moving file to destination url")
            }
        }

    }
    

}
