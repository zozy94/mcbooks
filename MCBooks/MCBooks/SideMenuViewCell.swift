//
//  SideMenuViewCell.swift
//  LaundryApp
//
//  Created by ZoZy on 7/13/15.
//  Copyright (c) 2015 ZoZy. All rights reserved.
//

import UIKit

class SideMenuViewCell: UITableViewCell {
    
    @IBOutlet weak var logo: UILabel!
    @IBOutlet weak var title: UILabel!
    var segue = -1
}
