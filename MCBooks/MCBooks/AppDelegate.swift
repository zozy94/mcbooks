//
//  AppDelegate.swift
//  MCBooks
//
//  Created by Quang Anh on 6/1/16.
//  Copyright © 2016 Quang Anh. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    var userData = User()
    var tabBarPage = 0
    var kClientID = "119892903324-tm2g0nh5a8m403tnum828pkuvorlovd1.apps.googleusercontent.com"
    var screenWidth:CGFloat = 0
    var screenHeight:CGFloat = 0
    var itemWidth:CGFloat = 0
    var itemHeight:CGFloat = 0
    var favouriteBook = Array<String>()
    var FavouriteMedias = Array<Media>()
    var Books = Array<Book>()
    var Categories = Array<Category>()
    var Gifts = Array<Gift>()
    var SelectedBook = Book()
    var SelectedMedia = Media()
    var lastScrollOffset:CGPoint!
    var showTab = 0
    var isGridView = true
    var catName = ""
    var catID = ""
    var showID = 0
    var isShowSearch = false
    var viewFrame:CGRect!
    var imageCache = [String:String]()
    var bookCache = [String:AnyObject]()
    var mediaCache = [String:AnyObject]()

    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        // Initialize sign-in
//        var configureError: NSError?
//        GGLContext.sharedInstance().configureWithError(&configureError)
//        assert(configureError == nil, "Error configuring Google services: \(configureError)")
        GIDSignIn.sharedInstance().clientID = kClientID
//        GIDSignIn.sharedInstance().delegate = self

        // Override point for customization after application launch.
        UIApplication.sharedApplication().statusBarStyle = .LightContent
        let navigationBarAppearance = UINavigationBar.appearance()
        navigationBarAppearance.tintColor = UIColor.whiteColor()
        navigationBarAppearance.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.whiteColor()]
        navigationBarAppearance.barTintColor = Color.theme
        navigationBarAppearance.setBackgroundImage(UIImage(), forBarMetrics: UIBarMetrics.Default)
        navigationBarAppearance.shadowImage = UIImage()
        navigationBarAppearance.translucent = false
        let screenSize: CGRect = UIScreen.mainScreen().bounds
        screenWidth = screenSize.width
        screenHeight = screenSize.height
        itemWidth = screenWidth/3
        itemHeight = itemWidth*225/140
        return FBSDKApplicationDelegate.sharedInstance().application(application, didFinishLaunchingWithOptions: launchOptions)
    }
    func findBook(){
        print("Find")
    }
    func InitData(loginData: AnyObject, json: AnyObject){
        loadData(json)
        SaveUserData(loginData)
        getFavourite()
    }
    func InitData(json: AnyObject){
        loadData(json)
        SaveGoogleData()
        getFavourite()
    }
    func loadData(json: AnyObject){
        let resultData = json.valueForKey("result")! as AnyObject
        self.userData.token = resultData.valueForKey("access_token") as! String
        AppFunc().setNSObject(self.userData.token, key: "token")
        loadBookData(resultData)
        if let category = resultData.valueForKey("categories") as? Array<AnyObject> {
            AppFunc().setNSObject(category, key: "categories")
            for item in category {
                let cat = Category()
                cat.id = item.valueForKey("id") as! String
                cat.name = item.valueForKey("name") as! String
                cat.description = item.valueForKey("description") as! String
                self.Categories.append(cat)
            }
        }
    }
    func loadBookData(resultData: AnyObject){
        if let hotbook = resultData.valueForKey("hot_books") as? Array<AnyObject> {
            for item in hotbook {
                let book = AppFunc().fetchBookData(item)
                addToData(book.id,data: item)
                updateBook(book)
            }
        }
        if let newbook = resultData.valueForKey("new_books") as? Array<AnyObject> {
            for item in newbook {
                let book = AppFunc().fetchBookData(item)
                addToData(book.id,data: item)
                updateBook(book)
            }
        }
        if let Coming = resultData.valueForKey("coming_books") as? Array<AnyObject> {
            for item in Coming {
                let book = AppFunc().fetchBookData(item)
                addToData(book.id,data: item)
                updateBook(book)
            }
        }
    }
    func loadLocalBookData(){
        if let cache =  AppFunc().getNSObject("bookCache") {
            self.bookCache = cache as! [String:AnyObject]
            var count = 1
            for (_,data) in bookCache {
                print("book count \(count)")
                count += 1
                Books.append(AppFunc().fetchBookData(data))
            }
        } else {
            print("no book cache")
        }

        if let cache =  AppFunc().getNSObject("imageCache") {
            self.imageCache = cache as! [String:String]
            print("image count \(imageCache.count)")
        } else {
            print("no image cache")
        }
        if let category = AppFunc().getNSObject("categories") as? Array<AnyObject> {
            for item in category {
                let cat = Category()
                cat.id = item.valueForKey("id") as! String
                cat.name = item.valueForKey("name") as! String
                cat.description = item.valueForKey("description") as! String
                self.Categories.append(cat)
            }
        }
        AppFunc().reloadBookData()
    }
    func SaveUserData(json: AnyObject){
            if let facebookId = json.valueForKey("id") as? String {
                print(facebookId)
                userData.facebookId = facebookId
                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)) {
                    self.userData.avatarURL = "https://graph.facebook.com/\(self.userData.facebookId)/picture?width=150&height=150"
                    if let image = self.getCachedImage("userAvatar") {
                        self.userData.avatar = image
                    } else {
                        if let image = AppFunc().getImage(self.userData.avatarURL) {
                            self.userData.avatar = image
                            self.cachedImage("userAvatar", image: image)
                        }
                    }
                }
            }
            userData.name = json.valueForKey("name") as! String
            userData.email = json.valueForKey("email") as! String
            AppFunc().setNSObject(json, key: "userData")
    }
    func SaveGoogleData(){
        let ggData = googleData()
        ggData.name = userData.name
        ggData.email = userData.email
        ggData.avatarURL = userData.avatarURL
        ggData.googleID = userData.googleID
        ggData.token = userData.token
        ggData.phone = userData.phone
        AppFunc().setNSObject(ggData, key: "googleData")
    }
    func LoadGoogleData(){
        let ggData = AppFunc().getNSObject("googleData") as! googleData
        userData.name = ggData.name
        userData.email = ggData.email
        userData.avatarURL = ggData.avatarURL
        userData.googleID = ggData.googleID
        userData.token = ggData.token
        userData.phone = ggData.phone
        print(userData.name)
    }
    func loadData() -> Bool{
        if let userData = AppFunc().getNSObject("userData") {
            SaveUserData(userData)
            if let token = AppFunc().getNSObject("token") as? String {
                self.userData.token = token
                print("Loading book data...")
                loadLocalBookData()
                loadFavouriteBook()
                loadFavouriteAudio()
                print("Done load book data")
            } else {
                return false
            }
            return true
        } else if let ggData = AppFunc().getNSObject("googleData") as? googleData {
            print("load googleData")
            userData.name = ggData.name
            userData.email = ggData.email
            userData.avatarURL = ggData.avatarURL
            userData.googleID = ggData.googleID
            userData.token = ggData.token
            userData.phone = ggData.phone
            if let token = AppFunc().getNSObject("token") as? String {
                self.userData.token = token
                print("Loading book data...")
                loadLocalBookData()
                loadFavouriteBook()
                loadFavouriteAudio()
                print("Done load book data")
                if let image = getCachedImage("userAvatar") {
                    self.userData.avatar = image
                } else {
                    if let image = AppFunc().getImage(self.userData.avatarURL) {
                        self.userData.avatar = image
                        cachedImage("userAvatar", image: image)
                    }
                }
            } else {
                return false
            }
        return true
        }
        return false
    }
    func getCachedImage(url: String) -> UIImage? {
        print("getting cache \(url)")
        if imageCache[url] != nil {
            let data = NSData(base64EncodedString: imageCache[url]!, options: NSDataBase64DecodingOptions(rawValue: 0))
            return UIImage(data: data!)
        }
        return nil
    }
    func cachedImage(url: String, image: UIImage){
        imageCache[url] =  NSData(data:UIImageJPEGRepresentation(image, 1.0)!).base64EncodedStringWithOptions(NSDataBase64EncodingOptions.init(rawValue: 0))
    }
    func addToData(id: String, data: AnyObject){
        bookCache[id] = data
        print("added book \(bookCache.count)")
    }

    func hotBooks() -> Array<Book>{
        var array = Array<Book>()
        for item in Books {
            if item.isHot {
                array.append(item)
            }
        }
        return array
    }
    func newBooks() -> Array<Book>{
        var array = Array<Book>()
        for item in Books {
            if item.isNew {
                array.append(item)
            }
        }
        return array
    }
    func comingBooks() -> Array<Book>{
        var array = Array<Book>()
        for item in Books {
            if item.isComing {
                array.append(item)
            }
        }
        return array
    }
    func favouriteBooks() -> Array<Book> {
        var array = Array<Book>()
        for id in favouriteBook {
            for item in Books {
                if item.id == id {
                    array.append(item)
                    break
                }
            }
        }

        return array
    }
    func favouriteMedia(type: Int) -> Array<Media> {
        var array = Array<Media>()
        for item in FavouriteMedias {
            print("type \(item.type)")
            if item.type == type {
                array.append(item)
            }
        }
        print("type \(type) count \(array.count)")

        return array
    }
    func addFavourite(id: String) {
        if !favouriteBook.contains(id){
            favouriteBook.append(id)
        }
    }
    func removeFavourite(id: String) {
        let filtered = favouriteBook.filter{$0 != id}
        favouriteBook = filtered
    }

    func getBook(id: String) -> Book {
        for item in Books {
            if item.id == id {
                return item
            }
        }
        return Book()
    }
    func updateBook(book: Book){
        var check = false
        for i in 0..<Books.count {
            if Books[i].id == book.id {
                Books[i] = book
                check = true
                break
            }
        }
        if !check {
            Books.append(book)
        }
    }
    func updateFavouriteMedia(media: Media){
        var check = false
        for i in 0..<FavouriteMedias.count {
            if FavouriteMedias[i].id == media.id {
                FavouriteMedias[i] = media
                check = true
                break
            }
        }
        if !check {
            FavouriteMedias.append(media)
        }
    }
    func getFavourite(){
        let CN = ConnectServer()
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)) {
            var status = 0
            if let json: AnyObject = CN.GETPageBook(URL.favBook+"?page=1"){
                status =  json.valueForKey("code") as! Int
                if (status == 1){
                    if let result = json.valueForKey("result") as? Array<AnyObject> {
                        for item in result {
                            let book = AppFunc().fetchBookData(item)
                            book.isFavourite = true
                            self.updateBook(book)
                            self.addFavourite(book.id)
                        }
                    }
                }
            }
        }
    }
    func loadFavouriteAudio(){
        if let cache =  AppFunc().getNSObject("favouriteMedia") {
            self.mediaCache = cache as! [String:AnyObject]
            var count = 1
            for (_,data) in mediaCache {
                print("media count \(count)")
                count += 1
                let media = AppFunc().fetchMediaData(data)
                self.FavouriteMedias.append(media)
            }
        } else {
            print("no media cache")
        }
//        if let json = AppFunc().getNSObject("favouriteAudio") {
//            if let result = json.valueForKey("result") as? Array<AnyObject> {
//                print(result.count)
//                if result.count > 0 {
//                    for item in result {
//                        let book = AppFunc().fetchMediaData(item)
//                        self.FavouriteMedias.append(book)
//                    }
//                }
//            }
//        }
    }

    func loadFavouriteBook(){
        if let data = AppFunc().getNSObject("favouriteBook") as? Array<String> {
            favouriteBook = data
//            if let result = json.valueForKey("result") as? Array<AnyObject> {
//                print(result.count)
//                if result.count > 0 {
//                    for item in result {
//                        let book = AppFunc().fetchBookData(item)
//                        self.addFavourite(book.id)
//                        self.updateBook(book)
//                    }
//                }
//            }
        }
    }
    func logout(){
        let alert = UIAlertController(title: "Hết phiên đăng nhập", message: "", preferredStyle: .Alert)
        alert.addAction(UIAlertAction(title: "Đăng nhập lại", style: UIAlertActionStyle.Default, handler: {(action: UIAlertAction) in
            self.clearData()
            let storyboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewControllerWithIdentifier("loginView")
            UIApplication.currentViewController()!.presentViewController(vc,animated: true,completion: nil)
        }))
        UIApplication.currentViewController()!.presentViewController(alert, animated: true, completion: nil)
    }

    func clearData(){
        userData = User()
        Books.removeAll()
        FavouriteMedias.removeAll()
        Categories.removeAll()
        AppFunc().deleteNSObject("token")
        AppFunc().deleteNSObject("imageCache")
        AppFunc().deleteNSObject("bookCache")
        AppFunc().deleteNSObject("favouriteMedia")
        AppFunc().deleteNSObject("favouriteBook")

    }
    
//    func saveFavouriteBook(json:AnyObject){
//        AppFunc().setNSObject(json, key: "favouriteBook")
//    }
//    func saveFavouriteMedia(json: AnyObject){
//        AppFunc().setNSObject(json, key: "favouriteAudio")
//    }

    func isFavouriteMedia(id: String) -> Bool {
        for item in self.FavouriteMedias {
            if(item.id == id){
                return true
            }
        }
        return false
    }
    func isFavouriteBook(id: String) -> Bool {
        return favouriteBook.contains(id)
    }
    func setFavourite(id: String, isFav: Bool){
        if isFav {
            var check = true
            for bookid in favouriteBook {
                if id == bookid {
                    check = false
                    break
                }
            }
            if check {
                favouriteBook.append(id)
            }
        } else {
            removeFavourite(id)
        }
    }
    func saveCache(){
        do {
            let bookJson = try NSJSONSerialization.dataWithJSONObject(bookCache, options: NSJSONWritingOptions.PrettyPrinted)
            let bookResult: AnyObject? = try? NSJSONSerialization.JSONObjectWithData(bookJson, options: NSJSONReadingOptions(rawValue: 0))
            AppFunc().setNSObject(bookResult!, key: "bookCache")
        } catch let error as NSError {
            print("Book")
            print(error)
        }
        do {
            let jsonData = try NSJSONSerialization.dataWithJSONObject(imageCache, options: NSJSONWritingOptions.PrettyPrinted)
            let jsonResult: AnyObject? = try? NSJSONSerialization.JSONObjectWithData(jsonData, options: NSJSONReadingOptions(rawValue: 0))
            AppFunc().setNSObject(jsonResult!, key: "imageCache")
        } catch let error as NSError {
            print("image")
            print(error)
        }
        do {
            var mediatemp = [String:AnyObject]()
            for item in FavouriteMedias {
                if let cache = mediaCache[item.id] {
                    mediatemp[item.id] = cache
                }
            }
            let jsonData = try NSJSONSerialization.dataWithJSONObject(mediatemp, options: NSJSONWritingOptions.PrettyPrinted)
            let jsonResult: AnyObject? = try? NSJSONSerialization.JSONObjectWithData(jsonData, options: NSJSONReadingOptions(rawValue: 0))
            AppFunc().setNSObject(jsonResult!, key: "favouriteMedia")
        } catch let error as NSError {
            print("media")
            print(error)
        }
        AppFunc().setNSObject(favouriteBook, key: "favouriteBook")
        
    }
    func applicationWillResignActive(application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
        print("save cache")
        saveCache()
    }

    func applicationWillEnterForeground(application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        FBSDKAppEvents.activateApp()

    }

    func applicationWillTerminate(application: UIApplication) {
        print("save cache")
        saveCache()
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    func application(application: UIApplication,
                     openURL url: NSURL,
                             sourceApplication: String?,
                             annotation: AnyObject?) -> Bool {
//            var options: [String: AnyObject] = [UIApplicationOpenURLOptionsSourceApplicationKey: sourceApplication!,
//                                                UIApplicationOpenURLOptionsAnnotationKey: ""]
            return FBSDKApplicationDelegate.sharedInstance().application(
                application,
                openURL: url,
                sourceApplication: sourceApplication,
                annotation: annotation) || GIDSignIn.sharedInstance().handleURL(url, sourceApplication: sourceApplication, annotation: annotation)
    }
//    func signIn(signIn: GIDSignIn!, didSignInForUser user: GIDGoogleUser!,
//                withError error: NSError!) {
//        if (error == nil) {
//            // Perform any operations on signed in user here.
//            userData.googleID = user.userID                  // For client-side use only!
//            //            let idToken = user.authentication.idToken // Safe to send to the server
//            userData.name = user.profile.name
//            //            let givenName = user.profile.givenName
//            //            let familyName = user.profile.familyName
//            userData.email = user.profile.email
//            if user.profile.hasImage{
//                userData.avatarURL = user.profile.imageURLWithDimension(200).absoluteString
//                userData.avatar = AppFunc().getImage(userData.avatarURL)
//            }
//            
//        } else {
//            print("\(error.localizedDescription)")
//        }
//
//    }
    func signIn(signIn: GIDSignIn!, didDisconnectWithUser user:GIDGoogleUser!,
                withError error: NSError!) {
        // Perform any operations when the user disconnects from app here.
        // ...
    }
}
extension UIColor {
    convenience init(red: Int, green: Int, blue: Int) {
        assert(red >= 0 && red <= 255, "Invalid red component")
        assert(green >= 0 && green <= 255, "Invalid green component")
        assert(blue >= 0 && blue <= 255, "Invalid blue component")
        
        self.init(red: CGFloat(red) / 255.0, green: CGFloat(green) / 255.0, blue: CGFloat(blue) / 255.0, alpha: 1.0)
    }
    
    convenience init(netHex:Int) {
        self.init(red:(netHex >> 16) & 0xff, green:(netHex >> 8) & 0xff, blue:netHex & 0xff)
    }
}
extension UIApplication {
    class func currentViewController(base: UIViewController? = UIApplication.sharedApplication().keyWindow?.rootViewController) -> UIViewController? {
        if let nav = base as? UINavigationController {
            return currentViewController(nav.visibleViewController)
        }
        if let tab = base as? UITabBarController {
            if let selected = tab.selectedViewController {
                return currentViewController(selected)
            }
        }
        if let presented = base?.presentedViewController {
            return currentViewController(presented)
        }
        return base
    }
}

extension UIApplication {
    class func topViewController(base: UIViewController? = UIApplication.sharedApplication().keyWindow?.rootViewController) -> UIViewController? {
        if let nav = base as? UINavigationController {
            return topViewController(nav.visibleViewController)
        }
        if let tab = base as? UITabBarController {
            if let selected = tab.selectedViewController {
                return topViewController(selected)
            }
        }
        if let presented = base?.presentedViewController {
            return topViewController(presented)
        }
        return base
    }
}
extension Dictionary {
    
    /// Build string representation of HTTP parameter dictionary of keys and objects
    ///
    /// This percent escapes in compliance with RFC 3986
    ///
    /// http://www.ietf.org/rfc/rfc3986.txt
    ///
    /// :returns: String representation in the form of key1=value1&key2=value2 where the keys and values are percent escaped
    
    func stringFromHttpParameters() -> String {
        let parameterArray = self.map { (key, value) -> String in
            let percentEscapedKey = (key as! String).stringByAddingPercentEncodingForURLQueryValue()!
            let percentEscapedValue = "\(value)".stringByAddingPercentEncodingForURLQueryValue()!
            return "\(percentEscapedKey)=\(percentEscapedValue)"
        }
        
        return parameterArray.joinWithSeparator("&")
    }
    
}
extension String {
    
    /// Percent escapes values to be added to a URL query as specified in RFC 3986
    ///
    /// This percent-escapes all characters besides the alphanumeric character set and "-", ".", "_", and "~".
    ///
    /// http://www.ietf.org/rfc/rfc3986.txt
    ///
    /// :returns: Returns percent-escaped string.
    
    func stringByAddingPercentEncodingForURLQueryValue() -> String? {
        let allowedCharacters = NSCharacterSet(charactersInString: "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789-._~")
        
        return self.stringByAddingPercentEncodingWithAllowedCharacters(allowedCharacters)
    }
    
}
extension UIImage {
    func imageByCroppingImage(size: CGSize) -> UIImage {
        let newCropWidth, newCropHeight : CGFloat;
        
        if(self.size.width < self.size.height) {
            if (self.size.width < size.width) {
                newCropWidth = self.size.width;
            }
            else {
                newCropWidth = size.width;
            }
            newCropHeight = (newCropWidth * size.height)/size.width;
        } else {
            if (self.size.height < size.height) {
                newCropHeight = self.size.height;
            }
            else {
                newCropHeight = size.height;
            }
            newCropWidth = (newCropHeight * size.width)/size.height;
        }
        
        let x = self.size.width / 2 - newCropWidth / 2;
        let y = self.size.height / 2 - newCropHeight / 2;
        
        let cropRect = CGRectMake(x, y, newCropWidth, newCropHeight);
        let imageRef = CGImageCreateWithImageInRect(self.CGImage, cropRect);
        
        let croppedImage : UIImage = UIImage(CGImage: imageRef!, scale: 0, orientation: self.imageOrientation);
        
        return croppedImage;
    }
}
extension UIView {
    // Name this function in a way that makes sense to you...
    // slideFromLeft, slideRight, slideLeftToRight, etc. are great alternative names
    func slideInFromLeft(duration: NSTimeInterval = 1.0, completionDelegate: AnyObject? = nil) {
        // Create a CATransition animation
        let slideInFromLeftTransition = CATransition()
        
        // Set its callback delegate to the completionDelegate that was provided (if any)
        if let delegate: AnyObject = completionDelegate {
            slideInFromLeftTransition.delegate = delegate
        }
        
        // Customize the animation's properties
        slideInFromLeftTransition.type = kCATransitionPush
        slideInFromLeftTransition.subtype = kCATransitionFromLeft
        slideInFromLeftTransition.duration = duration
        slideInFromLeftTransition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
        slideInFromLeftTransition.fillMode = kCAFillModeRemoved
        // Add the animation to the View's layer
        self.layer.addAnimation(slideInFromLeftTransition, forKey: "slideInFromLeftTransition")
    }
    
    func slideInFromRight(duration: NSTimeInterval = 1.0, completionDelegate: AnyObject? = nil) {
        // Create a CATransition animation
        let slideInFromRightTransition = CATransition()
        
        // Set its callback delegate to the completionDelegate that was provided (if any)
        if let delegate: AnyObject = completionDelegate {
            slideInFromRightTransition.delegate = delegate
        }
        
        // Customize the animation's properties
        slideInFromRightTransition.type = kCATransitionPush
        slideInFromRightTransition.subtype = kCATransitionFromRight
        slideInFromRightTransition.duration = duration
        slideInFromRightTransition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
        slideInFromRightTransition.fillMode = kCAFillModeRemoved
        
        // Add the animation to the View's layer
        self.layer.addAnimation(slideInFromRightTransition, forKey: "slideInFromRightTransition")
    }
    
}

