//
//  BookHorizontalView.swift
//  MCBooks
//
//  Created by Quang Anh on 6/10/16.
//  Copyright © 2016 Quang Anh. All rights reserved.
//

import UIKit
@IBDesignable
class BookHorizontalView: UIView {
    var view: UIView!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var title: UILabel!
    var type = 0
    var listBook = Array<Book>()
    override init(frame: CGRect) {
        super.init(frame: frame)
        xibSetup()
    }
    func setupTitle(title: String){
        self.title.text = title
    }
    func addBook(item: Book){
        print("add \(item.name)")
        let itemWidth = AppFunc().appDelegate.itemWidth
        let itemHeight = AppFunc().appDelegate.itemHeight
        let position = CGFloat(listBook.count)*itemWidth
        let bookItem = BookItemView(frame: CGRectMake(position, 0, itemWidth, itemHeight))
        bookItem.setup(item)
        scrollView.addSubview(bookItem)
        listBook.append(item)
        scrollView.contentSize = CGSize(width: CGFloat(listBook.count)*itemWidth, height: itemHeight)
        self.scrollView.setNeedsDisplay()
    }
    
    @IBAction func ShowAll(sender: AnyObject) {
        AppFunc().appDelegate.catName = self.title.text!
        AppFunc().appDelegate.showID = type
        NSNotificationCenter.defaultCenter().postNotificationName("ShowAll", object: nil, userInfo: nil)
    }
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        xibSetup()
    }
    
    func xibSetup() {
        view = loadViewFromNib()
        view.frame = bounds
        addSubview(view)
    }
    func loadViewFromNib() -> UIView {
        let bundle = NSBundle(forClass: self.dynamicType)
        let nib = UINib(nibName: "BookHorizontalView", bundle: bundle)
        let view = nib.instantiateWithOwner(self, options: nil)[0] as! UIView
        return view
    }
}