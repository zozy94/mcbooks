//
//  WebView.swift
//  MCBooks
//
//  Created by Quang Anh on 6/10/16.
//  Copyright © 2016 Quang Anh. All rights reserved.
//

import UIKit
import WebKit

@IBDesignable
class WebView: UIView, UIWebViewDelegate {
    var view: UIView!
    
    @IBOutlet weak var indicator: UIActivityIndicatorView!
    @IBOutlet weak var webView: UIWebView!
    override init(frame: CGRect) {
        super.init(frame: frame)
        xibSetup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        xibSetup()
    }
    
    func xibSetup() {
        view = loadViewFromNib()
        view.frame = bounds
        webView.delegate = self
        addSubview(view)
    }
    func setPDF(link: NSURL){
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)) {
        self.webView.loadRequest(NSURLRequest(URL: link))
        }
    }

    func clean(){
        print("Clean memory")
        self.webView.loadRequest(NSURLRequest(URL: NSURL(string: "about:blank")!))
        self.webView.stopLoading()
        self.webView.delegate = nil
        self.webView.removeFromSuperview()
        self.webView = nil
        NSURLCache.sharedURLCache().removeAllCachedResponses()
        NSURLCache.sharedURLCache().diskCapacity = 0
        NSURLCache.sharedURLCache().memoryCapacity = 0
        self.webView = nil
    }
    func webViewDidFinishLoad(webView: UIWebView) {
        if self.indicator != nil {
            self.indicator.hidden = true
        }
    }
    func loadViewFromNib() -> UIView {
        let bundle = NSBundle(forClass: self.dynamicType)
        let nib = UINib(nibName: "WebView", bundle: bundle)
        let view = nib.instantiateWithOwner(self, options: nil)[0] as! UIView
        return view
    }
}