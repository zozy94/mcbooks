//
//  LaunchScreenController.swift
//  MCBooks
//
//  Created by Quang Anh on 6/10/16.
//  Copyright © 2016 Quang Anh. All rights reserved.
//

import UIKit
class LaunchScreenController: UIViewController {
    var checkData = false
    let NO_CONNECT = 2
    let EXPIRED = 0
    let OK = 1
    var count_attemping = 0
    let appDelegate = AppFunc().appDelegate
    override func viewWillAppear(animated: Bool) {
        checkData = appDelegate.loadData()
        print(checkData)
    }
    override func viewDidAppear(animated: Bool) {
        if(checkData){
            appDelegate.getFavourite()
            let storyboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewControllerWithIdentifier("mainView")
            self.presentViewController(vc,animated: true,completion: nil)
        } else {
            let storyboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewControllerWithIdentifier("loginView")
            self.presentViewController(vc,animated: true,completion: nil)
        }
    }
}
