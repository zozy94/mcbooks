//
//  AudioViewController.swift
//  MCBooks
//
//  Created by Quang Anh on 6/18/16.
//  Copyright © 2016 Quang Anh. All rights reserved.
//

import UIKit
import AVFoundation

class AudioViewController: UIViewController {
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var progressBar: UISlider!
    @IBOutlet weak var playTime: UILabel!
    @IBOutlet weak var remainTime: UILabel!
    @IBOutlet weak var centerIcon: UILabel!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var playButton: UIButton!
    @IBOutlet weak var indicator: UIActivityIndicatorView!
    @IBOutlet weak var repeatBtn: UIButton!
    var isRepeat = false
    var audioURL = ""
    var data = Array<Media>()
    var playerItem: AVPlayerItem!
    var player: AVPlayer!
    var isPlaying = false
    var Timer: NSTimer!
    var cellHeight:CGFloat = 0
    var playingID = ""
    var playIndex = 0
    override func viewDidLoad() {
        self.navigationController!.interactivePopGestureRecognizer!.enabled = false
        topView.backgroundColor = Color.theme
        indicator.hidden = true
        cellHeight = AppFunc().appDelegate.screenHeight/10
        data = AppFunc().appDelegate.SelectedBook.audio()
        tableView.separatorStyle = .None
        repeatBtn.setTitle("", forState: .Normal)

    }
    override func viewWillAppear(animated: Bool) {
        tableView.reloadData()
    }
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data.count
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let rowData = data[indexPath.row]
        if playingID != rowData.id {
            playIndex = indexPath.row
            playingID = rowData.id
            if rowData.isDownload {
                audioURL = "file://" + rowData.offlineURL
            } else {
                audioURL = rowData.url
            }
            loadMedia(audioURL)
            tableView.reloadData()
        }
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return cellHeight
    }
    func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
        cell.selectionStyle = UITableViewCellSelectionStyle.None
    }
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("mediaItemCell") as! MediaItemCell
        data[indexPath.row].getOfflineURL()
        data[indexPath.row].isFav = AppFunc().appDelegate.isFavouriteMedia(data[indexPath.row].id)
        if playingID == data[indexPath.row].id {
            cell.setPlaying(true)
        } else {
            cell.setPlaying(false)
        }
        cell.url = data[indexPath.row].url
        cell.favButton.tag = indexPath.row
        cell.downloadButton.tag = indexPath.row
        cell.setup(data[indexPath.row])
        return cell
    }

    func updateSlider() {
        if player.currentItem?.status == AVPlayerItemStatus.ReadyToPlay {
            let time = Float(CMTimeGetSeconds(player.currentTime()))
            progressBar.value = time
            self.playTime.text = stringFromTimeInterval(NSTimeInterval(time))
        }
    }
    
    @IBAction func playClick(sender: AnyObject) {
        if audioURL == "" && data.count > 0 {
            let indexPath = NSIndexPath(forRow: 0, inSection: 0);
            self.tableView.selectRowAtIndexPath(indexPath, animated: false, scrollPosition: UITableViewScrollPosition.None)
            self.tableView(self.tableView, didSelectRowAtIndexPath: indexPath)
            return
        }

        if(!isPlaying){
            if(player == nil){
                loadMedia(audioURL)
            } else {
                isPlaying = true
                player.play()
                self.playButton.setTitle("", forState: .Normal)
            }
        } else {
            if player != nil {
                isPlaying = false
                player.pause()
                self.playButton.setTitle("", forState: .Normal)
            } else {
                loadMedia(audioURL)
            }
        }
    }
    func loadMedia(url: String){
        print(url)
        indicator.hidden = false
        centerIcon.hidden = true
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)) {
            if let sound = NSURL(string: url) {
                let asset = AVURLAsset(URL: sound, options: nil)
                let audioDuration = asset.duration
                let audioDurationSeconds = CMTimeGetSeconds(audioDuration)
                self.progressBar.maximumValue = Float(audioDurationSeconds)
                asset.loadValuesAsynchronouslyForKeys(["tracks"], completionHandler: {
                    self.playerItem = AVPlayerItem(asset: asset)
                    self.player = AVPlayer(playerItem: self.playerItem)
                    self.player.play()
                    self.isPlaying = true
                    
                    dispatch_async(dispatch_get_main_queue()) {
                        self.playButton.setTitle("", forState: .Normal)
                        self.remainTime.text = self.stringFromTimeInterval(audioDurationSeconds)
                        self.indicator.hidden = true
                        self.centerIcon.hidden = false
                        self.Timer = NSTimer.scheduledTimerWithTimeInterval(0.1, target: self, selector: #selector(AudioViewController.updateSlider), userInfo: nil, repeats: true)
                        
                        NSNotificationCenter.defaultCenter().addObserver(self,
                            selector: #selector(AudioViewController.playerItemDidReachEnd(_:)),
                            name: AVPlayerItemDidPlayToEndTimeNotification,
                            object: self.player.currentItem)
                    }
                })
            } else {
                dispatch_async(dispatch_get_main_queue()) {
                    let alert = AlertController()
                    alert.showAlert("Lỗi", mess: "Audio không tồn tại hoặc đã bị xóa")
                }
            }
        }
        

    }
    func stringFromTimeInterval(interval: NSTimeInterval) -> String {
        let interval = Int(interval)
        let seconds = interval % 60
        let minutes = (interval / 60) % 60
        return String(format: "%02d:%02d", minutes, seconds)
    }
    @IBAction func forwardClick(sender: AnyObject) {
        if player != nil {
            player.seekToTime(CMTimeMakeWithSeconds(CMTimeGetSeconds(player.currentTime()) + 5, player.currentTime().timescale))
            self.playTime.text = stringFromTimeInterval(NSTimeInterval(progressBar.value))
        }
    }
    @IBAction func backwardClick(sender: AnyObject) {
        if player != nil {
            player.seekToTime(CMTimeMakeWithSeconds(CMTimeGetSeconds(player.currentTime()) - 5, player.currentTime().timescale))
            self.playTime.text = stringFromTimeInterval(NSTimeInterval(progressBar.value))
        }
    }
    
    @IBAction func beginChangeTime(sender: AnyObject) {
        if player != nil {
            Timer.invalidate()
        }
    }
    @IBAction func endChangeTime(sender: AnyObject) {
    }
    @IBAction func touchUpEditg(sender: AnyObject) {
        if player != nil {
            indicator.hidden = false
            centerIcon.hidden = true
            player.pause()
            player.seekToTime(CMTimeMake(Int64(progressBar.value), 1))
            self.playTime.text = stringFromTimeInterval(NSTimeInterval(progressBar.value))
            player.play()
            self.Timer = NSTimer.scheduledTimerWithTimeInterval(0.1, target: self, selector: #selector(AudioViewController.updateSlider), userInfo: nil, repeats: true)
            indicator.hidden = true
            centerIcon.hidden = false
        } else {
            progressBar.value = 0
        }
        
    }
    
    @IBAction func ChangeAudioTime(sender: AnyObject) {
        if player != nil {
            Timer.invalidate()
        }
    }
    @IBAction func repeatClick(sender: AnyObject) {
        if isRepeat {
            repeatBtn.setTitle("", forState: .Normal)
        } else {
            repeatBtn.setTitle("", forState: .Normal)
        }
        isRepeat = !isRepeat
    }
    override func viewWillDisappear(animated: Bool) {
        stop()
        self.navigationController!.interactivePopGestureRecognizer!.enabled = true
    }
    func playerItemDidReachEnd(notification: NSNotification) {
        if isRepeat {
            player.pause()
            player.seekToTime(CMTimeMakeWithSeconds(CMTimeGetSeconds(player.currentTime()-player.currentTime()), player.currentTime().timescale))
            self.playTime.text = stringFromTimeInterval(NSTimeInterval(progressBar.value))
            player.play()
        } else {
            if playIndex + 1 < data.count {
                let indexPath = NSIndexPath(forRow: playIndex+1, inSection: 0);
                self.tableView.selectRowAtIndexPath(indexPath, animated: false, scrollPosition: UITableViewScrollPosition.None)
                self.tableView(self.tableView, didSelectRowAtIndexPath: indexPath)
            } else {
                let indexPath = NSIndexPath(forRow: 0, inSection: 0);
                self.tableView.selectRowAtIndexPath(indexPath, animated: false, scrollPosition: UITableViewScrollPosition.None)
                self.tableView(self.tableView, didSelectRowAtIndexPath: indexPath)

            }
        }
    }

    func stop(){
        if player != nil {
            isPlaying = false
            self.playButton.setTitle("", forState: .Normal)
            player.pause()
        }
    }

}
