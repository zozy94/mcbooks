//
//  BookHorizontalView.swift
//  MCBooks
//
//  Created by Quang Anh on 6/10/16.
//  Copyright © 2016 Quang Anh. All rights reserved.
//

import UIKit
@IBDesignable
class BookDetailScrollView: UIView {
    var view: UIView!
    @IBOutlet weak var scrollView: UIScrollView!
    var url = ""
    var listBook = Array<Book>()
    override init(frame: CGRect) {
        super.init(frame: frame)
        xibSetup()
    }
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        xibSetup()
    }
    func xibSetup() {
        view = loadViewFromNib()
        view.frame = bounds
        let bookDetailView = BookDetailView(frame: self.view.frame)
        bookDetailView.setupBook(AppFunc().appDelegate.SelectedBook)
        self.scrollView.addSubview(bookDetailView)
        self.scrollView.contentSize = CGSizeMake(bookDetailView.wrapView.frame.width,bookDetailView.wrapView.frame.height)
        addSubview(view)
    }
    func loadViewFromNib() -> UIView {
        let bundle = NSBundle(forClass: self.dynamicType)
        let nib = UINib(nibName: "BookDetailScrollView", bundle: bundle)
        let view = nib.instantiateWithOwner(self, options: nil)[0] as! UIView
        return view
    }
}