//
//  SliderView.swift
//  MCBooks
//
//  Created by Quang Anh on 6/24/16.
//  Copyright © 2016 Quang Anh. All rights reserved.
//

import UIKit
@IBDesignable
class SliderView: UIView,UIScrollViewDelegate {
    var view: UIView!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var indicatorLabel: UILabel!
    var image = ["slide1.png","slide2.png","Layer 5.jpg"]
    var count = 0
    override init(frame: CGRect) {
        super.init(frame: frame)
        xibSetup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        xibSetup()
    }
    func xibSetup() {
        view = loadViewFromNib()
        view.frame = bounds
        var posX = 0
        for img in image {
            let logo = UIImage(named: img)
            let imageView = UIImageView(image:logo)
            imageView.frame = CGRectMake(CGFloat(posX)*view.frame.width,0,view.frame.width,view.frame.height)
            imageView.contentMode = .ScaleAspectFill
            imageView.clipsToBounds = true
            posX += 1
            scrollView.addSubview(imageView)
        }
        scrollView.delegate = self
        scrollView.contentSize = CGSizeMake(CGFloat(posX)*view.frame.width, view.frame.height)
        scrollView.setNeedsDisplay()
        var string = ""
        for i in 0..<image.count {
            if i == 0 {
                string += ""
            } else {
                string += ""
            }
        }
        indicatorLabel.text = string

        NSTimer.scheduledTimerWithTimeInterval(3, target: self, selector: #selector(SliderView.update), userInfo: nil, repeats: true)
        addSubview(view)
    }
    func scrollViewDidEndScrollingAnimation(scrollView: UIScrollView) {
        let pos = Int(scrollView.contentOffset.x/view.frame.width)
        count = pos
        var string = ""
        for i in 0..<image.count {
            if i == pos {
                string += ""
            } else {
                string += ""
            }
        }
        indicatorLabel.text = string
    }
    func scrollViewDidEndDecelerating(scrollView: UIScrollView) {
        let pos = Int(scrollView.contentOffset.x/view.frame.width)
        count = pos
        var string = ""
        for i in 0..<image.count {
            if i == pos {
                string += ""
            } else {
                string += ""
            }
        }
        indicatorLabel.text = string

    }
    func update(){
        scrollView.setContentOffset(CGPointMake(CGFloat(count)*view.frame.width, 0), animated: true)
        count += 1
        if count >= image.count {
            count = 0
        }
    }
    func loadViewFromNib() -> UIView {
        let bundle = NSBundle(forClass: self.dynamicType)
        let nib = UINib(nibName: "SliderView", bundle: bundle)
        let view = nib.instantiateWithOwner(self, options: nil)[0] as! UIView
        return view
    }
    
}
