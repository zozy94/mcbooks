//
//  NavMenuViewController.swift
//  JupViec
//
//  Created by Quang Anh on 4/26/16.
//  Copyright © 2016 Quang Anh. All rights reserved.
//

import UIKit

class NavMenuViewController:UIViewController, UITableViewDataSource, UITableViewDelegate{
    @IBOutlet weak var tableView: UITableView!
    var menuItems = Array<CellInfo>()
    var VC: RootViewController!
    var shieldView: UIView!
    override func viewWillAppear(animated: Bool) {
        //Layout setup
        self.view.backgroundColor = Color.theme
        setupMenu()
        
        shieldView = UIView(frame: self.revealViewController().frontViewController.view.frame)
        self.revealViewController().frontViewController.view.addSubview(shieldView)
        shieldView.addGestureRecognizer(self.revealViewController().tapGestureRecognizer())
        shieldView.addGestureRecognizer(self.revealViewController().panGestureRecognizer())

        //SW setup
        self.revealViewController().frontViewController.view.gestureRecognizers?.removeAll()
        self.revealViewController().frontViewController.view.addGestureRecognizer(self.revealViewController().tapGestureRecognizer())
        self.revealViewController().view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        // Tableview setup
        self.tableView.separatorStyle = UITableViewCellSeparatorStyle.None
        
    }
    override func viewDidLoad() {
        let front = self.revealViewController().frontViewController as! UITabBarController
        let nav = front.childViewControllers[0] as! UINavigationController
        VC = nav.childViewControllers[0] as! RootViewController
    }
    override func viewWillDisappear(animated: Bool) {
        shieldView.removeFromSuperview()
    }
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return menuItems.count
    }
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell{
        let data = menuItems[indexPath.row]
        var cell: SideMenuViewCell!
        
        cell = tableView.dequeueReusableCellWithIdentifier("menuCell") as! SideMenuViewCell
        cell.title.text = data.label
        cell.logo.text = String(format: "%C", Icon.value[data.icon]!)
        cell.segue = data.segue
        
        cell.preservesSuperviewLayoutMargins = false
        cell.separatorInset = UIEdgeInsetsZero
        cell.layoutMargins = UIEdgeInsetsZero
        return cell
    }
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
//        let cell = tableView.cellForRowAtIndexPath(indexPath) as! SideMenuViewCell
        self.revealViewController().revealToggleAnimated(true)
        AppFunc().appDelegate.catID = menuItems[indexPath.row].catid
        AppFunc().appDelegate.showID = showType.CAT
        if menuItems[indexPath.row].segue == 1 {
            self.VC.goTo()
        } else {
            self.VC.showMCBooks()
        }
        tableView.deselectRowAtIndexPath(indexPath, animated: true)

    }
    func setupMenu(){
        menuItems.removeAll()
        let categories = AppFunc().appDelegate.Categories
        for item in categories {
            menuItems.append(CellInfo(label: item.name, segue: 1, icon: "book", catid: item.id))
        }
//        menuItems.append(CellInfo(label: "Tin tức", segue: 1, icon: "news"))
        menuItems.append(CellInfo(label: "Về MCBooks", segue: 0, icon: "info"))
        tableView.reloadData()
    }
    
}
class CellInfo {
    var label: String = ""
    var icon: String = ""
    var catid = ""
    var segue = -1
    init(label: String, segue: Int, icon: String){
        self.label = label
        self.segue = segue
        self.icon = icon
    }
    init(label: String, segue: Int, icon: String, catid: String ){
        self.catid = catid
        self.label = label
        self.segue = segue
        self.icon = icon
    }
}
