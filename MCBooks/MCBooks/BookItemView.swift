//
//  BookItemView.swift
//  MCBooks
//
//  Created by Quang Anh on 6/10/16.
//  Copyright © 2016 Quang Anh. All rights reserved.
//

import UIKit
@IBDesignable
class BookItemView: UIView {
    var view: UIView!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var rateLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var giftLabel: UILabel!
    
    var bookData = Book()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        xibSetup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        xibSetup()
    }
    
    func setup(item: Book){
        bookData = item
        if item.thumb != nil {
            self.imageView.image = item.thumb
        }
        titleLabel.text = item.name
        rateLabel.text = AppFunc().ratingLabel(item.avg_star)
        priceLabel.text = AppFunc().decimalNum(item.price) + " vnđ"
        if(item.sale.count > 0){
            giftLabel.hidden = false
        } else {
            giftLabel.hidden = true
        }
        if(item.sale.count>0){
            if(item.sale[0] == ""){
                giftLabel.hidden = true
            } else {
                giftLabel.hidden = false
            }
        } else {
            giftLabel.hidden = true
        }
        let image = AppFunc().appDelegate.getCachedImage(item.id)
        if(image == nil){
            print("not cached \(item.id)")
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)) {
                if let thumb = AppFunc().getImage(URL.Thumb + item.image + "&width=\(AppFunc().appDelegate.screenWidth/2.4)"){
                    item.thumb = thumb
                    AppFunc().appDelegate.cachedImage(item.id, image: thumb)
                    dispatch_async(dispatch_get_main_queue()) {
                        self.setAvatar(thumb)
                    }
                }
            }
        } else {
            item.thumb = image
            self.setAvatar(item.thumb!)
        }
        
    }
    func setAvatar(image: UIImage){
        let width = image.size.width/1.4
        let height = image.size.height*width/image.size.width
        self.imageView.image = image.imageByCroppingImage(CGSizeMake(width, height))

    }
    func xibSetup() {
        view = loadViewFromNib()
        view.frame = bounds
        imageView.layer.cornerRadius = imageView.bounds.width/4.5
        let gesture = UITapGestureRecognizer(target: self, action: #selector(BookItemView.tap(_:)))
        view.addGestureRecognizer(gesture)
        addSubview(view)
    }
    
    func tap (sender:UITapGestureRecognizer){
        AppFunc().appDelegate.SelectedBook = self.bookData
        NSNotificationCenter.defaultCenter().postNotificationName("ShowBook", object: nil, userInfo: nil)
    }
    func loadViewFromNib() -> UIView {
        let bundle = NSBundle(forClass: self.dynamicType)
        let nib = UINib(nibName: "BookItemView", bundle: bundle)
        let view = nib.instantiateWithOwner(self, options: nil)[0] as! UIView
        return view
    }

}
