//
//  FirstViewController.swift
//  MCBooks
//
//  Created by Quang Anh on 6/1/16.
//  Copyright © 2016 Quang Anh. All rights reserved.
//

import UIKit

class RootViewController: UIViewController {
    var currentViewController: UIViewController?
    @IBOutlet weak var ViewHolder: UIView!
    @IBOutlet weak var navRightButton: UIButton!
    var homeView:HomeView!
    var favouriteView:FavouriteView!
    var popupSearch: PopupSearch!
    var scanQR: ScanQRView!
    var GiftView: WebView!
    var profileView: ProfileView!
    var viewpage = ""
    var tabID = 0
    var isShowBook = false
    var findButtonBtn:UIBarButtonItem!
    var switchViewBtn:UIBarButtonItem!
    
    let appDelegate = AppFunc().appDelegate
    override func viewWillAppear(animated: Bool) {
        switch(tabID){
        case 0:
            showSearchButton()
            showHomeView()
            break
        case 1:
            showSearchButton()
            showGift()
            break
        case 2:
            showScanQR()
            break
        case 3:
            findButtonBtn = UIBarButtonItem(title: "", style: UIBarButtonItemStyle.Plain, target: self, action: #selector(RootViewController.showSearch))
            findButtonBtn.setTitleTextAttributes([
                NSFontAttributeName: UIFont(name: "FontAwesome", size: 26.0)!,
                NSForegroundColorAttributeName: UIColor.whiteColor()], forState: .Normal)
            
            switchViewBtn = UIBarButtonItem(title: "", style: UIBarButtonItemStyle.Plain, target: self, action: #selector(RootViewController.switchView))
            switchViewBtn.setTitleTextAttributes([
                NSFontAttributeName: UIFont(name: "FontAwesome", size: 26.0)!,
                NSForegroundColorAttributeName: UIColor.whiteColor()], forState: .Normal)
            
            if(appDelegate.isGridView){
                        switchViewBtn.title = ""
            } else {
                switchViewBtn.title = ""
            }
            
            if appDelegate.showTab == Tab.BOOK {
                ShowSwitchView()
            } else {
                HideSwitchView()
            }
            NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(RootViewController.HideSwitchView), name: "HideSwitchView", object: nil)
            NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(RootViewController.ShowSwitchView), name: "ShowSwitchView", object: nil)
            showFavouriteView()
            break
        case 4:
            showLogoutButton()
            showProfileView()
            break
        default:
            showSearchButton()
            break
        }
        if self.revealViewController() != nil {
            self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
            self.view.addGestureRecognizer(self.revealViewController().tapGestureRecognizer())
            self.revealViewController().rearViewRevealWidth = self.view.bounds.width - 75
        }
        
    }
    func HideSwitchView(){
        self.navigationItem.setRightBarButtonItems([findButtonBtn], animated: true)
    }
    func ShowSwitchView(){
        self.navigationItem.setRightBarButtonItems([findButtonBtn,switchViewBtn], animated: true)
    }
    func switchView(){
        NSNotificationCenter.defaultCenter().postNotificationName("switchView", object: nil, userInfo: nil)
        if(appDelegate.isGridView){
            setListView()
        } else {
            setGridView()
        }
    }
    
    func setGridView(){
        findButtonBtn.title = ""
        switchViewBtn.title = ""
        appDelegate.isGridView = true
    }
    func setListView(){
        findButtonBtn.title = ""
        switchViewBtn.title = ""
        appDelegate.isGridView = false
    }

    func showSearchButton(){
        navRightButton.addTarget(self, action: #selector(RootViewController.showSearch), forControlEvents: .TouchUpInside)
    }
    func showLogoutButton(){
        let findButtonBtn = UIBarButtonItem(title: "", style: UIBarButtonItemStyle.Plain, target: self, action: #selector(RootViewController.LogOut))
        findButtonBtn.setTitleTextAttributes([
            NSFontAttributeName: UIFont(name: "FontAwesome", size: 26.0)!,
            NSForegroundColorAttributeName: UIColor.whiteColor()], forState: .Normal)
        self.navigationItem.setRightBarButtonItems([findButtonBtn], animated: true)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        let btn = UIBarButtonItem(title: " ", style: UIBarButtonItemStyle.Plain, target: nil, action: nil)
        self.navigationItem.backBarButtonItem = btn
        UIApplication.sharedApplication().statusBarStyle = .LightContent
        
    }
    
    override func viewDidAppear(animated: Bool) {
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(RootViewController.ShowBook), name: "ShowBook", object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(RootViewController.ShowBook), name: "ShowBookSearch", object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(RootViewController.ShowAll), name: "ShowAll", object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(RootViewController.ShowMedia), name: "ShowMedia", object: nil)
        print("Did appear frame \(self.view.frame)")
    }
    func showProfileView(){
        if profileView == nil {
            profileView = ProfileView(nibName: "ProfileView", bundle: nil)
            profileView.showInView(self.view, animated: true)
        }
    }
    func showHomeView(){
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(RootViewController.reloadHome), name: "reloadedData", object: nil)
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)) {
            let frame = CGRectMake(0, 0, self.view.frame.width, self.view.frame.height-54)
            if(AppFunc().appDelegate.viewFrame == nil){
                AppFunc().appDelegate.viewFrame = frame
            }
            if self.homeView == nil {
                self.homeView = HomeView(frame: frame)
                dispatch_async(dispatch_get_main_queue()) {
                    self.view.addSubview(self.homeView)
                    self.homeView.loadItem()
                }
            } else {
                dispatch_async(dispatch_get_main_queue()) {
                    self.homeView.reloadItem()
                }
            }
        }
    }
    func reloadHome(){
        if UIApplication.currentViewController() != nil && self.homeView != nil {
            self.homeView.reloadItem()
            print("Reload Home")
        }
    }
    func showFavouriteView(){
        if favouriteView == nil {
            favouriteView = FavouriteView(frame: appDelegate.viewFrame)
            self.view.addSubview(favouriteView)
            favouriteView.refresh()

        } else {
            print("refresh")
            favouriteView.frame = appDelegate.viewFrame
            favouriteView.refresh()
        }
    }
    func ShowBook(){
        performSegueWithIdentifier("bookDetail", sender: self)
    }
    func ShowMedia(){
        if appDelegate.SelectedMedia.type == 0 {
            performSegueWithIdentifier("showMedia", sender: self)

        } else {
            performSegueWithIdentifier("showVideo", sender: self)

        }
    }
    func ShowAll(){
        viewpage = "ShowAll"
        performSegueWithIdentifier("detail", sender: self)
    }
    func showGift(){
        if GiftView == nil {
            GiftView = WebView(frame: appDelegate.viewFrame)
            GiftView.setPDF(NSURL(string: URL.Gift)!)
            self.view.addSubview(GiftView)
        }
    }
    func showMCBooks(){
        performSegueWithIdentifier("showWeb", sender: self)
    }
    func LogOut(){
        let alert = UIAlertController(title: "Bạn có chắc muốn đăng xuất không?", message: "", preferredStyle: .ActionSheet)
        alert.addAction(UIAlertAction(title: "Có", style: UIAlertActionStyle.Default, handler: {(action: UIAlertAction) in
            AppFunc().appDelegate.clearData()
            let storyboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewControllerWithIdentifier("loginView")
            self.presentViewController(vc,animated: true,completion: nil)
        }))
        alert.addAction(UIAlertAction(title: "Không", style: .Cancel, handler: nil))
        presentViewController(alert, animated: true, completion: nil)
    }
    
    func showScanQR(){
        if scanQR == nil {
            scanQR = ScanQRView(nibName: "ScanQRView", bundle: nil)
            scanQR.showInView(self.view, animated: true)
        } else {
            scanQR.restart()
        }
    }
    func showSearch(){
        if !appDelegate.isShowSearch {
            popupSearch = PopupSearch(nibName: "PopupSearch", bundle: nil)
            popupSearch.showInView(self.view, animated: true)
        } else {
            popupSearch.close()
        }
    }
    
    func goTo(){
        let controller = self.tabBarController as! MCbooksTabbarController
        controller.selectTab(0)
        viewpage = "ShowAll"
        performSegueWithIdentifier("detail", sender: self)
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if(segue.identifier == "detail"){
            let VC = segue.destinationViewController as! DetailViewController
            VC.viewpage = viewpage
        } else if(segue.identifier == "bookDetail"){
            let VC = segue.destinationViewController as! BookDetailViewController
            VC.bookDetail = appDelegate.SelectedBook
        } else if(segue.identifier == "showWeb"){
            let VC = segue.destinationViewController as! WebViewController
//            VC.URL = ""

        }
    }
    @IBAction func revealButton(sender: AnyObject) {
        self.revealViewController().revealToggle(nil)
    }
    override func viewDidDisappear(animated: Bool) {
        if appDelegate.isShowSearch && popupSearch != nil {
            popupSearch.close()
            appDelegate.isShowSearch = false
        }
        if tabID == 0 {
            appDelegate.lastScrollOffset = homeView.scrollView.contentOffset
        }
        NSNotificationCenter.defaultCenter().removeObserver(self, name: "reloadedData", object: nil)
        NSNotificationCenter.defaultCenter().removeObserver(self, name: "ShowMedia", object: nil)
        NSNotificationCenter.defaultCenter().removeObserver(self, name: "ShowBook", object: nil)
        NSNotificationCenter.defaultCenter().removeObserver(self, name: "ShowBookSearch", object: nil)
        NSNotificationCenter.defaultCenter().removeObserver(self, name: "ShowAll", object: nil)
//        if(homeView != nil){
//            homeView.removeFromSuperview()
//        }
//        self.view = nil

    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

