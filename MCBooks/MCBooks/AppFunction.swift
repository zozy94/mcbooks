//
//  FunctionProcessing.swift
//  LaundryApp
//
//  Created by ZoZy on 8/18/15.
//  Copyright (c) 2015 ZoZy. All rights reserved.
//

import UIKit

class AppFunc{
    let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
    func getDateFromString(date: String) -> NSDate? {
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        dateFormatter.locale = NSLocale(localeIdentifier: "vi_VN")
        let d: NSDate! = dateFormatter.dateFromString(date)
        return d
    }
    func getDateTime(date: NSDate) -> String {
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        dateFormatter.locale = NSLocale(localeIdentifier: "vi_VN")
        return dateFormatter.stringFromDate(date)
    }
    func combineDateTime(date: NSDate, time: String) -> NSDate{
        let dateFormatter1 = NSDateFormatter()
        dateFormatter1.dateFormat = "yyyy-MM-dd"
        let dateStr = dateFormatter1.stringFromDate(date)
        let dateTime = dateStr + " " + time
        print(dateTime)
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm"
        return dateFormatter.dateFromString(dateTime)!

    }
    func screenWidth() -> CGFloat{
        return appDelegate.window!.bounds.width
    }
    func setNSObject(object: AnyObject, key: String){
        let dataSave = NSKeyedArchiver.archivedDataWithRootObject(object)
        NSUserDefaults.standardUserDefaults().setObject(dataSave, forKey: key)
        NSUserDefaults.standardUserDefaults().synchronize()
    }
    func getNSObject(key: String) -> AnyObject?{
        if let data = NSUserDefaults.standardUserDefaults().objectForKey(key){
            return NSKeyedUnarchiver.unarchiveObjectWithData(data as! NSData)
        }
        print("\(key) nil")
        return nil
    }
    func deleteNSObject(key: String){
        NSUserDefaults.standardUserDefaults().removeObjectForKey(key)
    }
    func getjsonString(content: AnyObject) -> String{
        var string = ""
        var error : NSError?
        do {
            let jsonData = try NSJSONSerialization.dataWithJSONObject(content, options: [])
            let jsonString = NSString(data: jsonData, encoding: NSUTF8StringEncoding)! as String
            string = jsonString
        } catch let error1 as NSError {
            error = error1
            print("Error in JSON conversion: \(error!.localizedDescription)")
        }
        return string
    }
    
    func decimalNum(number: AnyObject) -> String{
        return "\(numberFormat().stringFromNumber(number as! NSNumber)!)"
    }
    func numberFormat() -> NSNumberFormatter{
        let formatter = NSNumberFormatter()
        formatter.numberStyle = NSNumberFormatterStyle.DecimalStyle
        formatter.locale = NSLocale(localeIdentifier: "vi_VN")
        formatter.minimumFractionDigits = 0
        formatter.maximumFractionDigits = 2
        return formatter
    }
    func isValidPhone(number: String) -> Bool{
        if((number.characters.count == 10 || number.characters.count == 11) && number.characters.first == "0") {
            return true
        }
        return false
    }
    func isValidEmail(testStr:String) -> Bool {
        // println("validate calendar: \(testStr)")
        let emailFormat = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let emailPredicate = NSPredicate(format:"SELF MATCHES %@", emailFormat)
        return emailPredicate.evaluateWithObject(testStr)
    }
    func stringFromTimeInterval(interval: NSTimeInterval) -> String {
        if !interval.isNaN {
            let interval = Int(interval)
            let seconds = interval % 60
            let minutes = (interval / 60) % 60
            return String(format: "%02d:%02d", minutes, seconds)
        }
        return ""
    }

    func getDataFromUrl(url:NSURL, completion: ((data: NSData?, response: NSURLResponse?, error: NSError? ) -> Void)) {
        NSURLSession.sharedSession().dataTaskWithURL(url) { (data, response, error) in
            completion(data: data, response: response, error: error)
            }.resume()
    }
    func reloadAvatar(){
        appDelegate.userData.avatar = getImage(appDelegate.userData.avatarURL)
    }
    func getImage(url: String) -> UIImage?{
        let url = NSURL(string: url)
        if let data = NSData(contentsOfURL: url!){
            return UIImage(data: data)
        }
        return nil
    }
    func fetchBookData(item: AnyObject) -> Book{
        let book = Book()
        book.id = item.valueForKey("id") as! String
        if let image = item.valueForKey("image") as? String
        {
            book.image = image
        }
        book.name = item.valueForKey("name") as! String
        book.price = item.valueForKey("price") as! Int
        let info = item.valueForKey("information")! as AnyObject
        book.publisher = info.valueForKey("publisher") as! String
        book.author = info.valueForKey("author") as! String
        book.description = info.valueForKey("description") as! String

        book.preview = item.valueForKey("preview") as! String
        book.categories = Array<Category>()
        let categories = item.valueForKey("categories")! as! Array<AnyObject>
        for cat in categories {
            let catItem = Category()
            catItem.id = cat.valueForKey("id") as! String
            catItem.name = cat.valueForKey("name") as! String
            book.categories.append(catItem)
        }
        book.medias = Array<Media>()
        let medias = item.valueForKey("medias")! as! Array<AnyObject>
        for media in medias {
            let Item = Media()
            Item.id = media.valueForKey("id") as! String
            Item.name = media.valueForKey("name") as! String
            if let url = media.valueForKey("url") as? String {
                Item.url = url
            }
            Item.type = media.valueForKey("type") as! Int
            if Item.type == 0 {
                Item.url = URL.Image + Item.url
            }
            book.medias.append(Item)
        }
        book.sale = item.valueForKey("sale_offs")! as! Array<String>
        book.isHot = item.valueForKey("hot") as! Bool
        book.isNew = item.valueForKey("new") as! Bool
        book.isComing = item.valueForKey("coming") as! Bool
        let rate = item.valueForKey("ratings")! as AnyObject
        book.avg_star = rate.valueForKey("avg_star") as! Double
        book.myrate = Rate()
        if let myrate = rate.valueForKey("my_rating") {
            if let comment = myrate.valueForKey("comment") as? String {
                book.myrate.comment = comment
                let date = myrate.valueForKey("create_at") as! Int
                book.myrate.date = NSDate(timeIntervalSince1970: NSTimeInterval(date/1000)).formattedISO8601

                book.myrate.stars = myrate.valueForKey("stars") as! Double
            }
        }
        if let buyURL = item.valueForKey("buy_url") as? String {
            book.buyURL = buyURL
        }
        return book
    }
    func fetchMediaData(item: AnyObject) -> Media {
        let media = Media()
        media.id = item.valueForKey("id") as! String
        media.name = item.valueForKey("name") as! String
        media.url = item.valueForKey("url") as! String
        media.type = item.valueForKey("type") as! Int
        media.url = URL.Image + media.url
        if let book = item.valueForKey("book") {
            media.bookName = book.valueForKey("name") as! String
            media.bookID = book.valueForKey("id") as! String
        }
        appDelegate.mediaCache[media.id] = item
        return media
    }
    func fetchGiftData(item: AnyObject) -> Gift {
        let gift = Gift()
        gift.id = item.valueForKey("id") as! String
        gift.name = item.valueForKey("name") as! String
        gift.image = item.valueForKey("image") as! String
        gift.image = URL.Image + gift.image
        gift.type = item.valueForKey("type") as! Int
        gift.file = item.valueForKey("file") as! String
        return gift
    }
    func createTempDirectoryToStoreFile(){
        let paths = NSSearchPathForDirectoriesInDomains(NSSearchPathDirectory.DocumentDirectory, NSSearchPathDomainMask.UserDomainMask, true)
        let documentsDirectory: AnyObject = paths[0]
        let tempPath = documentsDirectory.stringByAppendingPathComponent("MCBooks")
        if (!NSFileManager.defaultManager().fileExistsAtPath(tempPath)) {
            do {
             try NSFileManager.defaultManager().createDirectoryAtPath(tempPath, withIntermediateDirectories: false, attributes: nil)
            } catch _ {
            }
        }
    }
    func clearAllFilesFromTempDirectory(){
        let paths = NSSearchPathForDirectoriesInDomains(NSSearchPathDirectory.DocumentDirectory, NSSearchPathDomainMask.UserDomainMask, true)
        let documentsDirectory: AnyObject = paths[0]
        let tempPath = documentsDirectory.stringByAppendingPathComponent("MCBooks")
        if (!NSFileManager.defaultManager().fileExistsAtPath(tempPath)) {
            do {
                try NSFileManager.defaultManager().removeItemAtPath(tempPath)
            } catch _ {
            }
        }
    }

    func getDate(date: NSDate) -> String {
            let dateFormatter = NSDateFormatter()
            dateFormatter.locale = NSLocale(localeIdentifier: "vi_VN")
            dateFormatter.dateFormat = "dd-MM-yyyy"
            return dateFormatter.stringFromDate(date)
    }
    func getTime(time: NSDate) -> String {
            let dateFormatter = NSDateFormatter()
            dateFormatter.locale = NSLocale(localeIdentifier: "vi_VN")
            dateFormatter.dateFormat = "HH:mm"
            return dateFormatter.stringFromDate(time)
    }
    func ratingLabel(rate: Double) -> String{
        let denominator = 2.0
        let rate = round(rate*denominator)/denominator
        switch(rate){
            case 0.5:
                return ""
            
            case 1.0:
                return ""
            
            case 1.5:
                return ""
            
            case 2.0:
                return ""
            
            case 2.5:
                return ""
            
            case 3.0:
                return ""
            
            case 3.5:
                return ""
            
            case 4.0:
                return ""
            
            case 4.5:
                return ""
            
            case 5.0:
                return ""
            
        default:
            return ""
            
        }
    }
    func reloadBookData(){
        let CN = ConnectServer()
        var newData = Array<Book>()
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)) {
            if let json: AnyObject = CN.GETPageBook(URL.hotBook+"?page=1") {
                if (json.valueForKey("code") as! Int == 1){
                    if let result = json.valueForKey("result") as? Array<AnyObject> {
                        if result.count > 0 {
                            for item in result {
                                let book = self.fetchBookData(item)
                                self.appDelegate.addToData(book.id,data: item)
                                newData.append(book)
                            }
                        }
                    }
                }
            }
            if let json: AnyObject = CN.GETPageBook(URL.newBook+"?page=1") {
                if (json.valueForKey("code") as! Int == 1){
                    if let result = json.valueForKey("result") as? Array<AnyObject> {
                        if result.count > 0 {
                            for item in result {
                                let book = self.fetchBookData(item)
                                self.appDelegate.addToData(book.id,data: item)
                                newData.append(book)
                            }
                        }
                    }
                }
            }
            if let json: AnyObject = CN.GETPageBook(URL.comingBook+"?page=1") {
                if (json.valueForKey("code") as! Int == 1){
                    if let result = json.valueForKey("result") as? Array<AnyObject> {
                        if result.count > 0 {
                            for item in result {
                                let book = self.fetchBookData(item)
                                self.appDelegate.addToData(book.id,data: item)
                                newData.append(book)
                            }
                        }
                    }
                }
            }
            if newData.count > 0 {
                for item in newData {
                    self.appDelegate.updateBook(item)
                }
                dispatch_async(dispatch_get_main_queue()) {
                    NSNotificationCenter.defaultCenter().postNotificationName("reloadedData", object: nil, userInfo: nil)
                }
            }
//            do {
//                let jsonData = try NSJSONSerialization.dataWithJSONObject(params, options: NSJSONWritingOptions.PrettyPrinted)
//                let jsonResult: AnyObject? = try? NSJSONSerialization.JSONObjectWithData(jsonData, options: NSJSONReadingOptions(rawValue: 0))
//                AppFunc().setNSObject(jsonResult!, key: "bookData")
////                self.appDelegate.imageCache = nil
//            } catch let error as NSError {
//                print(error)
//            }
        }
    }
}