//
//  BookHorizontalView.swift
//  MCBooks
//
//  Created by Quang Anh on 6/10/16.
//  Copyright © 2016 Quang Anh. All rights reserved.
//

import UIKit
@IBDesignable
class ShowAllView: UIView,UIScrollViewDelegate {
    var view: UIView!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var indicator: UIActivityIndicatorView!
    @IBOutlet weak var label: UILabel!
    var refreshControl: UIRefreshControl!
    var isGetting = false
    var isGrid = true
    var bookData = Array<Book>()
    var mediaData = Array<Media>()
    var giftData = Array<Gift>()
    var page = 1
    var url = ""
    var type = -1
    var gridWidth: CGFloat!
    var gridHeight: CGFloat!
    var listHeight: CGFloat!
    var listWidth: CGFloat!
    var isVideo = false
    var appDelegate = AppFunc().appDelegate
    override init(frame: CGRect) {
        super.init(frame: frame)
        xibSetup()
    }
    func removeSubview() {
        for subview in scrollView.subviews {
            subview.removeFromSuperview()
        }
    }
    func setURL(type: Int){
        mediaData.removeAll()
        removeSubview()
        self.indicator.hidden = false
        self.indicator.startAnimating()
        self.type = type
        switch(type){
        case showType.HOT:
            self.url = URL.hotBook
            self.bookData = appDelegate.hotBooks()
            switchView()
            getMoreBook()
            break
        case showType.NEW:
            self.bookData = appDelegate.newBooks()
            self.url = URL.newBook
            switchView()
            getMoreBook()
            break
        case showType.COMING:
            self.bookData = appDelegate.comingBooks()
            self.url = URL.comingBook
            switchView()
            getMoreBook()
            break
        case showType.FAV:
            self.bookData = appDelegate.favouriteBooks()
            self.url = URL.favBook
            switchView()
            getMoreBook()
            break
        case showType.CAT:
            self.url = URL.category+"/\(appDelegate.catID)/books"
            getMoreBook()
            break
        case showType.MEDIA:
            let Data = appDelegate.favouriteMedia(0)
            for item in Data {
                print("add \(item.name)")
                    addMediaView(item)
            }
            getMoreAudio()
            break
        case showType.VIDEO:
            self.type = showType.MEDIA
            isVideo = true
            let Data = appDelegate.favouriteMedia(1)
            for item in Data {
                    addMediaView(item)
            }
            getMoreAudio()
            break
        case showType.GIFT:
            self.giftData = appDelegate.Gifts
            break
        default:
            break
        }
    }
    func loadGridView(){
        for(subview) in self.scrollView.subviews {
            UIView.transitionWithView(self, duration: 0.25, options: .TransitionCrossDissolve, animations: {
                subview.removeFromSuperview()
                }, completion: nil)
        }
        for i in 0..<bookData.count {
            let posX = CGFloat(i%3)*gridWidth
            let posY = CGFloat(Int(i/3))*gridHeight
            let bookItem = BookItemView(frame: CGRectMake(posX, posY, gridWidth, gridHeight))
            bookItem.setup(bookData[i])
            UIView.transitionWithView(self, duration: 0.25, options: .TransitionCrossDissolve, animations: {
                self.scrollView.addSubview(bookItem)
                }, completion: nil)
        }
        let row = ceil(Float(bookData.count)/3.0)
        self.scrollView.contentSize = CGSize(width: listWidth, height: CGFloat(row)*gridHeight)
        print(self.scrollView.contentSize)
        self.scrollView.setNeedsDisplay()
        self.view.setNeedsLayout()
    }
    func loadListView(){
        for(subview) in self.scrollView.subviews {
            UIView.transitionWithView(self, duration: 0.25, options: .TransitionCrossDissolve, animations: {
                subview.removeFromSuperview()
                }, completion: nil)
        }
        print(bookData.count)
        
        for i in 0..<bookData.count {
            let posY = CGFloat(i)*listHeight
            let bookItem = BookItemListView(frame: CGRectMake(0, posY, listWidth, listHeight))
            bookItem.setup(bookData[i])
            UIView.transitionWithView(self, duration: 0.25, options: .TransitionCrossDissolve, animations: {
                self.scrollView.addSubview(bookItem)
                }, completion: nil)
            print("\(bookData[i].name) : \(posY)")
        }
        print("Height \(CGFloat(bookData.count)*listHeight)")
        scrollView.contentSize = CGSize(width: listWidth, height: CGFloat(bookData.count)*listHeight)
        print(self.scrollView.contentSize)

        self.scrollView.setNeedsDisplay()
        self.view.setNeedsLayout()
    }

    func getMoreBook(){
        if label != nil {
            label.hidden = true
        }
        print("getting")
        isGetting = true
        let CN = ConnectServer()
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)) {
            var status = 0
            if let json: AnyObject = CN.GETPageBook(self.url+"?page=\(self.page)"){
//                print(json)
                status =  json.valueForKey("code") as! Int
                if (status == 1){
                    if let result = json.valueForKey("result") as? Array<AnyObject> {
                        print("getting result")
                        if result.count > 0 {
//                            AppFunc().appDelegate.saveFavouriteBook(json)
                            print("got result \(result.count)")
                            self.page += 1
                            dispatch_async(dispatch_get_main_queue()) {
                                if self.indicator != nil {
                                    self.indicator.stopAnimating()
                                }
                            }
                            for item in result {
                                let book = AppFunc().fetchBookData(item)
                                if(self.type == showType.FAV){
                                    book.isFavourite = true
                                    AppFunc().appDelegate.updateBook(book)
                                    AppFunc().appDelegate.addFavourite(book.id)
                                }
                                dispatch_async(dispatch_get_main_queue()) {
                                    if(self.isGrid){
                                        self.addGridView(book)
                                    } else {
                                        self.addListView(book)
                                    }
                                }
                            }
                            self.isGetting = false
                            
                        } else if self.bookData.count == 0 {
                            dispatch_async(dispatch_get_main_queue()) {
                                if self.label != nil {
                                self.label.hidden = false
                                self.label.text = "Không có cuốn sách nào"
                                }
                            }
                            print(self.label.text)
                        }
                    }
                }
            }
            dispatch_async(dispatch_get_main_queue()) {
                if self.indicator != nil {
                    self.indicator.stopAnimating()
                }
            }
        }
        print(label.text)
    }

    func getMoreAudio(){
        var mediaType = 0
        if !isVideo {
            mediaType = 0
        } else {
            mediaType = 1
        }
        print("Media type\(mediaType)")

        label.hidden = true
        isGetting = true
        let CN = ConnectServer()
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)) {
            var status = 0
            if let json: AnyObject = CN.GETPageBook(URL.FavMedia + "?page=\(self.page)"){
                status =  json.valueForKey("code") as! Int
                if (status == 1){
                    if let result = json.valueForKey("result") as? Array<AnyObject> {
                        if result.count > 0 {
                            self.page += 1
                            dispatch_async(dispatch_get_main_queue()) {
                                if self.indicator != nil {
                                    self.indicator.stopAnimating()
                                }
                            }
//                            self.appDelegate.saveFavouriteMedia(json)
                            for item in result {
                                let media = AppFunc().fetchMediaData(item)
                                print(media.type)
                                self.appDelegate.updateFavouriteMedia(media)
                                dispatch_async(dispatch_get_main_queue()) {
                                        if media.type == mediaType {
                                            self.addMediaView(media)
                                        }
                                }
                            }
                            self.isGetting = false
                        }  else if self.mediaData.count == 0 {
                            dispatch_async(dispatch_get_main_queue()) {
                                if !self.isVideo{
                                    self.label.hidden = false
                                    self.label.text = "Không có audio nào"
                                } else {
                                    self.label.hidden = false
                                    self.label.text = "Không có video nào"
                                }
                                if self.indicator != nil {
                                    self.indicator.stopAnimating()
                                }
                            }
                        }
                    }
                }
            }
            dispatch_async(dispatch_get_main_queue()) {
                if self.indicator != nil {
                    self.indicator.stopAnimating()
                }
            }
        }
//        AppFunc().appDelegate.FavouriteMedias = self.mediaData
    }
    func addMediaView(item: Media){
        item.isFav = true
        var check = true
        for mediaItem in mediaData {
            if(mediaItem.id == item.id){
                check = false
            }
        }
        if(check){
            print("add \(item.name)")
            let posX:CGFloat = 0
            let posY = CGFloat(mediaData.count)*listHeight
            let mediaItem = AudioItemView(frame: CGRectMake(posX, posY, listWidth, listHeight))
            mediaItem.setup(item)
            UIView.transitionWithView(self, duration: 0.25, options: .TransitionCrossDissolve, animations: {
                self.scrollView.addSubview(mediaItem)
                }, completion: nil)
            mediaData.append(item)
            scrollView.contentSize = CGSize(width: listWidth, height: CGFloat(Int(mediaData.count))*listHeight)
            self.scrollView.setNeedsDisplay()
            self.view.setNeedsLayout()
        }
    }
    func addListView(item: Book){
        var check = true
        for bookItem in bookData {
            if(bookItem.id == item.id){
                check = false
            }
        }
        if(check){
            let posY = CGFloat(bookData.count)*listHeight
            let bookItem = BookItemListView(frame: CGRectMake(0, posY, listWidth, listHeight))
            bookItem.setup(item)
            print(item.name)
            UIView.transitionWithView(self, duration: 0.25, options: .TransitionCrossDissolve, animations: {
                self.scrollView.addSubview(bookItem)
                }, completion: nil)
            bookData.append(item)
            scrollView.contentSize = CGSize(width: listWidth, height: CGFloat(bookData.count)*listHeight)
            print(scrollView.contentSize)
            self.scrollView.setNeedsDisplay()
            self.view.setNeedsLayout()
        }
    }
    func addGridView(item: Book){
        var check = true
        for bookItem in bookData {
            if(bookItem.id == item.id){
                check = false
            }
        }
        if(check){
            let posX = CGFloat(bookData.count%3)*gridWidth
            let posY = CGFloat(Int(bookData.count/3))*gridHeight
            let bookItem = BookItemView(frame: CGRectMake(posX, posY, gridWidth, gridHeight))
            bookItem.setup(item)
            UIView.transitionWithView(self, duration: 0.25, options: .TransitionCrossDissolve, animations: {
                self.scrollView.addSubview(bookItem)
                }, completion: nil)
            bookData.append(item)
            print("Added \(item.name)")
            let row = ceil(Float(bookData.count)/3.0)
            self.scrollView.contentSize = CGSize(width: listWidth, height: CGFloat(row)*gridHeight)
            self.scrollView.setNeedsDisplay()
            self.view.setNeedsLayout()
        }
    }

    func showGift(){
        for i in 0..<giftData.count{
            let posY = 100.0*CGFloat(i)
            let giftItem = GiftItemView(frame: CGRectMake(0, posY, listWidth, 100.0))
            giftItem.setup(giftData[i])
        }
        self.scrollView.contentSize = CGSize(width: listWidth, height: 100.0*CGFloat(giftData.count))
        self.scrollView.setNeedsDisplay()
        self.view.setNeedsLayout()
    }
    func getGift(){
        isGetting = true
        let CN = ConnectServer()
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)) {
            var status = 0
            if let json: AnyObject = CN.GETPageBook(URL.FavMedia + "?page=\(self.page)"){
                //                print(json)
                status =  json.valueForKey("code") as! Int
                if (status == 1){
                    if let result = json.valueForKey("result") as? Array<AnyObject> {
                        if result.count > 0 {
                            self.page += 1
                            dispatch_async(dispatch_get_main_queue()) {
                                if self.indicator != nil {
                                    self.indicator.stopAnimating()
                                }
                            }
                            for item in result {
                                let media = AppFunc().fetchMediaData(item)
                                print(media.name)
                                dispatch_async(dispatch_get_main_queue()) {
                                    self.addMediaView(media)
                                }
                            }
                            self.isGetting = false
                        }
                    }
                }
            }
            dispatch_async(dispatch_get_main_queue()) {
                if self.indicator != nil {
                    self.indicator.stopAnimating()
                }
            }
        }
    }
    func addGift(item: Gift){
        let posY = 100.0*CGFloat(giftData.count)
        let giftItem = GiftItemView(frame: CGRectMake(0, posY, listWidth, 100.0))
        giftItem.setup(item)
        giftData.append(item)
        self.scrollView.contentSize = CGSize(width: listWidth, height: 100.0*CGFloat(giftData.count))
        self.scrollView.setNeedsDisplay()
        self.view.setNeedsLayout()
    }
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        xibSetup()
    }
    
    func xibSetup() {
        view = loadViewFromNib()
        view.frame = appDelegate.viewFrame
        self.scrollView.frame = appDelegate.viewFrame
        self.scrollView.delegate = self
        self.indicator.center = scrollView.center
        label.frame = appDelegate.viewFrame
        label.center = scrollView.center
        refreshControl = UIRefreshControl()
        refreshControl.attributedTitle = NSAttributedString(string: "Đang tải dữ liệu")
        refreshControl.addTarget(self, action: #selector(ShowAllView.refresh), forControlEvents: UIControlEvents.ValueChanged)
        self.scrollView.addSubview(refreshControl)
        self.scrollView.alwaysBounceVertical = true
        gridWidth = appDelegate.itemWidth
        gridHeight = appDelegate.itemHeight
        listHeight =  80
        listWidth = appDelegate.screenWidth
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(ShowAllView.switchView), name: "switchView", object: nil)
        addSubview(view)
    }
    func refresh(sender:AnyObject){
        switch(type){
        case showType.HOT,showType.NEW,showType.COMING,showType.FAV,showType.CAT, showType.VIDEO:
            getMoreBook()
            break
        default:
            getMoreAudio()
            break
        }
        refreshControl.endRefreshing()
    }
    func switchView(){
        if type != showType.MEDIA {
            self.isGrid = appDelegate.isGridView
            if(isGrid){
                loadGridView()
            } else {
                loadListView()
            }
        }
    }
    func scrollViewDidScroll(scrollView: UIScrollView) {
        if(self.scrollView.scrolledToBottom && !isGetting) {
            print("get more")
            if type == -1 {
                getMoreAudio()
            } else {
                getMoreBook()
            }
        }
    }
    func loadViewFromNib() -> UIView {
        let bundle = NSBundle(forClass: self.dynamicType)
        let nib = UINib(nibName: "ShowAllView", bundle: bundle)
        let view = nib.instantiateWithOwner(self, options: nil)[0] as! UIView
        return view
    }
}
extension UIScrollView {
    
    var scrolledToTop: Bool {
        let topEdge = 0 - contentInset.top
        return contentOffset.y <= topEdge
    }
    
    var scrolledToBottom: Bool {
        let bottomEdge = contentSize.height + contentInset.bottom - bounds.height
        return contentOffset.y >= bottomEdge
    }
    
}
