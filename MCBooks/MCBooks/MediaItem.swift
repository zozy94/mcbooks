//
//  MediaItem.swift
//  MCBooks
//
//  Created by Quang Anh on 6/11/16.
//  Copyright © 2016 Quang Anh. All rights reserved.
//

import UIKit
@IBDesignable
class MediaItem: UIView {
    var view: UIView!

    @IBOutlet weak var iconLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var duration: UILabel!
    @IBOutlet weak var downloadButton: UIButton!
    @IBOutlet weak var favButton: UIButton!
    var audioData:Media!
    override init(frame: CGRect) {
        super.init(frame: frame)
        xibSetup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        xibSetup()
    }
    
    func xibSetup() {
        view = loadViewFromNib()
        view.frame = bounds
        let gesture = UITapGestureRecognizer(target: self, action: #selector(BookItemView.tap(_:)))
        view.addGestureRecognizer(gesture)

        addSubview(view)
    }
    
    func tap (sender:UITapGestureRecognizer){
//        AppFunc().appDelegate.SeletedAudio = self.audioData
//        NSNotificationCenter.defaultCenter().postNotificationName("ShowBook", object: nil, userInfo: nil)
    }

    func setup(media: Media){
        if(media.type == 0){
            iconLabel.text = ""
        } else {
            iconLabel.text = ""
        }
        self.titleLabel.text = media.name
        if(media.isDownload){
            downloadButton.enabled = false
        }
        if(media.isFav){
            favButton.setTitleColor(Color.red, forState: .Normal)
        }
    }
    func loadViewFromNib() -> UIView {
        let bundle = NSBundle(forClass: self.dynamicType)
        let nib = UINib(nibName: "MediaItem", bundle: bundle)
        let view = nib.instantiateWithOwner(self, options: nil)[0] as! UIView
        return view
    }
}