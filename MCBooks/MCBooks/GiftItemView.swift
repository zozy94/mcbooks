//
//  GiftItemView.swift
//  MCBooks
//
//  Created by Quang Anh on 6/22/16.
//  Copyright © 2016 Quang Anh. All rights reserved.
//

import UIKit
@IBDesignable
class GiftItemView: UIView {
    var view: UIView!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var desLabel: UILabel!
    var type = 0
    var bookData = Book()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        xibSetup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        xibSetup()
    }
    func setup(item: Gift){
        self.titleLabel.text = item.name
        self.desLabel.text = item.description
        if(item.thumb == nil){
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)) {
                if let thumb = AppFunc().getImage(URL.Thumb + item.image + "&width=150"){
                    item.thumb = thumb
                    let width = item.thumb!.size.width/1.4
                    let height = item.thumb!.size.height*width/item.thumb!.size.width
                    dispatch_async(dispatch_get_main_queue()) {
                        self.imageView.image = item.thumb!.imageByCroppingImage(CGSizeMake(width, height))
                    }
                }
            }
        } else {
            let width = item.thumb!.size.width/1.4
            let height = item.thumb!.size.height*width/item.thumb!.size.width
            self.imageView.image = item.thumb!.imageByCroppingImage(CGSizeMake(width, height))
        }

    }
    func xibSetup() {
        view = loadViewFromNib()
        view.frame = bounds
        imageView.layer.cornerRadius = imageView.bounds.width/4
        let gesture = UITapGestureRecognizer(target: self, action: #selector(GiftItemView.tap(_:)))
        view.addGestureRecognizer(gesture)
        addSubview(view)
    }
    
    func tap (sender:UITapGestureRecognizer){
        AppFunc().appDelegate.SelectedBook = self.bookData
        NSNotificationCenter.defaultCenter().postNotificationName("ShowBook", object: nil, userInfo: nil)
    }
    func loadViewFromNib() -> UIView {
        let bundle = NSBundle(forClass: self.dynamicType)
        let nib = UINib(nibName: "GiftItemView", bundle: bundle)
        let view = nib.instantiateWithOwner(self, options: nil)[0] as! UIView
        return view
    }
    
}
