//
//  ProfileView.swift
//  MCBooks
//
//  Created by Quang Anh on 6/4/16.
//  Copyright © 2016 Quang Anh. All rights reserved.
//

import UIKit
@IBDesignable
class ProfileView: UIViewController, UITextFieldDelegate, UIImagePickerControllerDelegate,UINavigationControllerDelegate {
    @IBOutlet weak var topView: UIView!
    
    @IBOutlet weak var saveButton: UIButton!
    @IBOutlet weak var avatarImageView: UIImageView!
    @IBOutlet weak var nameTF: UITextField!
    @IBOutlet weak var emailTF: UITextField!
    @IBOutlet weak var phoneTF: UITextField!
    let imagePicker = UIImagePickerController()
    
    @IBOutlet var popupView: UIView!
    var isEditAvatar = false
    var isEdit = false
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
    }
    var connection: NSURLConnection!
    override init(nibName nibNameOrNil: String!, bundle nibBundleOrNil: NSBundle!) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    }
    override func viewWillAppear(animated: Bool) {
        self.view.frame = AppFunc().appDelegate.viewFrame
    }
    override func viewDidAppear(animated: Bool) {
        self.view.frame = AppFunc().appDelegate.viewFrame
//        let width = view.frame.width

    }
    override func viewDidLoad() {
        self.view.frame = AppFunc().appDelegate.viewFrame
        let width = view.frame.width

        topView.backgroundColor = Color.theme
        saveButton.backgroundColor = Color.theme
        let appDelegate = AppFunc().appDelegate
        nameTF.text = appDelegate.userData.name
        emailTF.text = appDelegate.userData.email
        phoneTF.text = appDelegate.userData.phone
        if(appDelegate.userData.avatar != nil){
            avatarImageView.image = appDelegate.userData.avatar
        }
        let gesture = UITapGestureRecognizer(target: self, action: #selector(ProfileView.profileClick(_:)))
        self.avatarImageView.addGestureRecognizer(gesture)
        avatarImageView.userInteractionEnabled = true
        imagePicker.delegate = self
        let numberToolbar: UIToolbar = UIToolbar(frame: CGRectMake(width/2-10, 0, width, 50))
        let doneButton = UIButton(frame: CGRectMake(0, 0, width, 40))
        doneButton.addTarget(self, action: #selector(ProfileView.doneButton), forControlEvents: UIControlEvents.TouchUpInside)
        doneButton.backgroundColor = Color.theme
        doneButton.setTitle("Xong", forState: UIControlState.Normal)
        doneButton.setTitle("Xong", forState: UIControlState.Highlighted)
        doneButton.setTitleColor(UIColor.whiteColor(), forState: UIControlState.Normal)
        doneButton.setTitleColor(UIColor.grayColor(), forState: UIControlState.Highlighted)
        numberToolbar.addSubview(doneButton)
        phoneTF.inputAccessoryView = numberToolbar
        
        nameTF.enabled = false
        phoneTF.enabled = false
        emailTF.enabled = false
        saveButton.setTitle("CHỈNH SỬA THÔNG TIN", forState: .Normal)
        isEdit = false
    }
    func setEdit(){
        if isEdit {
            nameTF.enabled = false
            phoneTF.enabled = false
            emailTF.enabled = false
            saveButton.setTitle("CHỈNH SỬA THÔNG TIN", forState: .Normal)
            isEdit = false
        } else {
            nameTF.enabled = true
            phoneTF.enabled = true
            emailTF.enabled = false
            saveButton.setTitle("LƯU THÔNG TIN", forState: .Normal)
            isEdit = true
        }
    }
    func doneButton(){
        phoneTF.resignFirstResponder()
    }
    @IBAction func updateClick(sender: AnyObject) {
        if isEdit {
            updateProfile()
        } else {
            setEdit()
        }
    }
    func profileClick(sender:UITapGestureRecognizer){
                print("open image picker")
        isEditAvatar = true
        self.imagePicker.allowsEditing = false
        
        let alert = UIAlertController(title: "Chọn ảnh từ", message: "", preferredStyle: .ActionSheet)
        alert.addAction(UIAlertAction(title: "Camera", style: .Default, handler:  {(action: UIAlertAction) in
            alert.dismissViewControllerAnimated(true, completion: nil)
            
            self.imagePicker.sourceType = .Camera
            self.presentViewController(self.imagePicker, animated: true, completion: nil)
        }))
        alert.addAction(UIAlertAction(title: "Thư viện ảnh", style: .Default, handler:  {(action: UIAlertAction) in
            alert.dismissViewControllerAnimated(true, completion: nil)
            self.imagePicker.allowsEditing = false
            self.imagePicker.sourceType = .PhotoLibrary
            self.presentViewController(self.imagePicker, animated: true, completion: nil)
        }))
        alert.addAction(UIAlertAction(title: "Hủy", style: .Cancel, handler: nil))
        presentViewController(alert, animated: true, completion: nil)
    }
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]) {
        if isEditAvatar {
            dismissViewControllerAnimated(true, completion: {
                if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
                    self.view.frame = AppFunc().appDelegate.viewFrame
                    self.avatarImageView.contentMode = .ScaleAspectFill
                    self.avatarImageView.image = pickedImage
                    AppFunc().appDelegate.userData.avatar = pickedImage
                    let imageData = NSData(data:UIImageJPEGRepresentation(pickedImage, 1.0)!)
                    let paths = NSSearchPathForDirectoriesInDomains(NSSearchPathDirectory.DocumentDirectory, NSSearchPathDomainMask.UserDomainMask, true)
                    var docs: String = paths[0]
                    let fullPath = docs.stringByAppendingPathComponent("tempavatar.jpg")
                    let result = imageData.writeToFile(fullPath, atomically: true)
                    AppFunc().appDelegate.cachedImage("userAvatar", image: pickedImage)
                    self.uploadAvatar()
                }
            })
        }
    }
    func uploadAvatar(){
        var  imageData:NSData!
        print("upload")
        let alert = AlertController()
        alert.ShowIndicator()
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)) {
            let nsDocumentDirectory = NSSearchPathDirectory.DocumentDirectory
            let nsUserDomainMask    = NSSearchPathDomainMask.UserDomainMask
            if let paths            = NSSearchPathForDirectoriesInDomains(nsDocumentDirectory, nsUserDomainMask, true) as? [String]
            {
                if paths.count > 0
                {
                    if let dirPath = paths[0] as? String
                    {
                        let readPath = dirPath.stringByAppendingPathComponent("tempavatar.jpg")
                        let image    = UIImage(contentsOfFile: readPath)
                        imageData = UIImageJPEGRepresentation(image!, 0.5)
                    }
                }
            }
            if imageData != nil{
                let boundary = self.generateBoundaryString()
                let mimetype = "image/jpeg"
                let body = NSMutableData()
                let fname = "avatar.jpg"
                body.appendData("------\(boundary)\r\n".dataUsingEncoding(NSUTF8StringEncoding)!)
                body.appendData("Content-Disposition:form-data; name=\"avatar\"; filename=\"\(fname)\"\r\n".dataUsingEncoding(NSUTF8StringEncoding)!)
                body.appendData("Content-Type: \(mimetype)\r\n\r\n".dataUsingEncoding(NSUTF8StringEncoding)!)
                body.appendData(imageData)
                body.appendData("\r\n".dataUsingEncoding(NSUTF8StringEncoding)!)
                body.appendData("------\(boundary)--\r\n".dataUsingEncoding(NSUTF8StringEncoding)!)
                var response: NSURLResponse?
                do {
                    print(URL.Server+"me/avatar")
                    let request = NSMutableURLRequest(URL: NSURL(string:URL.Server+"me/avatar")!)
                    request.HTTPMethod = "PUT"
                    request.setValue("Access_token " + AppFunc().appDelegate.userData.token, forHTTPHeaderField: "Authorization")
                    request.setValue("multipart/form-data; boundary=----\(boundary)", forHTTPHeaderField: "Content-Type")
                    request.HTTPBody = body
                    request.setValue("\(body.length)", forHTTPHeaderField:"Content-Length")
                    let urlData = try NSURLConnection.sendSynchronousRequest(request, returningResponse: &response)
                    print(String(data: urlData, encoding: NSUTF8StringEncoding))
                    if let json = try? NSJSONSerialization.JSONObjectWithData(urlData, options: NSJSONReadingOptions(rawValue: 0)) {
                            print(json)
                            let code = json.valueForKey("code") as! Int
                        if code != 1 {
                            dispatch_async(dispatch_get_main_queue()) {
                                alert.DismissIndicator()
                                alert.showAlert("Lỗi", mess: json.valueForKey("result") as! String)
                            }
                        }
                        }
                } catch let error1 as NSError {
                    print(error1)
                }
                
            }
            dispatch_async(dispatch_get_main_queue()) {
                alert.DismissIndicator()
            }
        }
    }
    func generateBoundaryString() -> String
    {
        return "WebKitFormBoundary\(NSUUID().UUIDString)"
    }
    func updateProfile(){
        let appDelegate = AppFunc().appDelegate
        appDelegate.userData.name =  self.nameTF.text!
        appDelegate.userData.email = self.emailTF.text!
        appDelegate.userData.phone = self.phoneTF.text!
        let CN = ConnectServer()
        var params : [String: AnyObject] = [:]
        params["name"] = self.nameTF.text
        params["phone"] = self.phoneTF.text
        params["email"] = self.emailTF.text
        let alert = AlertController()
        alert.ShowIndicator()
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)) {
            var status = 0
            var mess = ""
            if let json: AnyObject = CN.PUTwAuth(params, url: URL.updateProfile){
                print(json)
                status =  json.valueForKey("code") as! Int
//                if (status == 0){
//                    mess = json.valueForKey("message") as! String
//                } else {
//                    AppFunc().appDelegate.SaveUserData(json)
//                }
            } else {
                mess = "Không thể kết nối tới máy chủ"
            }
            dispatch_async(dispatch_get_main_queue()) {
                if (status == 0){
                    UIAlertView(title: "Có lỗi xảy ra", message: mess, delegate: self, cancelButtonTitle: "OK").show()
                }
                alert.DismissIndicator()
                self.setEdit()
            }
        }
        
    }
    func showInView(aView: UIView!, animated: Bool)
    {
        self.view.frame = AppFunc().appDelegate.viewFrame
        aView.addSubview(self.view)
        if animated
        {
            self.showAnimate()
        }
    }
    func showAnimate()
    {
        self.view.transform = CGAffineTransformMakeScale(1.3, 1.3)
        self.view.alpha = 0.0;
        UIView.animateWithDuration(0.25, animations: {
            self.view.alpha = 1.0
            self.view.transform = CGAffineTransformMakeScale(1.0, 1.0)
        });
    }
    
    @IBAction func closePopup(sender: AnyObject) {
        close()
    }
    func close(){
        UIView.animateWithDuration(0.25, animations: {
            self.view.transform = CGAffineTransformMakeScale(1.3, 1.3)
            self.view.alpha = 0.0;
            }, completion:{(finished : Bool)  in
                if (finished)
                {
                    self.view.removeFromSuperview()
                    AppFunc().appDelegate.isShowSearch = false
                }
        });
    }

    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}
extension String {
    
    func stringByAppendingPathComponent(path: String) -> String {
        
        let nsSt = self as NSString
        
        return nsSt.stringByAppendingPathComponent(path)
    }
}
