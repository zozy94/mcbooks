//
//  MediaItemCell.swift
//  MCBooks
//
//  Created by Quang Anh on 6/11/16.
//  Copyright © 2016 Quang Anh. All rights reserved.
//

import UIKit
import AVFoundation

class SearchItemCell: UITableViewCell {
    var book: Book!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var bookImage: UIImageView!
    func setup(item: Book){
        self.book = item
        self.nameLabel.text = item.name
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)) {
            if let thumb = AppFunc().getImage(URL.Thumb + item.image + "&width=100"){
                item.thumb = thumb
                let width = item.thumb!.size.width/1.4
                let height = item.thumb!.size.height*width/item.thumb!.size.width
                dispatch_async(dispatch_get_main_queue()) {
                    self.bookImage.image = item.thumb!.imageByCroppingImage(CGSizeMake(width, height))
                }
            }
        }
    }
}
