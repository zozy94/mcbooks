//
//  AddressInputView.swift
//  JupViec
//
//  Created by Quang Anh on 5/4/16.
//  Copyright © 2016 Quang Anh. All rights reserved.
//
import AVFoundation
import UIKit
@IBDesignable
class ScanQRView: UIViewController,AVCaptureMetadataOutputObjectsDelegate {
//    var view: UIView!
    var captureSession: AVCaptureSession!
    var previewLayer: AVCaptureVideoPreviewLayer!

    @IBOutlet weak var cameraView: UIView!
    @IBOutlet var popupView: UIView!
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
    }
    var connection: NSURLConnection!
    override init(nibName nibNameOrNil: String!, bundle nibBundleOrNil: NSBundle!) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    }
    var currentTask: NSURLSessionTask?
    override func viewDidLoad() {
        super.viewDidLoad()
//        view.backgroundColor = UIColor.blackColor()
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)) {
            self.captureSession = AVCaptureSession()
            
            let videoCaptureDevice = AVCaptureDevice.defaultDeviceWithMediaType(AVMediaTypeVideo)
            let videoInput: AVCaptureDeviceInput
            
            do {
                videoInput = try AVCaptureDeviceInput(device: videoCaptureDevice)
            } catch {
                return
            }
            
            if (self.captureSession.canAddInput(videoInput)) {
                self.captureSession.addInput(videoInput)
            } else {
                self.failed();
                return;
            }
            
            let metadataOutput = AVCaptureMetadataOutput()
            
            if (self.captureSession.canAddOutput(metadataOutput)) {
                self.captureSession.addOutput(metadataOutput)
                
                metadataOutput.setMetadataObjectsDelegate(self, queue: dispatch_get_main_queue())
                metadataOutput.metadataObjectTypes = metadataOutput.availableMetadataObjectTypes
            } else {
                self.failed()
                return
            }
            dispatch_async(dispatch_get_main_queue()) {
                self.previewLayer = AVCaptureVideoPreviewLayer(session: self.captureSession);
                self.previewLayer.videoGravity = AVLayerVideoGravityResizeAspectFill
                self.previewLayer.frame = self.cameraView.layer.bounds;
                self.previewLayer.videoGravity = AVLayerVideoGravityResizeAspectFill;
                self.cameraView.clipsToBounds = true
                self.cameraView.layer.addSublayer(self.previewLayer);
                self.captureSession.startRunning();
            }
        }
    }
    func restart(){
        self.captureSession.startRunning();
    }
    func failed() {
        let ac = UIAlertController(title: "Scanning not supported", message: "Your device does not support scanning a code from an item. Please use a device with a camera.", preferredStyle: .Alert)
        ac.addAction(UIAlertAction(title: "OK", style: .Default, handler: nil))
        presentViewController(ac, animated: true, completion: nil)
        captureSession = nil
    }
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        if (captureSession?.running == false) {
            captureSession.startRunning();
        }
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        
        if (captureSession?.running == true) {
            captureSession.stopRunning();
        }
    }
    
    func captureOutput(captureOutput: AVCaptureOutput!, didOutputMetadataObjects metadataObjects: [AnyObject]!, fromConnection connection: AVCaptureConnection!) {
        captureSession.stopRunning()
        
        if let metadataObject = metadataObjects.first {
            if let readableObject = metadataObject as? AVMetadataMachineReadableCodeObject {
                AudioServicesPlaySystemSound(SystemSoundID(kSystemSoundID_Vibrate))
                foundCode(readableObject.stringValue);
            } else {
                self.captureSession.stopRunning()
                    let alert = UIAlertController(title: "Không tìm thấy cuốn sách bạn yêu cầu", message: "", preferredStyle: .Alert)
                    alert.addAction(UIAlertAction(title: "OK", style: .Cancel,handler: { Void -> () in
                        self.captureSession.startRunning()
                    }))
                    self.presentViewController(alert, animated: true, completion: nil)
                }
        }
        dismissViewControllerAnimated(true, completion: nil)
    }
    
    func foundCode(code: String) {
        print(code)
        let CN = ConnectServer()
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)) {
            var status = 0
            if let json: AnyObject = CN.GETPageBook("scan?barcode=\(code)"){
                status =  json.valueForKey("code") as! Int
                print(status)
                if (status == 1){
                    if let result = json.valueForKey("result") {
                            let book = AppFunc().fetchBookData(result)
                            AppFunc().appDelegate.SelectedBook = book
                            print(book.name)
                        if let thumb = AppFunc().getImage(URL.Thumb + book.image + "&width=250"){
                            book.thumb = thumb
//                            let width = book.thumb!.size.width/1.4
//                            let height = book.thumb!.size.height*width/book.thumb!.size.width
                        }
                        dispatch_async(dispatch_get_main_queue()) {
                            self.captureSession.startRunning()

                                NSNotificationCenter.defaultCenter().postNotificationName("ShowBook", object: nil, userInfo: nil)
                        }
                        }
                } else {
                    dispatch_async(dispatch_get_main_queue()) {
                        self.captureSession.stopRunning()

                        let alert = UIAlertController(title: "Không tìm thấy cuốn sách bạn yêu cầu", message: "", preferredStyle: .Alert)
                        alert.addAction(UIAlertAction(title: "OK", style: .Cancel,handler: { Void -> () in 
                            self.captureSession.startRunning()
                            }))
                        self.presentViewController(alert, animated: true, completion: nil)
                    }
                }
            } else {
                dispatch_async(dispatch_get_main_queue()) {
                    self.captureSession.stopRunning()
                    let alert = UIAlertController(title: "Không tìm thấy cuốn sách bạn yêu cầu", message: "", preferredStyle: .Alert)
                    alert.addAction(UIAlertAction(title: "OK", style: .Cancel,handler: { Void -> () in
                        self.captureSession.startRunning()
                    }))
                    self.presentViewController(alert, animated: true, completion: nil)
                }
            }
        }
    }

    override func prefersStatusBarHidden() -> Bool {
        return true
    }
    
    override func supportedInterfaceOrientations() -> UIInterfaceOrientationMask {
        return .Portrait
    }

    func showInView(aView: UIView!, animated: Bool)
    {
        self.view.frame = AppFunc().appDelegate.viewFrame
        aView.addSubview(self.view)
        if animated
        {
            self.showAnimate()
        }
    }
    func showAnimate()
    {
        self.view.transform = CGAffineTransformMakeScale(1.3, 1.3)
        self.view.alpha = 0.0;
        UIView.animateWithDuration(0.25, animations: {
            self.view.alpha = 1.0
            self.view.transform = CGAffineTransformMakeScale(1.0, 1.0)
        });
    }
    
    @IBAction func closePopup(sender: AnyObject) {
        close()
    }
    func close(){
        UIView.animateWithDuration(0.25, animations: {
            self.view.transform = CGAffineTransformMakeScale(1.3, 1.3)
            self.view.alpha = 0.0;
            }, completion:{(finished : Bool)  in
                if (finished)
                {
                    self.view.removeFromSuperview()
                    AppFunc().appDelegate.isShowSearch = false
                }
        });
    }
    enum JSONError: String, ErrorType {
        case NoData = "ERROR: no data"
        case ConversionFailed = "ERROR: conversion from JSON failed"
    }

}
