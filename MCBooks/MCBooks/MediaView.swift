//
//  MediaView.swift
//  MCBooks
//
//  Created by Quang Anh on 6/11/16.
//  Copyright © 2016 Quang Anh. All rights reserved.
//

import UIKit
import AVFoundation

@IBDesignable
class MediaView: UIView {
    var view: UIView!
    //player
    
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var progressBar: UISlider!
    @IBOutlet weak var playTime: UILabel!
    @IBOutlet weak var remainTime: UILabel!
    @IBOutlet weak var centerIcon: UILabel!
    @IBOutlet weak var playButton: UIButton!
    @IBOutlet weak var indicator: UIActivityIndicatorView!
    var data = Array<Media>()
    var playerItem: AVPlayerItem!
    var player: AVPlayer!
    var isPlaying = false
    var Timer: NSTimer!
    var cellHeight:CGFloat = 0
    var playingID = ""
    //book info
    @IBOutlet weak var bookImage: UIImageView!
    @IBOutlet weak var bookName: UILabel!
    @IBOutlet weak var publisher: UILabel!
    @IBOutlet weak var author: UILabel!
    @IBOutlet weak var pageNum: UILabel!
    @IBOutlet weak var size: UILabel!
    @IBOutlet weak var rateStar: UILabel!
    @IBOutlet weak var price: UILabel!
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        xibSetup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        xibSetup()
    }
    
    func xibSetup() {
        view = loadViewFromNib()
        view.frame = bounds
        topView.backgroundColor = Color.theme
        indicator.hidden = true
        cellHeight = AppFunc().appDelegate.screenHeight/10
        data = AppFunc().appDelegate.SelectedBook.medias
//        tableView.allowsSelection = false
//        tableView.allowsSelection = false
        addSubview(view)
    }
    @IBAction func favouriteClick(sender: AnyObject) {
    }
    @IBAction func downloadClick(sender: AnyObject) {
    }
    
    func FavClick(sender: UIButton!) {
        print("Fav \(data[sender.tag].name)")
    }
    func DownloadClick(sender: UIButton!) {
        print("Down \(data[sender.tag].name)")
    }
    func updateSlider() {
        if player.currentItem?.status == AVPlayerItemStatus.ReadyToPlay {
            let time = Float(CMTimeGetSeconds(player.currentTime()))
            progressBar.value = time
            self.playTime.text = stringFromTimeInterval(NSTimeInterval(time))
        }
    }

    @IBAction func playClick(sender: AnyObject) {
        if(!isPlaying){
            if(player == nil){
                loadMedia("https://americanenglish.state.gov/files/ae/resource_files/guitar.mp3")
            } else {
                player.play()
                self.playButton.setTitle("", forState: .Normal)
            }
        } else {
            if player != nil {
                isPlaying = false
                player.pause()
                self.playButton.setTitle("", forState: .Normal)
            } else {
                loadMedia("https://americanenglish.state.gov/files/ae/resource_files/guitar.mp3")
            }
        }
    }
    func loadMedia(url: String){
        indicator.hidden = false
        centerIcon.hidden = true
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)) {
            let sound = NSURL(string: url)
            let asset = AVURLAsset(URL: sound!, options: nil)
            let audioDuration = asset.duration
            let audioDurationSeconds = CMTimeGetSeconds(audioDuration)
            self.progressBar.maximumValue = Float(audioDurationSeconds)
            asset.loadValuesAsynchronouslyForKeys(["tracks"], completionHandler: {
                self.playerItem = AVPlayerItem(asset: asset)
                self.player = AVPlayer(playerItem: self.playerItem)
                self.player.play()
                self.isPlaying = true
                dispatch_async(dispatch_get_main_queue()) {
                    self.playButton.setTitle("", forState: .Normal)
                    self.remainTime.text = self.stringFromTimeInterval(audioDurationSeconds)
                    self.indicator.hidden = true
                    self.centerIcon.hidden = false
                    self.Timer = NSTimer.scheduledTimerWithTimeInterval(0.1, target: self, selector: #selector(MediaView.updateSlider), userInfo: nil, repeats: true)
                }
            })
        }
    }
    func stringFromTimeInterval(interval: NSTimeInterval) -> String {
        let interval = Int(interval)
        let seconds = interval % 60
        let minutes = (interval / 60) % 60
        return String(format: "%02d:%02d", minutes, seconds)
    }
    @IBAction func forwardClick(sender: AnyObject) {
        if player != nil {
            player.seekToTime(CMTimeMakeWithSeconds(CMTimeGetSeconds(player.currentTime()) + 5, player.currentTime().timescale))
            self.playTime.text = stringFromTimeInterval(NSTimeInterval(progressBar.value))
        }
    }
    @IBAction func backwardClick(sender: AnyObject) {
        if player != nil {
            player.seekToTime(CMTimeMakeWithSeconds(CMTimeGetSeconds(player.currentTime()) - 5, player.currentTime().timescale))
            self.playTime.text = stringFromTimeInterval(NSTimeInterval(progressBar.value))
        }
    }
    
    @IBAction func ChangeAudioTime(sender: AnyObject) {
        if player != nil {
            player.pause()
            Timer.invalidate()
            player.seekToTime(CMTimeMake(Int64(progressBar.value), 1))
            self.playTime.text = stringFromTimeInterval(NSTimeInterval(progressBar.value))
            player.play()
            Timer.fire()
        } else {
            progressBar.value = 0
        }
    }
    func stop(){
        if player != nil {
            isPlaying = false
            self.playButton.setTitle("", forState: .Normal)
            player.pause()
        }
    }
    func loadViewFromNib() -> UIView {
        let bundle = NSBundle(forClass: self.dynamicType)
        let nib = UINib(nibName: "MediaView", bundle: bundle)
        let view = nib.instantiateWithOwner(self, options: nil)[0] as! UIView
        return view
    }
}