//
//  SecondViewController.swift
//  MCBooks
//
//  Created by Quang Anh on 6/1/16.
//  Copyright © 2016 Quang Anh. All rights reserved.
//

import UIKit

class DetailViewController: UIViewController {
    var viewpage = ""
    var showAll:ShowAllView!
    var appDelegate = AppFunc().appDelegate
//    var findButtonBtn:UIBarButtonItem!
//    var switchViewBtn:UIBarButtonItem!
    
    @IBOutlet weak var findButton: UIButton!
    
    @IBOutlet weak var switchButton: UIButton!
    var popupSearch: PopupSearch!

    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.frame = AppFunc().appDelegate.viewFrame
        let btn = UIBarButtonItem(title: " ", style: UIBarButtonItemStyle.Plain, target: nil, action: nil)
        self.navigationItem.backBarButtonItem = btn
        
        if(appDelegate.isGridView){
            switchButton.setTitle("",forState: .Normal) // list logo
        } else {
            switchButton.setTitle("",forState: .Normal) // grid logo
        }
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(DetailViewController.ShowBook), name: "ShowBook", object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(DetailViewController.ShowBook), name: "ShowBookSearch", object: nil)
        self.showAll = ShowAllView(frame: self.view.frame)
        self.view.addSubview(self.showAll)
        self.showAll.setURL(appDelegate.showID)
    }

    override func viewDidAppear(animated: Bool) {
    }
    

    @IBAction func findClick(sender: AnyObject) {
        findBook()
    }
    
    @IBAction func switchclick(sender: AnyObject) {
        switchView()
    }
    func findBook(){
        if !appDelegate.isShowSearch {
            popupSearch = PopupSearch(nibName: "PopupSearch", bundle: nil)
            popupSearch.showInView(self.view, animated: true)
        } else {
            if popupSearch != nil {
                popupSearch.close()
            }
        }
    }
    func switchView(){
        if(appDelegate.isGridView){
            setListView()
        } else {
            setGridView()
        }
        NSNotificationCenter.defaultCenter().postNotificationName("switchView", object: nil, userInfo: nil)
    }
    
    func setGridView(){
        switchButton.setTitle("",forState: .Normal) // list logo
        appDelegate.isGridView = true
    }
    func setListView(){
        switchButton.setTitle("",forState: .Normal) // grid logo
        appDelegate.isGridView = false
    }
    func ShowBook(){
        performSegueWithIdentifier("bookDetail", sender: self)
    }

    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if(segue.identifier == "bookDetail"){
            let VC = segue.destinationViewController as! BookDetailViewController
            VC.bookDetail = appDelegate.SelectedBook
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewWillDisappear(animated: Bool) {
        self.view = nil
//        self.navigationItem.setRightBarButtonItems([], animated: true)

        NSNotificationCenter.defaultCenter().removeObserver(self, name: "ShowBook", object: nil)
        NSNotificationCenter.defaultCenter().removeObserver(self, name: "ShowBookSearch", object: nil)

    }

}

