//
//  NavigationSegue.swift
//  LaundryApp
//
//  Created by ZoZy on 7/6/15.
//  Copyright (c) 2015 ZoZy. All rights reserved.
//

import UIKit

class NavigationSegue: UIStoryboardSegue {
    
    override func perform() {
        if(self.sourceViewController is RootViewController){
            let tabBarController = self.sourceViewController as! RootViewController
            let destinationController = self.destinationViewController
            
            for view in tabBarController.ViewHolder.subviews {
                view.removeFromSuperview()
            }
            // Add view to placeholder view
            
            //        for gesture in tabBarController.view.gestureRecognizers {
            //            println(gesture.locationInView)
            //        }
            //        if let recognizers = tabBarController.view.gestureRecognizers {
            //            for recognizer in recognizers {
            //                println("position \(recognizer.locationInView(tabBarController.view).x)")
            //            }
            //        }
            tabBarController.currentViewController = destinationController
            
            tabBarController.ViewHolder.addSubview(destinationController.view)
//            if(tabBarController.TransitionMode == .RIGHT) {
//                tabBarController.ViewHolder.slideInFromRight(0.4, completionDelegate: self)
//            } else if(tabBarController.TransitionMode == .LEFT) {
//                tabBarController.ViewHolder.slideInFromLeft(0.4, completionDelegate: self)
//            }
            // Set autoresizing
            tabBarController.ViewHolder.translatesAutoresizingMaskIntoConstraints = false
            destinationController.view.translatesAutoresizingMaskIntoConstraints = false
            
            let horizontalConstraint = NSLayoutConstraint.constraintsWithVisualFormat("H:|-(0)-[v1]-(0)-|", options: .AlignAllTop, metrics: nil, views: ["v1": destinationController.view])
            
            tabBarController.ViewHolder.addConstraints(horizontalConstraint)
            
            let verticalConstraint = NSLayoutConstraint.constraintsWithVisualFormat("V:|-0-[v1]-0-|", options: .AlignAllTop, metrics: nil, views: ["v1": destinationController.view])
            
            tabBarController.ViewHolder.addConstraints(verticalConstraint)
            
            tabBarController.ViewHolder.layoutIfNeeded()
            destinationController.didMoveToParentViewController(tabBarController)

        }         
        
    }
    
}
