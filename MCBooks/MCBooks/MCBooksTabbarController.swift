//
//  MCBooksTabbarController.swift
//  MCBooks
//
//  Created by Quang Anh on 6/1/16.
//  Copyright © 2016 Quang Anh. All rights reserved.
//

import UIKit

class MCbooksTabbarController: UITabBarController, UITabBarControllerDelegate {
    var button: UIButton = UIButton()
    var isHighLighted:Bool = false
    var MCtabBar: MCBooksTabBar!
    let appDelegate = AppFunc().appDelegate
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.delegate = self
        MCtabBar = MCBooksTabBar(frame: CGRectMake(0, 0, self.view.frame.width, 60))
        MCtabBar.center = self.tabBar.center
//        print(self.tabBar.frame.height)
        self.view.addSubview(MCtabBar)
        MCtabBar.homeButton.addTarget(self, action: #selector(MCbooksTabbarController.changeTabToMiddleTab(_:)), forControlEvents: .TouchUpInside)
        MCtabBar.giftButton.addTarget(self, action:  #selector(MCbooksTabbarController.changeTabToMiddleTab(_:)), forControlEvents: .TouchUpInside)
        MCtabBar.scanButton.addTarget(self, action: #selector(MCbooksTabbarController.changeTabToMiddleTab(_:)), forControlEvents: .TouchUpInside)
        MCtabBar.favButton.addTarget(self, action: #selector(MCbooksTabbarController.changeTabToMiddleTab(_:)), forControlEvents: .TouchUpInside)
        MCtabBar.profileButton.addTarget(self, action: #selector(MCbooksTabbarController.changeTabToMiddleTab(_:)), forControlEvents: .TouchUpInside)
        changeTabToMiddleTab(MCtabBar.homeButton)
    }
    
    func changeTabToMiddleTab(sender:UIButton)
    {
        let selectedIndex = sender.tag
//        var isFromRight = false
//        if(self.selectedIndex < selectedIndex) {
//            isFromRight = true
//        }
        self.selectedIndex = selectedIndex
        appDelegate.tabBarPage = selectedIndex
//        print("Select \(selectedIndex)")
        
        self.selectedViewController = (self.viewControllers as [AnyObject]?)?[selectedIndex] as? UIViewController
        let VC = self.selectedViewController as! UINavigationController
        (VC.childViewControllers[0] as! RootViewController).tabID = selectedIndex
//        if isFromRight {
//            self.selectedViewController!.view.slideInFromRight(0.4, completionDelegate: self)
//        } else {
//            self.selectedViewController!.view.slideInFromLeft(0.4, completionDelegate: self)
//        }
        dispatch_async(dispatch_get_main_queue(), {
            self.MCtabBar.selectButton(selectedIndex)
        });
    }
    func selectTab(index: Int){
        self.selectedIndex = index
        appDelegate.tabBarPage = index
        self.selectedViewController = (self.viewControllers as [AnyObject]?)?[selectedIndex] as? UIViewController
        let VC = self.selectedViewController as! UINavigationController
        (VC.childViewControllers[0] as! RootViewController).tabID = selectedIndex
        dispatch_async(dispatch_get_main_queue(), {
            self.MCtabBar.selectButton(index)
        });

    }
}
extension UIButton {
    func setBackgroundColor(color: UIColor, forState: UIControlState) {
        UIGraphicsBeginImageContext(CGSize(width: 1, height: 1))
        CGContextSetFillColorWithColor(UIGraphicsGetCurrentContext(), color.CGColor)
        CGContextFillRect(UIGraphicsGetCurrentContext(), CGRect(x: 0, y: 0, width: 1, height: 1))
        let colorImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        self.setBackgroundImage(colorImage, forState: forState)
    }}