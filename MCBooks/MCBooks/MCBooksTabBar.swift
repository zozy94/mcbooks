//
//  MCBooksTabBar.swift
//  MCBooks
//
//  Created by Quang Anh on 6/1/16.
//  Copyright © 2016 Quang Anh. All rights reserved.
//

import UIKit
@IBDesignable
class MCBooksTabBar: UIView {
    var view: UIView!
    
    @IBOutlet weak var homeButton: UIButton!
    @IBOutlet weak var scanButton: UIButton!
    @IBOutlet weak var giftButton: UIButton!
    @IBOutlet weak var favButton: UIButton!
    @IBOutlet weak var profileButton: UIButton!
    override init(frame: CGRect) {
        super.init(frame: frame)
        xibSetup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        xibSetup()
    }
    
    func xibSetup() {
        view = loadViewFromNib()
        view.frame = bounds
        homeButton.tag = 0
        homeButton.backgroundColor = Color.theme
        
        giftButton.tag = 1
        giftButton.backgroundColor = Color.theme

        scanButton.tag = 2
        scanButton.backgroundColor = Color.themelight
        
        favButton.tag = 3
        favButton.backgroundColor = Color.theme

        profileButton.tag = 4
        profileButton.backgroundColor = Color.theme

        addSubview(view)
    }
    func selectButton(tag: Int){
        switch(tag){
        case 0 :
            homeButton.backgroundColor = Color.themedark
            giftButton.backgroundColor = Color.theme
            scanButton.backgroundColor = Color.themelight
            favButton.backgroundColor = Color.theme
            profileButton.backgroundColor = Color.theme
            break
        case 1:
            homeButton.backgroundColor = Color.theme
            giftButton.backgroundColor = Color.themedark
            scanButton.backgroundColor = Color.themelight
            favButton.backgroundColor = Color.theme
            profileButton.backgroundColor = Color.theme
            break
        case 2:
            homeButton.backgroundColor = Color.theme
            giftButton.backgroundColor = Color.theme
            scanButton.backgroundColor = Color.themedark
            favButton.backgroundColor = Color.theme
            profileButton.backgroundColor = Color.theme
            break
        case 3:
            homeButton.backgroundColor = Color.theme
            giftButton.backgroundColor = Color.theme
            scanButton.backgroundColor = Color.themelight
            favButton.backgroundColor = Color.themedark
            profileButton.backgroundColor = Color.theme
            break
        case 4:
            homeButton.backgroundColor = Color.theme
            giftButton.backgroundColor = Color.theme
            scanButton.backgroundColor = Color.themelight
            favButton.backgroundColor = Color.theme
            profileButton.backgroundColor = Color.themedark
            break
        default:
            break
        }
    }
    func loadViewFromNib() -> UIView {
        let bundle = NSBundle(forClass: self.dynamicType)
        let nib = UINib(nibName: "MCBooksTabBar", bundle: bundle)
        let view = nib.instantiateWithOwner(self, options: nil)[0] as! UIView
        return view
    }
}