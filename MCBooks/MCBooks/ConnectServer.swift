//
//  ConnectServer.swift
//  JupViec
//
//  Created by Quang Anh on 5/6/16.
//  Copyright © 2016 Quang Anh. All rights reserved.
//

import Foundation

class ConnectServer {
    var token = ""
    init(){
        token = AppFunc().appDelegate.userData.token
    }
//    func checkToken(){
//        let myUrl = NSURL(string: URL.Server + "scan?barcode=123")
//        print(myUrl)
//        let request = NSMutableURLRequest(URL:myUrl!);
//        request.HTTPMethod = "GET";
//        request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
//        request.setValue("application/json", forHTTPHeaderField: "Accept")
//        request.setValue("Access_token " + token, forHTTPHeaderField: "Authorization")
//        var response: NSURLResponse?
//        do {
//            let urlData = try NSURLConnection.sendSynchronousRequest(request, returningResponse: &response)
//            if let jsonResult = try? NSJSONSerialization.JSONObjectWithData(urlData, options: NSJSONReadingOptions(rawValue: 0)) {
//                if (jsonResult.valueForKey("code") as! Int) == 2 {
//                    AppFunc().appDelegate.logout()
//                }
//            }
//        } catch let error1 as NSError {
//            print(error1)
//        }
//    }
    func GETPageBook(url: String) -> AnyObject?{
        if let myUrl = NSURL(string: URL.Server + url) {
        print(myUrl)
        let request = NSMutableURLRequest(URL:myUrl)
        request.HTTPMethod = "GET";
        request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        request.setValue("application/json", forHTTPHeaderField: "Accept")
        request.setValue("Access_token " + token, forHTTPHeaderField: "Authorization")
        print("Token = \(token)")
        var response: NSURLResponse?
        do {
            let urlData = try NSURLConnection.sendSynchronousRequest(request, returningResponse: &response)
            let jsonResult: AnyObject? = try? NSJSONSerialization.JSONObjectWithData(urlData, options: NSJSONReadingOptions(rawValue: 0))
            if jsonResult != nil {
                if (jsonResult!.valueForKey("code") as! Int) == 2 {
                    AppFunc().appDelegate.logout()
                }
            }
            return jsonResult
        } catch let error1 as NSError {
            print(error1)
        }
        }
        return nil
    }
    
    func POSTwAuth(params: [String: AnyObject],url: String) -> AnyObject? {
        let parameterString = params.stringFromHttpParameters()
        let myUrl = NSURL(string: URL.Server + url)
        
        let request = NSMutableURLRequest(URL: myUrl!);
        request.HTTPMethod = "POST";
        request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        request.HTTPBody = parameterString.dataUsingEncoding(NSUTF8StringEncoding);
        request.setValue("Access_token " + token, forHTTPHeaderField: "Authorization")

        var response: NSURLResponse?
        do {
            let urlData = try NSURLConnection.sendSynchronousRequest(request, returningResponse: &response)
            let jsonResult: AnyObject? = try? NSJSONSerialization.JSONObjectWithData(urlData, options: NSJSONReadingOptions(rawValue: 0))
            if jsonResult != nil {
                if (jsonResult!.valueForKey("code") as! Int) == 2 {
                    AppFunc().appDelegate.logout()
                }
            }
            return jsonResult
        } catch let error1 as NSError {
            //            error = error1
            print(error1)
        }
        return nil
    }
    func PUTwAuth(params: [String: AnyObject]?,url: String) -> AnyObject?{
        let parameterString = params!.stringFromHttpParameters()
        let myUrl = URL.Server + url
        let requestURL = NSURL(string:"\(myUrl)?\(parameterString)")!
        
        print(requestURL)
        let request = NSMutableURLRequest(URL:requestURL);
        request.HTTPMethod = "PUT";
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.setValue("application/json", forHTTPHeaderField: "Accept")
        request.setValue("Access_token " + token, forHTTPHeaderField: "Authorization")

        var response: NSURLResponse?
        do {
            let urlData = try NSURLConnection.sendSynchronousRequest(request, returningResponse: &response)
            let jsonResult: AnyObject? = try? NSJSONSerialization.JSONObjectWithData(urlData, options: NSJSONReadingOptions(rawValue: 0))
            if jsonResult != nil {
                if (jsonResult!.valueForKey("code") as! Int) == 2 {
                    AppFunc().appDelegate.logout()
                }
            }
            //            println(jsonResult)
            return jsonResult
        } catch let error1 as NSError {
            //            error = error1
            print(error1)
        }
        return nil
    }

    func DELETE(url: String) -> AnyObject?{
        let myUrl = NSURL(string: URL.Server + url)
        let request = NSMutableURLRequest(URL: myUrl!);
        request.HTTPMethod = "DELETE";
        request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        request.setValue("Access_token " + token, forHTTPHeaderField: "Authorization")

        var response: NSURLResponse?
        do {
            let urlData = try NSURLConnection.sendSynchronousRequest(request, returningResponse: &response)
            let jsonResult: AnyObject? = try? NSJSONSerialization.JSONObjectWithData(urlData, options: NSJSONReadingOptions(rawValue: 0))
            return jsonResult
        } catch let error1 as NSError {
            //            error = error1
            print(error1)
        }
        return nil
    }

    func POST(params: [String: AnyObject],url: String) -> AnyObject?{
        let parameterString = params.stringFromHttpParameters()
        let myUrl = NSURL(string: URL.Server + url)
        
        let request = NSMutableURLRequest(URL: myUrl!);
        request.HTTPMethod = "POST";
        request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        request.HTTPBody = parameterString.dataUsingEncoding(NSUTF8StringEncoding);
        
        var response: NSURLResponse?
        do {
            let urlData = try NSURLConnection.sendSynchronousRequest(request, returningResponse: &response)
            let jsonResult: AnyObject? = try? NSJSONSerialization.JSONObjectWithData(urlData, options: NSJSONReadingOptions(rawValue: 0))
            return jsonResult
        } catch let error1 as NSError {
//            error = error1
            print(error1)
        }
        return nil
    }
    func PUT(params: [String: AnyObject]?,url: String) -> AnyObject?{
        let parameterString = params!.stringFromHttpParameters()
        let myUrl = URL.Server + url
        let requestURL = NSURL(string:"\(myUrl)?\(parameterString)")!
        
        print(requestURL)
        let request = NSMutableURLRequest(URL:requestURL);
        request.HTTPMethod = "PUT";
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.setValue("application/json", forHTTPHeaderField: "Accept")
        var response: NSURLResponse?
        do {
            let urlData = try NSURLConnection.sendSynchronousRequest(request, returningResponse: &response)
            let jsonResult: AnyObject? = try? NSJSONSerialization.JSONObjectWithData(urlData, options: NSJSONReadingOptions(rawValue: 0))
            //            println(jsonResult)
            return jsonResult
        } catch let error1 as NSError {
            //            error = error1
            print(error1)
        }
        return nil
    }
    func GET(url: String) -> AnyObject?{
        let myUrl = NSURL(string: URL.Server + url)
        print(myUrl)
        let request = NSMutableURLRequest(URL:myUrl!);
        request.HTTPMethod = "GET";
        
        request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        request.setValue("application/json", forHTTPHeaderField: "Accept")
        
        var response: NSURLResponse?
        do {
            let urlData = try NSURLConnection.sendSynchronousRequest(request, returningResponse: &response)
            let jsonResult: AnyObject? = try? NSJSONSerialization.JSONObjectWithData(urlData, options: NSJSONReadingOptions(rawValue: 0))
            return jsonResult
        } catch let error1 as NSError {
            print(error1)
        }
        return nil
    }

}