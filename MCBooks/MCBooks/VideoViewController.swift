//
//  VideoViewController.swift
//  MCBooks
//
//  Created by Quang Anh on 6/23/16.
//  Copyright © 2016 Quang Anh. All rights reserved.
//

import UIKit
class VideoViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UIWebViewDelegate {
    var data = Array<Media>()
    @IBOutlet weak var webView: UIWebView!
    @IBOutlet weak var tableView: UITableView!
    var playingID = ""
    
    override func viewDidLoad() {
        data = AppFunc().appDelegate.SelectedBook.video()
        webView.delegate = self
        
        webView.allowsInlineMediaPlayback = true
        webView.sizeThatFits(webView.bounds.size)
        webView.scalesPageToFit = true
        webView.scrollView.scrollEnabled = false
        webView.contentMode = UIViewContentMode.ScaleAspectFit

        loadVideo(data[0].url)
        playingID = data[0].id
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data.count
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let rowData = data[indexPath.row]
        if playingID != rowData.id {
            playingID = rowData.id
            loadVideo(rowData.url)
            tableView.reloadData()
        }
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 80
    }
    func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
        cell.selectionStyle = UITableViewCellSelectionStyle.None
    }
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("mediaItemCell") as! MediaItemCell
        if playingID == data[indexPath.row].id {
            cell.setPlaying(true)
        } else {
            cell.setPlaying(false)
        }
        data[indexPath.row].isFav = AppFunc().appDelegate.isFavouriteMedia(data[indexPath.row].id)
        cell.setup(data[indexPath.row])
        return cell
    }
    func loadVideo(url: String){
        let id = getID(url)
        let embededHTML = "<html><style> #map_container {position: relative;width: 100%;padding-bottom: 60%;} #map { position:absolute;width:100%; height:100%;}</style> <body id=\"map_container\" style=\"background: #0173bc;\"><iframe  id=\"map\" src=\"https://www.youtube.com/embed/\(id)?&playsinline=1&rel=0&autoplay=1\" width=\"100%%\" height=\"100%%\" frameborder=\"0\" allowfullscreen></iframe></body></html>"
        print(embededHTML)
        webView.loadHTMLString(embededHTML, baseURL: nil)
    }
    func getID(url: String) -> String{
        print(url)
        var id = ""
        let stop: Character = "="
        for index in url.characters.indices {
            let char = url[index]
            if char == stop {
                id = url.substringFromIndex(index.advancedBy(1))
                break
            }
        }
        return id
    }
    func webViewDidFinishLoad(webView: UIWebView) {
    }
    func webViewDidStartLoad(webView: UIWebView) {
        
    }
}
extension String {
    subscript(integerIndex: Int) -> Character {
        let index = startIndex.advancedBy(integerIndex)
        return self[index]
    }
    
    subscript(integerRange: Range<Int>) -> String {
        let start = startIndex.advancedBy(integerRange.startIndex)
        let end = startIndex.advancedBy(integerRange.endIndex)
        let range = start..<end
        return self[range]
    }
}
