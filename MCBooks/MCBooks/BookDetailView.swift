//
//  BookDetailView.swift
//  MCBooks
//
//  Created by Quang Anh on 6/10/16.
//  Copyright © 2016 Quang Anh. All rights reserved.
//

import UIKit
@available(iOS 9.0, *)
@IBDesignable
class BookDetailView: UIView {
    var view: UIView!
    @IBOutlet weak var wrapView: UIView!
    @IBOutlet weak var buyButton: UIButton!
    @IBOutlet weak var bookImage: UIImageView!
    @IBOutlet weak var bookName: UILabel!
    @IBOutlet weak var publisher: UILabel!
    @IBOutlet weak var author: UILabel!
    @IBOutlet weak var pageNumber: UILabel!
    @IBOutlet weak var size: UILabel!
    @IBOutlet weak var starRate: UILabel!
    @IBOutlet weak var price: UILabel!
    @IBOutlet weak var giftView: UIStackView!
    @IBOutlet weak var giftLabel: UILabel!
    @IBOutlet weak var desLabel: UILabel!
    @IBOutlet weak var readButton: UIButton!
    @IBOutlet weak var favButton: UIButton!
    @IBOutlet weak var mediaButton: UIButton!
    
    @IBOutlet weak var moreBtn: UIButton!
    var bookDetail = Book()
    override init(frame: CGRect) {
        super.init(frame: frame)
        xibSetup()
    }
    func setupBook(book: Book){
        print(book.author)
        bookImage.image = book.thumb
        bookName.text = book.name
        publisher.text = "NXB: " + book.publisher
        author.text = "Tác giả: " + book.author
        starRate.text = AppFunc().ratingLabel(book.avg_star)
        price.text = "  " + AppFunc().decimalNum(book.price) + " vnđ  "
        desLabel.text = book.description
        if(book.sale.count>0){
            giftView.hidden = false
            var salestring = ""
            for item in book.sale {
                if(item != ""){
                    salestring += item + " "
                }
            }
            if(salestring == ""){
                giftView.hidden = true
            } else {
                giftLabel.text = salestring
            }
        } else {
            giftView.hidden = true
        }
//        desLabel.text = book.
    }
        required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        xibSetup()
    }
    
    func xibSetup() {
        view = loadViewFromNib()
        view.frame = bounds
        readButton.layer.cornerRadius = 4
        favButton.layer.cornerRadius = 4
        mediaButton.layer.cornerRadius = 4
        buyButton.layer.cornerRadius = 4

        readButton.layer.borderColor = Color.theme.CGColor
        favButton.layer.borderColor = Color.theme.CGColor
        mediaButton.layer.borderColor = Color.theme.CGColor
        
        readButton.layer.borderWidth = 1
        favButton.layer.borderWidth = 1
        mediaButton.layer.borderWidth = 1
        
        price.layer.cornerRadius = 3
        price.layer.borderColor = Color.theme.CGColor
        price.layer.borderWidth = 0.6
        addSubview(view)
    }
    @IBAction func moreButton(sender: AnyObject) {
        if(desLabel.numberOfLines == 5){
            desLabel.numberOfLines = 0
            moreBtn.setTitle("Thu gọn", forState: .Normal)
        } else {
            desLabel.numberOfLines = 5
            moreBtn.setTitle("Xem thêm", forState: .Normal)
        }
//        self.scrollView.contentSize = self.wrapView.bounds.size
//        self.scrollView.setNeedsLayout()
    }
    @IBAction func previewButton(sender: AnyObject) {
        NSNotificationCenter.defaultCenter().postNotificationName("PreviewBook", object: nil, userInfo: nil)
    }
    
    @IBAction func buyButtonClick(sender: AnyObject) {
        NSNotificationCenter.defaultCenter().postNotificationName("BuyBook", object: nil, userInfo: nil)
    }
    
    @IBAction func mediaClick(sender: AnyObject) {
        NSNotificationCenter.defaultCenter().postNotificationName("ShowMedia", object: nil, userInfo: nil)
    }
    
    func loadViewFromNib() -> UIView {
        let bundle = NSBundle(forClass: self.dynamicType)
        let nib = UINib(nibName: "BookDetailView", bundle: bundle)
        let view = nib.instantiateWithOwner(self, options: nil)[0] as! UIView
        return view
    }
}