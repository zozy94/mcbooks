//
//  BookDetailViewController.swift
//  MCBooks
//
//  Created by Quang Anh on 6/17/16.
//  Copyright © 2016 Quang Anh. All rights reserved.
//

import UIKit

class BookDetailViewController: UIViewController, UITextFieldDelegate {
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var scrollView: UIScrollView!
    var viewpage = ""
    var bookDetail = Book()
    var popupSearch: PopupSearch!

    @IBOutlet weak var buyButton: UIButton!
    @IBOutlet weak var bookImage: UIImageView!
    @IBOutlet weak var bookName: UILabel!
    @IBOutlet weak var publisher: UILabel!
    @IBOutlet weak var author: UILabel!
    @IBOutlet weak var starRate: UILabel!
    @IBOutlet weak var price: UILabel!
    @IBOutlet weak var giftView: UIView!
    @IBOutlet weak var priceView: UIView!
    @IBOutlet weak var favButton: UIButton!
    @IBOutlet weak var desLabel: UILabel!
    @IBOutlet weak var readButton: UIButton!
    @IBOutlet weak var videoButton: UIButton!
    @IBOutlet weak var mediaButton: UIButton!
    
    @IBOutlet weak var moreBtn: UIButton!
    var searchButton:UIBarButtonItem!
    @IBOutlet var RateStar: [UIButton]!
    @IBOutlet weak var commentView: UIView!
    
    @IBOutlet weak var textField: UITextField!
    @IBOutlet weak var avatar: UIImageView!
    var rating = 0
    var appDelegate: AppDelegate!
    var originHeight: CGFloat!
    var originDesHeight: CGFloat!
    var originFrame: CGRect!
    var frame:CGRect!
 
    override func viewDidLoad() {
        self.appDelegate = AppFunc().appDelegate
        
        frame = CGRectMake(0, 64, self.view.frame.width, self.view.frame.height-115)
        let btn = UIBarButtonItem(title: " ", style: UIBarButtonItemStyle.Plain, target: nil, action: nil)
        self.navigationItem.backBarButtonItem = btn
        
        readButton.layer.cornerRadius = 4
        videoButton.layer.cornerRadius = 4
        mediaButton.layer.cornerRadius = 4
        buyButton.layer.cornerRadius = 4
        giftView.layer.cornerRadius = 4
        
        readButton.layer.borderColor = Color.theme.CGColor
        videoButton.layer.borderColor = Color.theme.CGColor
        mediaButton.layer.borderColor = Color.theme.CGColor
        giftView.layer.borderColor = Color.red.CGColor
        
        readButton.layer.borderWidth = 1
        videoButton.layer.borderWidth = 1
        mediaButton.layer.borderWidth = 1
        giftView.layer.borderWidth = 1

        priceView.layer.cornerRadius = 3
        priceView.layer.borderColor = Color.theme.CGColor
        priceView.layer.borderWidth = 0.6
        let gesture = UITapGestureRecognizer(target: self, action: #selector(BookDetailViewController.tap(_:)))
        giftView.addGestureRecognizer(gesture)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(BookDetailViewController.ShowBook), name: "ShowBookSearch", object: nil)
        avatar.image = appDelegate.userData.avatar
        avatar.clipsToBounds = true
        avatar.layer.cornerRadius = avatar.frame.width/2
        textField.delegate = self
        searchButton = UIBarButtonItem(title: "", style: UIBarButtonItemStyle.Plain, target: self, action: #selector(BookDetailViewController.showSearch))
        searchButton.setTitleTextAttributes([
            NSFontAttributeName: UIFont(name: "FontAwesome", size: 26.0)!,
            NSForegroundColorAttributeName: UIColor.whiteColor()], forState: .Normal)
        self.navigationItem.setRightBarButtonItems([searchButton], animated: true)
        let Bookgesture = UITapGestureRecognizer(target: self, action: #selector(BookDetailViewController.imageTapped(_:)))
        bookImage.addGestureRecognizer(Bookgesture)
        bookImage.userInteractionEnabled = true

    }
    override func viewWillAppear(animated: Bool) {
//        self.view.frame = appDelegate.viewFrame
//        self.scrollView.frame = self.view.frame
        originHeight = self.contentView.frame.height
        originDesHeight = self.desLabel.frame.height
        originFrame = self.scrollView.frame

        bookDetail = appDelegate.getBook(bookDetail.id)
        self.setupBook(bookDetail)
        if(self.bookDetail.isFavourite){
            favButton.setTitle("", forState: .Normal)
            favButton.setTitleColor(Color.red, forState: .Normal)
        } else {
            favButton.setTitle("", forState: .Normal)
            favButton.setTitleColor(Color.theme, forState: .Normal)
        }
        refreshScrollView()
        print("Will appear frame \(self.view.frame)")
        scrollView.contentSize = contentView.bounds.size
        scrollView.frame = CGRectMake(0,0,frame.width,frame.height-100)
        
        scrollView.setNeedsLayout()
        print("Afterlayout \(self.view.frame)")
        

    }
    override func viewDidAppear(animated: Bool) {
        print("Did appear 1 frame \(self.view.frame)")

        if(self.bookDetail.isFavourite){
            favButton.setTitle("", forState: .Normal)
            favButton.setTitleColor(Color.red, forState: .Normal)
        } else {
            favButton.setTitle("", forState: .Normal)
            favButton.setTitleColor(Color.theme, forState: .Normal)
        }
        print("Did appear before load comment frame \(self.view.frame)")

        loadComment()
        refreshScrollView()
        print("Did appear frame \(self.view.frame)")
    }
    @IBAction func rateClick(sender: AnyObject) {
        let rate = RateStar.indexOf(sender as! UIButton)
        let index = abs(Int((rate?.distanceTo(RateStar.startIndex))!))
        rating = index + 1
        for i in 0..<RateStar.count {
            if(i<=index){
                RateStar[i].setTitleColor(Color.theme, forState: .Normal)
            } else {
                RateStar[i].setTitleColor(UIColor.lightGrayColor(), forState: .Normal)
            }
        }
        refreshScrollView()
    }

    func tap (sender:UITapGestureRecognizer){
        var salestring = ""
        for item in bookDetail.sale {
            salestring += item + "\n"
        }
        let alert = UIAlertController(title: "Khuyến mại", message: salestring, preferredStyle: .Alert)
        alert.addAction(UIAlertAction(title: "OK", style: .Cancel, handler: nil))
        presentViewController(alert, animated: true, completion: nil)
    }
    func ShowBook(){
        self.bookDetail = appDelegate.SelectedBook
        self.setupBook(bookDetail)
        loadComment()
        refreshScrollView()
    }
    @IBAction func favTapped(sender: AnyObject) {
        FavTapped()
    }

    func setupBook(book: Book){
//        print(book.author)
        if book.thumb != nil {
            self.bookImage.image = book.thumb
        }
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)) {
            if let thumb = AppFunc().getImage(URL.Thumb + book.image){
                dispatch_async(dispatch_get_main_queue()) {
                    if self.bookImage != nil {
                        self.bookImage.image = thumb
                    }
                }
            }
        }
        if appDelegate.isFavouriteBook(book.id){
            bookDetail.isFavourite = true
            favButton.setTitle("", forState: .Normal)
            favButton.setTitleColor(Color.red, forState: .Normal)
        } else {
            bookDetail.isFavourite = false
            favButton.setTitle("", forState: .Normal)
            favButton.setTitleColor(Color.theme, forState: .Normal)
        }
        bookName.text = book.name
        publisher.text = "NXB: " + book.publisher
        author.text = "Tác giả: " + book.author
        starRate.text = AppFunc().ratingLabel(book.avg_star)
        price.text = AppFunc().decimalNum(book.price) + " vnđ"
        desLabel.text = book.description
        if(book.sale.count>0){
            giftView.hidden = false
            var salestring = ""
            for item in book.sale {
                if(item != ""){
                    salestring += item + " "
                }
            }
            if(salestring == ""){
                giftView.hidden = true
            } else {
            }
        } else {
            giftView.hidden = true
        }
        let audio = book.audio()
        if audio.count == 0 {
            self.mediaButton.enabled = false
            mediaButton.layer.borderColor = Color.gray.CGColor
            mediaButton.setTitleColor(Color.gray, forState: .Disabled)

        }
        let video = book.video()
        if video.count == 0 {
            self.videoButton.enabled = false
            videoButton.layer.borderColor = Color.gray.CGColor
            videoButton.setTitleColor(Color.gray, forState: .Disabled)
        }
    }

    @IBAction func moreButton(sender: AnyObject) {
        if(desLabel.numberOfLines == 5){
            desLabel.numberOfLines = 0
            moreBtn.setTitle("Thu gọn", forState: .Normal)
            
        } else {
            desLabel.numberOfLines = 5
            moreBtn.setTitle("Xem thêm", forState: .Normal)
        }
    }
    @IBAction func previewButton(sender: AnyObject) {
        if(viewpage != "PreviewBook"){
            viewpage = "PreviewBook"
            performSegueWithIdentifier("showContent", sender: self)
        }
    }
    @IBAction func buyButtonClick(sender: AnyObject) {
        let url:NSURL = NSURL(string: bookDetail.buyURL)!
        UIApplication.sharedApplication().openURL(url)
    }
    
    @IBAction func mediaClick(sender: AnyObject) {
        viewpage = "Media"
        performSegueWithIdentifier("audioView", sender: self)

    }
    @IBAction func videoClick(sender: AnyObject) {
        performSegueWithIdentifier("videoView", sender: self)

    }
    func showSearch(){
        if !appDelegate.isShowSearch {
            popupSearch = PopupSearch(nibName: "PopupSearch", bundle: nil)
            popupSearch.showInView(self.view, animated: true)
        } else {
            if popupSearch != nil {
            popupSearch.close()
            }
        }
    }
    
    func FavTapped(){
        if(!bookDetail.isFavourite){
            bookDetail.isFavourite = true
            favButton.setTitle("", forState: .Normal)
            favButton.setTitleColor(Color.red, forState: .Normal)
            appDelegate.addFavourite(bookDetail.id)
            let CN = ConnectServer()
            let params : [String: AnyObject] = [:]
            let alert = AlertController()
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)) {
                self.appDelegate.setFavourite(self.bookDetail.id,isFav: true)
                var status = 0
                var mess = ""
                if let json: AnyObject = CN.POSTwAuth(params,url: "favorite/books/" + self.bookDetail.id){
                    print(json)
                    status =  json.valueForKey("code") as! Int
                } else {
                    mess = "Không thể kết nối tới máy chủ"
                }
                dispatch_async(dispatch_get_main_queue()) {
                    if (status == 0){
                        alert.showAlert("Có lỗi xảy ra",mess: mess)
                    }
                }
            }
        } else {
            bookDetail.isFavourite = false
            appDelegate.removeFavourite(bookDetail.id)

            favButton.setTitle("", forState: .Normal)
            favButton.setTitleColor(Color.theme, forState: .Normal)
            let CN = ConnectServer()
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)) {
                self.appDelegate.setFavourite(self.bookDetail.id,isFav: false)
                if let json: AnyObject = CN.DELETE("favorite/books/" + self.bookDetail.id){
                    print(json)
//                    status =  json.valueForKey("code") as! Int
                } else {
//                    mess = "Không thể kết nối tới máy chủ"
                }
                dispatch_async(dispatch_get_main_queue()) {
                   
                }
            }
        }

    }
    
    func loadComment(){
        for item in commentView.subviews {
            item.removeFromSuperview()
        }
        AppFunc().reloadBookData()
        listComment.removeAll()
        let myrate = bookDetail.myrate
        if myrate.comment != "" {
            print("My rate \(myrate.comment)")
            myrate.name = appDelegate.userData.name
            if let avatar = appDelegate.userData.avatar {
                myrate.avatar = avatar
            }
            let myRate = CommentView(frame: CGRectMake(0,0,commentView.frame.width,100))
            myRate.setup(myrate)
            commentView.addSubview(myRate)
            listComment.append(myrate)
        }
        loadAllRate()
    }
    func reloadComment(){
        for item in commentView.subviews {
            item.removeFromSuperview()
        }
        let myrate = bookDetail.myrate
        if myrate.comment != "" {
            myrate.name = appDelegate.userData.name
            myrate.avatar = appDelegate.userData.avatar!
            if listComment.count > 0 && listComment[0].name == myrate.name {
                listComment[0] = myrate
            } else {
                listComment.insert(myrate, atIndex: 0)
            }
            self.bookDetail.myrate = myrate
        }
        for i in 0..<listComment.count {
            let rateView = CommentView(frame: CGRectMake(0,CGFloat(i*100),self.commentView.frame.width,100))
            rateView.setup(listComment[i])
//            self.contentView.frame = CGRectMake(0, 0,self.contentView.frame.width, originHeight + CGFloat(i*100))
            self.commentView.addSubview(rateView)
//            commentView.frame = CGRectMake(commentView.frame.minX, commentView.frame.minY,commentView.frame.width, CGFloat(i*100))
            self.contentView.frame = CGRectMake(0, 0,self.contentView.frame.width, self.originHeight + CGFloat(self.listComment.count*100))
            self.contentView.setNeedsLayout()
            self.contentView.setNeedsDisplay()
        }
    }
    func refreshScrollView(){
        for i in 0..<listComment.count {
            let rateView = CommentView(frame: CGRectMake(0,CGFloat(i*100),self.commentView.frame.width,100))
            rateView.setup(listComment[i])
//            self.contentView.frame = CGRectMake(0, 0,self.contentView.frame.width, originHeight + CGFloat(i*100))
            self.commentView.addSubview(rateView)
//            commentView.frame = CGRectMake(commentView.frame.minX, commentView.frame.minY,commentView.frame.width, CGFloat(i*100))
            print(CGFloat(i*100))
            self.contentView.frame = CGRectMake(0, 0,self.contentView.frame.width, self.originHeight + CGFloat(self.listComment.count*100))
            self.contentView.setNeedsLayout()
            self.contentView.setNeedsDisplay()

        }
    }
    @IBAction func postComment(sender: AnyObject) {
        textField.resignFirstResponder()
        let alert = AlertController()
        if rating == 0 {
            alert.showAlert("Không thể gửi phản hồi", mess: "Vui lòng chọn điểm đánh giá")
            refreshScrollView()
        } else {
            alert.ShowIndicator()
            var params : [String: AnyObject] = [:]
            
            params["stars"] = rating
            params["comment"] = textField.text
            let CN = ConnectServer()
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)) {
                var status = 0
                if let json: AnyObject = CN.POSTwAuth(params,url: "books/" + self.bookDetail.id + "/ratings"){
                    status =  json.valueForKey("code") as! Int
                    if (status == 1){
                        let myrate = Rate()
                        myrate.date = AppFunc().getDate(NSDate())
                        myrate.name = self.appDelegate.userData.name
                        myrate.avatar = self.appDelegate.userData.avatar!
                        myrate.comment = self.textField.text!
                        myrate.stars = Double(self.rating)
                        self.bookDetail.myrate = myrate
                        self.appDelegate.updateBook(self.bookDetail)
                    }
                }
                dispatch_async(dispatch_get_main_queue()) {
                    alert.DismissIndicator()
                    self.reloadComment()
                }
            }
        }
    }
    var page = 1
    var listComment = Array<Rate>()
    func loadAllRate(){
        let CN = ConnectServer()
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)) {
            var status = 0
            if let json: AnyObject = CN.GETPageBook("books/" + self.bookDetail.id + "/ratings?page=\(self.page)"){
                status =  json.valueForKey("code") as! Int
                if (status == 1){
                    let result = json.valueForKey("result") as! Array<AnyObject>
                    for item in result {
                        let rate = Rate()
                        rate.comment = item.valueForKey("comment") as! String
                        let date = item.valueForKey("create_at") as! Int
                        rate.date = NSDate(timeIntervalSince1970: NSTimeInterval(date/1000)).formattedISO8601
                        if let star = item.valueForKey("stars") as? Double {
                            rate.stars = star
                        }
                        let assessor = item.valueForKey("assessor")! as AnyObject
                        if let name = assessor.valueForKey("name") as? String {
                            rate.name = name
                        } else {
                            rate.name = "Anonymous"
                        }
                        rate.image = assessor.valueForKey("avatar") as! String
                        if let image = AppFunc().getImage(rate.image) {
                            rate.avatar = image
                        }
                        dispatch_async(dispatch_get_main_queue()) {
                            if self.commentView != nil {
                                let rateView = CommentView(frame: CGRectMake(0,CGFloat(self.listComment.count*100),self.commentView.frame.width,100))
                                rateView.setup(rate)
                                self.commentView.addSubview(rateView)
                                self.listComment.append(rate)
                                
                                //                                self.commentView.frame = CGRectMake(self.commentView.frame.minX, self.commentView.frame.minY,self.commentView.frame.width, CGFloat(self.listComment.count*100))
                                //                                print(CGFloat(self.commentView.frame.width*100))

                                self.contentView.frame = CGRectMake(0, 0,self.contentView.frame.width, self.originHeight + CGFloat(self.listComment.count*100))
                                self.contentView.setNeedsLayout()
                                self.contentView.setNeedsDisplay()

                            }
                        }
                    }
                }
            }
        }
        
    }


    override func viewDidDisappear(animated: Bool) {
                self.view = nil

    }
    override func viewDidLayoutSubviews() {
//        self.view.frame = frame
        if self.desLabel.frame.height != originDesHeight && originHeight != nil {
            let deltaY = self.desLabel.frame.height - originDesHeight
            originHeight = originHeight + deltaY
            originDesHeight = self.desLabel.frame.height
            refreshScrollView()
        }
        scrollView.contentSize = contentView.bounds.size
        scrollView.frame = CGRectMake(0,0,frame.width,frame.height)
        scrollView.setNeedsLayout()
        print("Afterlayout \(self.view.frame)")

    }
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if(segue.identifier == "showContent"){
            let VC = segue.destinationViewController as! WebViewController
            VC.URL = URL.Image + self.bookDetail.preview
        }
    }
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    var keyboardHeight:CGFloat = 0
    var delta:CGFloat = 0
    var parentOriFrame: CGRect!
    func textFieldDidBeginEditing(textField: UITextField) {
        parentOriFrame = self.view.frame
        PushUpKeyboard(self.view, textField: self.textField, originalFrame: self.view.frame)

    }
    func imageTapped(sender: UITapGestureRecognizer) {
        print("image tapped")
        let imageView = sender.view as! UIImageView
        let newImageView = UIImageView(image: imageView.image)
        let windowFrame = UIApplication.sharedApplication().windows.first!.frame
        newImageView.frame = windowFrame
        newImageView.backgroundColor = .blackColor()
        newImageView.contentMode = .ScaleAspectFit
        newImageView.userInteractionEnabled = true
        let tap = UITapGestureRecognizer(target: self, action: #selector(BookDetailViewController.dismissFullscreenImage(_:)))
        newImageView.addGestureRecognizer(tap)
        UIApplication.currentViewController()?.view.addSubview(newImageView)
        newImageView.transform = CGAffineTransformMakeScale(1.3, 1.3)
        newImageView.alpha = 0.0
        UIView.animateWithDuration(0.25, animations: {
            newImageView.alpha = 1.0
            newImageView.transform = CGAffineTransformMakeScale(1.0, 1.0)
        });
    }
    
    func dismissFullscreenImage(sender: UITapGestureRecognizer) {
        sender.view?.transform = CGAffineTransformMakeScale(1.0, 1.0)
        sender.view?.alpha = 1.0
        UIView.animateWithDuration(0.25, animations: {
        });
        UIView.animateWithDuration(0.25, animations: {
            sender.view?.alpha = 0.0
            sender.view?.transform = CGAffineTransformMakeScale(1.3, 1.3)
            }, completion: { (value: Bool) in
                sender.view?.removeFromSuperview()
        })
    }

    // Keyboard constant
    var keyboarHeight:CGFloat = 200
    var MINIMUM_SCROLL_FRACTION:CGFloat = 0.2
    var MAXIMUM_SCROLL_FRACTION:CGFloat = 0.8
    var animatedDistance: CGFloat!
    func PushUpKeyboard(view: UIView, textField: UITextField, originalFrame: CGRect){
        if let textFieldRect = view.window?.convertRect(textField.bounds, fromView: textField) {
            let viewRect: CGRect = view.window!.convertRect(view.bounds, fromView: view)
            let midline = textFieldRect.origin.y + 0.5 * textFieldRect.size.height
            if(midline > (view.bounds.height-keyboarHeight*2)){
                let numerator =
                    midline - viewRect.origin.y - MINIMUM_SCROLL_FRACTION * viewRect.size.height;
                let denominator = (MAXIMUM_SCROLL_FRACTION - MINIMUM_SCROLL_FRACTION)*viewRect.size.height;
                var heightFraction = numerator / denominator
                print(heightFraction)
                if (heightFraction < 0.0)
                {
                    heightFraction = 0.0;
                }
                else if (heightFraction > 1.0)
                {
                    heightFraction = 1.0;
                }
                animatedDistance = floor(keyboarHeight * heightFraction)
                var viewFrame = originalFrame
                viewFrame.origin.y -= animatedDistance
                print("viewFrame \(viewFrame)")

                UIView.animateWithDuration(0.3, animations: {
                    print("viewFrame \(viewFrame)")
                    view.frame = viewFrame
                })
            }
        }
    }

    func textFieldDidEndEditing(textField: UITextField) {
            UIView.animateWithDuration(0.3, animations: {
                self.view.frame = self.parentOriFrame
            })
        refreshScrollView()
    }
}
extension NSDate {
    var formattedISO8601: String {
        let formatter = NSDateFormatter()
        formatter.timeZone = NSTimeZone(forSecondsFromGMT: 0)
        formatter.dateFormat = "dd-MM-yyyy"   //+00:00
        return formatter.stringFromDate(self)
    }
}
