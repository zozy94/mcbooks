//
//  AudioItemView.swift
//  MCBooks
//
//  Created by Quang Anh on 6/20/16.
//  Copyright © 2016 Quang Anh. All rights reserved.
//

import UIKit

class AudioItemView: UIView {
    var view: UIView!
    
    @IBOutlet weak var audioName: UILabel!
    @IBOutlet weak var bookName: UILabel!
    var mediaData: Media!
    override init(frame: CGRect) {
        super.init(frame: frame)
        xibSetup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        xibSetup()
    }
    
    func xibSetup() {
        view = loadViewFromNib()
        view.frame = bounds
        let gesture = UITapGestureRecognizer(target: self, action: #selector(BookItemView.tap(_:)))
        view.addGestureRecognizer(gesture)
        addSubview(view)
    }
    func tap (sender:UITapGestureRecognizer){
        AppFunc().appDelegate.SelectedMedia = self.mediaData
        NSNotificationCenter.defaultCenter().postNotificationName("ShowMedia", object: nil, userInfo: nil)
    }

    func setup(media: Media){
        self.audioName.text = media.name
        self.bookName.text = media.bookName
        self.mediaData = media
    }
    
    func loadViewFromNib() -> UIView {
        let bundle = NSBundle(forClass: self.dynamicType)
        let nib = UINib(nibName: "AudioItemView", bundle: bundle)
        let view = nib.instantiateWithOwner(self, options: nil)[0] as! UIView
        return view
    }
}
